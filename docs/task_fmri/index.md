# Task fMRI

FSL contains a range of tools for working with task-based fMRI data. [FEAT](task_fmri/feat/index.md) is a full processing pipeline for both single-subject and group-level task-fMRI analyses. For more fine-grained control over the haemodynamic response function (HRF) used in the time series modelling, [FLOBS](task_fmri/flobs.md) is closely integrated into FEAT.

[FABBER](task_fmri/fabber.md) is a Bayesian model fitting tool intended for use in task modelling of ASL data.

If you have physiological recordings (heart rate, respiration, etc) throughout your fMRI acquisition, you can use [PNM](task_fmri/pnm.md) to generate regressors for use in a FEAT analysis.

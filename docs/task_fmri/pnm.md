# PNM

## Contents

- [PNM: Physiological Noise Modelling](#pnm-physiological-noise-modelling)
- [Referencing](#referencing)
- [User Guide](#user-guide)
- [Outputs of PNM](#outputs-of-pnm)
- [Using PNM in FEAT](#using-pnm-in-feat)
- [Other tools (advanced)](#other-tools-advanced)

## PNM: Physiological Noise Modelling

PNM is a tool used in conjunction with the GLM (via FEAT) that can be used to model (or "regress out") the **effects of physiological noise in functional MRI data**. That is, it creates EVs (regressors) that can be used to model the physiological noise within the GLM, alongside other stimulus-related regressors. The regressors are created using physiological recordings, such as pulse-ox and respiratory bellows, and model the MRI signal via a series of sinusoidal basis functions (like RETROICOR).

- This tool requires **physiological recordings** to be taken during the functional MRI session.
- We strongly recommend also recording scanner triggers with the other physiological recordings as this can be vital in getting the timing accurate.
- Physiological noise modelling is very important for studies looking at the brainstem, spinal cord, and other structures in the inferior parts of the brain, although there is some benefit in any study.

![pnm figure](pnm_fig.jpg)

## Referencing

If you use PNM in your research, please make sure that you reference at least the first of the articles listed below, and ideally other appropriate ones from the list.

>Brooks JC, Beckmann CF, Miller KL, Wise RG, Porro CA, Tracey I, Jenkinson M. Physiological noise modelling for spinal functional magnetic resonance imaging studies. Neuroimage, 39(2):680-92, 2008.

>Harvey AK, Pattinson KT, Brooks JC, Mayhew SD, Jenkinson M, Wise RG. Brainstem functional magnetic resonance imaging: disentangling signal from physiological noise. J Magn Reson Imaging. 28(6):1337-44, 2008.

>Birn RM, Diamond JB, Smith MA, Bandettini PA. Separating respiratory-variation-related fluctuations from neuronal-activity-related fluctuations in fMRI. Neuroimage, 31(4):1536-48, 2006.

>Glover GH, Li TQ, Ress D. Image-based method for retrospective correction of physiological motion effects in fMRI: RETROICOR. Magn Reson Med., 44(1):162-7, 2000.

>Shmueli K, van Gelderen P, de Zwart JA, Horovitz SG, Fukunaga M, Jansma JM, Duyn JH. Low-frequency fluctuations in the cardiac rate as a source of variance in the resting-state fMRI BOLD signal. Neuroimage, 38(2):306-20, 2007.

>Chang C, Cunningham JP, Glover GH. Influence of heart rate on the BOLD signal: the cardiac response function. Neuroimage, 44(3):857-69, 2009.

>Cohen-Adad J, Gauthier CJ, Brooks JC, Slessarev M, Han J, Fisher JA, Rossignol S, Hoge RD. BOLD signal responses to controlled hypercapnia in human spinal cord. Neuroimage, 50(3):1074-84, 2010.

## User Guide

### GUI

The recommended (and easiest) way to run PNM is via the Pnm GUI.

This GUI allows you to specify the following **inputs**:

- text file containing the sampled physiological recordings (each line containing numbers corresponding to one point in time)
- FMRI timeseries data (a 4D image in NIFTI format)
- information about the text file (which columns are cardiac/respiratory/triggers)
- information about the physiological recordings (e.g., sampling rate)
- information about the FMRI scans (e.g., slice ordering)

It also allows the user to specify the desired **outputs**:

- order of cardiac EVs (this is the number of pairs of sinusoidal frequencies to use - e.g. a value of 2 would indicate that the base frequency and the first harmonic, twice the base frequency, should be used - which would generate 4 regressors)
- order of respiratory EVs
- order of interaction EVs (see below)
- optional RVT, HeartRate and CSF regressors

![pnm gui](pnm_gui.png)

### Triggers

Scanner triggers must be supplied, as experience has shown that relative timings between the MR scans and physiological recordings is not sufficiently accurate/reliable without this. The scanner triggers need to be once per volume (or once per slice). The format for the trigger input is just the same as the other traces, and no special values need to be used: it just requires a strong rising edge per trigger point, returning to a consistent baseline level (e.g. baseline of zero going up to a value of one at each trigger and then down again after a small number of samples, anywhere from one sample to 50% of the interval can be spent at the high value).

Cardiac input information can either be provided as a full trace or as "Pulse Ox Triggers" (as detected from any external software). Scanner triggers and cardiac triggers are not interchangeable as they contain different information.

### Interaction EVs

The interaction EVs combine cardiac and respiratory frequencies. For instance, if the cardiac frequency is fc and the respiratory frequency is fr then the interaction terms have frequencies `nc*fc +/- nr*fr` where the permitted values of nc and nr are determined by the order of interactions entered in the GUI. For example, cardiac order 3 and respiratory order 2 would generate frequencies: `1*fc +/- 1*fr ; 2*fc +/- 1*fr ; 3*fc +/- 1*fr ; 1*fc +/- 2*fr ;2*fc +/- 2*fr ; 3*fc +/- 2*fr`. For each frequency a pair of regressors (sin and cos) are generated. So the number of interaction EVs (regressors) can grow quite large.

Note that in practice the frequencies here are changing throughout the scan (as heart rate and breathing rate vary) so they should be thought of more like instantaneous frequencies.

### Slice Timing Information

It is necessary to supply information about which direction is the through-slice direction (according to the voxel axes: x,y,z) and then either the slice ordering (sequential or interleaved; up or down) or a manually specified slicetiming file. If the interleaved option is used then the timing uses the Siemens conventions for the order, which depends on whether an even or odd number of slices are acquired. For users on a non-Siemens system we recommend using a manually specified slice timing file when working with interleaved data.

The manually specified slicetiming file needs to be a text file that specifies the timing (in seconds) of every slice relative to the start of the volume acquisition (t=0). The file must have one number per slice and be arranged in one line, separated by spaces, and each number is the timing value for that slice (e.g. third number gives the timing of the third slice, where numbering of slices is consistent with the NIFTI voxel coordinate; i.e. the way that FSLeyes shows them).

As an advanced option, it is also possible to specify separate times for each volume by having multiple lines (as many lines as number of volumes), where the values in each line represent the timings for one volume, across all slices, which allows for a variable slice timing per volume.

### PNM stage 1

Set up the GUI and click **Go** when ready.

### Manual check/edit

Unfortunately, physiological recordings, and the detection of the peaks in them, tends not to be perfect. Therefore we have a simple and quick method for manually checking the recordings and peak detections, and noting down errors to be corrected by the following stage. This is all done within a browser that shows the waveforms and the appropriate peaks.

The output directory will be created and when stage 1 has finished running there will be a file ending with `_pnm1.html`.

- Open this file in the firefox web browser
  - Note that it uses a javascript plugin that other browsers may not support, and must be run from the same machine that the analysis was run on, e.g. the server, and will not work if the files are moved

If you do not have the firefox browser (or cannot install it) then you can either skip this step (hoping for the best) or use some other software to view the outputs (e.g. matlab/octave) and note down the times (in seconds) of erroneous/extra peaks or missing peaks. You can then rerun popp (see the log file to get the previous options) and add the options `--respadd`,`--respdel`,`--cardadd`,`--carddel` for respiratory/cardiac additions/deletions.

Note that **respiratory peaks are only shown (and only detected) in the case where the RVT output has been requested**. If that is not the case, the respiratory trace is converted to a phase using the RETROICOR methodology, without requiring peak detection. Therefore, there is no need to manually check and correct the respiratory peaks unless RVT is requested. Currently the respiratory checkbox is still available on the webpage even when RVT is not selected - this will be changed in future.

### PNM stage 2

If you have done the manual check/edit in the browser then follow the instructions given in the browser window (which are tailored to your particular directory and data).

If you have not done a manual check/edit then go to the output directory and run the script `pnm_stage2`.

## Outputs of PNM

The main output of PNM is a file containing the desired regressor information. Since the timing of the MR acquisition varies with slice in the FMRI data, you do not get a simple text file output. Instead you get a set of outputs in the NIFTI format that are _voxelwise regressors_. There is also a text file with a name ending in _evlist.txt can contains the appropriately ordered list of all of these voxelwise regressors.

For those that are interested, the order of the individual EVs in the output list is the following:

- Cardiac only (sin, then cos, low to high freq: e.g. sin(p), cos(p), sin(2p), cos(2p), etc.)
- Respiratory only (as above for individual terms)
- Interaction terms `(cos(c+r), sin(c+r), cos(c-r), sin(c-r), for c,r = multiples of cardiac/resp phase`; e.g. starting with `c=1*cardiac phase` and `r=1*resp phase`, then `c=1*cardiac phase` and `r=2*resp phase` for next set of four, if `--multr` is 2 or more, ..., `c=1*cardiac` and `r=n*resp`, then `c=2*cardiac` and `r=1*resp`, if `--multc` is 2 or more, ...)
- Heartrate
- RVT
- CSF

If you are just entering the entire set of EVs as confounds into FEAT (the recommended method) then you don't need to worry about the order above.

## Using PNM in FEAT

To use the output of PNM with FEAT all you need to do is to find the file with the name ending in `_evlist.txt`and enter this (via the file browser) in the **Voxelwise Confound** List in the FEAT GUI (on the Stats tab). This will use these regressors as confounds, giving them automatic zero entries in all contrasts. With the GLM this ensures that wherever there is any shared variance between these and the EVs of interest (specified in the main design matrix) that the shared variance will be assigned to the physiological noise model. This is the conservative, and more correct, option and avoids inflating false positive rates, maintaining interpretability of any significant output results as being due to the changes of interest and not due to physiological noise.

### Note on randomise

Since `randomise` currently cannot account for time-series autocorrelations (it invalids the exchangeability) it is not possible to use PNM with `randomise`.

## Other tools (advanced)

The basic command-line tools used by PNM are: `popp` and `pnm_evs`.

The usage for these are:

#### popp

```bash
popp (Version 1.0)
Copyright(c) 2011, University of Oxford (Mark Jenkinson)

Usage: 
popp [options] -i <input data file> -o <output data file>

Compulsory arguments (You MUST set one or more of):
        -i,--in input physiological data filename (text format)
        -o,--out        output basename for physiological data and timing/triggers (no extensions)

Optional arguments (You may optionally specify one or more of):
        -s,--samplingrate       sampling rate in Hz (default is 100Hz)
        --tr    TR value in seconds
        --resp  specify column number of respiratory input
        --cardiac       specify column number of cardiac input
        --trigger       specify column number of trigger input
        --rvt   generate RVT data
        --heartrate     generate heartrate data
        --pulseox_trigger       specify that cardiac data is a trigger
        --smoothcard    specify smoothing amount for cardiac (in seconds)
        --smoothresp    specify smoothing amount for respiratory (in seconds)
        --highfreqcutoff        high frequency cut off for respiratory filter in Hz (default is 5Hz)
        --lowfreqcutoff low frequency cut off for respiratory filter in Hz (default is 0.1Hz)
        --initthreshc   initial threshold percentile for cardiac (default 90)
        --nthreshc      neighbourhood exclusion threshold percentile for cardiac (default 60)
        --initthreshr   initial threshold percentile for respiratory (default 80)
        --nthreshr      neighbourhood exclusion threshold percentile for respiratory (default 80)
        --invertresp    invert respiratory trace
        --invertcardiac invert cardiac trace
        --noclean1      turn off cleanup phase 1
        --noclean2      turn off cleanup phase 2
        --loadcardphase input cardiac phase for reprocessing (text format)
        --loadrespphase input respiratory phase for reprocessing (text format)
        --vol   input volumetric image (EPI) filename
        --startingsample        set sample number of the starting time (t=0)
        --respadd       comma separated list of times (in sec) to add to respiratory peak list (uses nearest local max)
        --respdel       comma separated list of times (in sec) to delete from respiratory peak list (uses nearest existing peak)
        --cardadd       comma separated list of times (in sec) to add to cardiac peak list (uses nearest local max)
        --carddel       comma separated list of times (in sec) to delete from cardiac peak list (uses nearest existing peak)
        -v,--verbose    switch on diagnostic messages
        --debug output debugging information
        -h,--help       display this message
```

#### pnm_evs

```bash
pnm_evs (Version 2.0)
Copyright(c) 2011, University of Oxford (Mark Jenkinson)

Usage: 
pnm_evs [options] --tr=3.0 -i -o -r -c ... TBD ...

Compulsory arguments (You MUST set one or more of):
        -i,--in input image filename (of 4D functional/EPI data)
        -o,--out        output filename (for confound/EV matrix)
        --tr    TR of fMRI volumes (in seconds)

Optional arguments (You may optionally specify one or more of):
        -c,--cardiac    input filename for cardiac values (1 or 2 columns: time [phase])
        -r,--respiratory        input filename for respiratory phase values (1 or 2 columns: time [phase])
        --oc    order of basic cardiac regressors (number of Fourier pairs) - default=2
        --or    order of basic respiratory regressors (number of Fourier pairs) - default=1
        --multc order of multiplicative cardiac terms (also need to set --multr) - default=0
        --multr order of multiplicative respiratory terms (also need to set --multc) - default=0
        --csfmask       filename of csf mask image (and generate csf regressor)
        --rvt   input filename of RVT data (2 columns: time value)
        --heartrate     input filename for heartrate data (2 columns: time value)
        --rvtsmooth     Optional smoothing of RVT regressor (in seconds - default 0)
        --heartratesmooth       Optional smoothing of heart rate regressor (in seconds - e.g. 10)
        --slicedir      specify slice direction (x/y/z) - default is z
        --sliceorder    specify slice ordering (up/down/interleaved_up/interleaved_down)
        --slicetiming   specify slice timing via an external file
        --debug turn on debugging output
        -v,--verbose    switch on diagnostic messages
        -h,--help       display this message
```
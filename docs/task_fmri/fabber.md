# FABBER

Bayesian nonlinear model fitting provides a more flexible alternative to traditional linear analysis of perfusion data. In multi-echo or multi-inversion-time data, this approach uses all time points to estimate all the parameters of interest simultaneously. This can be more accurate because it uses a detailed nonlinear model of the pulse sequence, rather than assuming that different parts of the signal are linearly related to each quantity of interest.

To make this analysis technique fast enough for routine use, we have adapted a fast approximate method known as Variational Bayes (VB) to work with non-linear generative models. Experimentally we have found that these calculations provide nearly identical results to sampling method and require only a fraction of the computation time (around a minute per slice).

`fabber` is a command-line tool for approximate Bayesian inference using generative signal models. The design is modular and new nonlinear forward models can be incorporated into the source code easily. Dual-echo ASL models are documented here, `fabber` can also be used for multi inversion time resting ASL data, but this can most easily be achieved through the [BASIL](physiological/basil.md) tool.


<div id="pagetoc" start="2" end="2"></div>


## Simultaneous Inference of BOLD and CBF from Dual-echo ASL Data

Dual-echo Arterial Spin Labelling is a pulse sequence used to acquire BOLD and Cerebral Blood Flow (CBF) information simultaneously. The standard analysis methods involve performing separate analyses on the data from each echo time to obtain separate BOLD and CBF estimates. A more flexible approach is to use a nonlinear forward model to determine all the physical parameters of interest simultaneously, using data from both echo times. This models blood flow and inversion recovery explicitly and is built into a Bayesian framework to provide estimates of uncertainty.

## Referencing

If you use `fabber` in your research, please make sure that you reference at least the first of the articles listed below, and ideally the complete list.

> Chappell, M.A., Groves, A.R., Woolrich, M.W., "Variational Bayesian inference for a non-linear forward model", IEEE Trans. Sig. Proc., 2009, 57(1), 223–236.

> Woolrich, M., Chiarelli, P., Gallichan, D., Perthen, J., Liu, T. "Bayesian Inference of Haemodynamic Changes in Functional ASL Data", Magnetic Resonance in Medicine, 56:891-906, 2006.

> Groves, A. R., Chappell, M. A., & Woolrich, M. W. (2009). Combined spatial and non-spatial prior for inference on MRI time-series. NeuroImage, 45(3), 2009. doi:10.1016/j.neuroimage.2008.12.027.


## Using `fabber` on dual-echo ASL data

To run `fabber` on a dual-echo ASL data set, you will need the following:

 - Data sets for both echo times
 - Stimulus information, preconvolved with the appropriate Haemodynamic Response Functions
 - Details of the pulse sequence used (e.g. echo times, inversion times). Currently supported models are QUIPSS II, Q2TIPS and pseudo-cASL.

## Preprocessing

We assume that the high- and low-TE data sets are stored as two separate files, which should be converted into NIFTI format (`.nii.gz` files) and should have the first few volumes discarded. You can remove very low frequency noise using `fslmaths`, for example

```bash
fslmaths raw_highte.nii.gz -bptf 50 -1 filtered_highte.nii.gz
fslmaths raw_lowte.nii.gz -bptf 50 -1 filtered_lowte.nii.gz
```

will remove low-frequency noise using sigma = 50 volumes. Other types of preprocessing may be used (e.g. using Matlab) but don't rescale the data sets, since it's important to preserve the relative signal magnitude between the two echo times.

A mask image is also required, showing which voxels to perform the inference on. This can be produced manually (in FSLeyes) or automatically (using [BET](structural/bet.md)).

For the nonlinear QUIPSS II model, three design matrices are required: BOLD, CBF, and Static Magnetization are all time-varying. FEAT can be used to prepare these three design-matrix files; they should simply be the stimulus convolved with a suitable Haemodynamic Response Function. A standard HRF can be used for the BOLD design matrix and a quicker-responding HRF should be used for the CBF and Static Magnetization design matrices. It's possible to use multiple regressors in any of these files (for example, a derivative term) if desired. A set of nuisance regressors (e.g., motion) may also be provided as an additional input.

## Command-line syntax

The typical syntax for analysing a QUIPSS II data set is:

```bash
fabber --method=vb --model=quipss2 --noise=ar --num-echoes=2 --output=<output_directory> --data1=<low_te_data> --data2=<high_te_data> --mask=<mask_image> --bold-basis=<bold_design> --cbf-basis=<cbf_design> --statmag-basis=<statmag_design>
```

There are a number of additional numerical parameters that can be added to adjust the forward model to fit the details of your particular scan. For QUIPSS II, these parameters are

 - Inversion times in seconds (defaults: `--ti1=0.6`, `--ti2=1.5`)
 - Echo times in milliseconds (defaults: `--te1=9.1`, `--te2=30`)
 - Tag/control order: (default: `--tag-pattern=TC`). The pattern of Cs and Ts will be repeated as necessary to fill the entire scan length.
 - In addition, the T1 of blood and the inversion efficiency are normally treated as constants (defaults: `--t1b=1.66`, `--inv-eff=1.0`).
 - These can be given uncertainties (and inferred upon) by providing nonzero standard deviations for the `--t1b-stdev=` and `--inv-eff-stdev=` options.

For a complete list of general command-line options, type

```bash
fabber --help
```

For parameters specific to the QUIPSS II model, type:

```
fabber --help --model=quipss2
```

The other models available are: `q2tips-dualecho` and `pcasl-dualecho`.


## Output files

After `fabber` finishes, the output folder will contain point estimate and pseudo-Z-statistic images for each of the forward model parameters, stored in `mean_<parameter_name>.nii.gz` and `zstat_<parameter_name>.nii.gz` respectively. To find significant stimulus-correlated changes in the BOLD effect, CBF, and Static Magnetization, look at `zstat_BOLD_abschg_1.nii.gz`, `zstat_Q_abschg_1.nii.gz`, and `zstat_M_abschg_1.nii.gz`. For quantitative parameter estimates, use the various `mean_*.nii.gz` files (e.g. % CBF change is simply `100 * mean_Q_abschg_1 / mean_Q0`). For more detailed analysis of the Bayesian results (e.g. looking for covariance in parameter uncertainties), use `finalMeans.nii.gz` and `finalCovariance.nii.gz.` These completely describe the multivariate normal distribution on the forward model parameters (in the NIFTI formats for vectors and symmetric matrices). Some noise parameter distributions are also appended to this distribution. The parameter names corresponding to the vector elements are saved (one per line) in `paramnames.txt`.

If you encounter problems, start by reading the output carefully and looking at the `logfile` - there may be some useful warning messages there.

Spatial regularization

It is also possible to use spatial priors with fabber, these are preferable to spatially smoothing the data before analysis, since that can result in interaction with the non-linear model. `--method=spatialvb`: This instructs fabber to use spatial priors, i.e. to take spatial homogeneity between neighbouring voxels into account. This produces smoother resulting parameter images, but in a more principled manner than simply smoothing data or results. The implementation is based on the approach described in

> Penny, W.D., Trujillo-Barreto, N.J., Friston K.J., "Bayesian fMRI time series analysis with spatial priors", NeuroImage 24:2, pp.350–362, 2005, with some minor modifications.

`--param-spatial-priors=<choice_of_prior_forms>`: Use only with `--method=spatialvb`. This makes it possible to choose different priors for each parameter. This should be a string with one letter per forward-model parameter. S is the spatial prior as above (default), N is a nonspatial prior, and D is a Gaussian-process based prior for combined spatial and non-spatial prior inference. A single + sign may be used to repeat the preceding letter zero or more times in order to reach the correct length. For example, `--param-spatial-priors=SN+D` would use a spatial prior on the first parameter, combination prior on the last, and nonspatial priors on all others. Note that the D option only make sense when an informative non-spatial prior is specified by `--fwd-initial-prior`; for parameters without informative nonspatial priors, use S. More details of the D prior can be found in the Groves 2009 NeuroImage paper.

## Other options

`--nuisance-basis=<noise_regressors>`: include additional regressors in the QUIPSS II model that act directly on the signal (not echo-time-weighted). Separate regression parameters are used for each echo time.

`--noise=white` (instead of `--noise=ar`): Use a white noise model instead of AR(1). By default this will use the same noise variance for all data points. Add `--noise-pattern=12` to use different variances for odd and even (i.e. TE1 and TE2) data points.

`--fwd-initial-prior=<prior_vest_file>`: Use an informative prior, specified by a symmetric matrix in a VEST-format file. For an N-parameter model, the matrix should be N+1 by N+1: an N-by-N prior covariance matrix in the top left (must be positive definite), supplemented by an extra column and row vector giving the prior mean of each parameter (must give the same values in both locations). The bottom-right corner must be the number one.

`--allow-bad-voxels`: By default fabber will halt if a numerical error arises during the calculation of any voxel. Using this option, it will simply issue a warning and skip to the next voxel; this should be used with caution. Each time a voxel is skipped you'll get a `Going on to the next voxel` message in the `logfile` - if you have more than a few of these then it's likely that other voxels are also experiencing convergence problems. If you are using your own forward model you may be able to correct this by changing the parameterization to make the signal more linear with respect to the parameters.

`--save-residuals` and `--save-model-fit`: produce 4D data sets for the residuals and the model fit. The sum of these two files should equal the original data (with the mask applied and the echo times interleaved).

`-@ <options_file>`: Read a list of options from a text file, as if they were typed at the command line.

`--ar1-cross-terms=dual`, `same`, or `none`: By default, an independent autoregressive noise model is used for each echo time (none). However, the noise between the two echo times is no independent, so this default tends to overestimate the amount of information in the data. By selecting `same` or `dual`, you can introduce one or two cross-terms into the AR(1) noise models which partially model this correlation. `dual` is recommended for dual-echo ASL data.

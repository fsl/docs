# BayCEST: Bayesian analysis for chemical exchange saturation transfer z-spectra

Quantitative analysis of CEST z-spectra can be achieved by fitting of a multi-pool model to each individual sampled spectrum. There are a large number of parameters within the Bloch-McConnell equations that describe each pool making model fitting prone to inaccuracy and increasing the risk of over fitting. A solution is to provide prior information about the parameters which necessitates the use of a Bayesian method. BayCEST exploits a Bayesian non-linear fitting algorithm, which is essentially a probabilistic version of non-linear least squares, along with a multi-pool implementation of the Bloch-McConnell equations. This algorithm provides a (relatively) fast means to quantify CEST data whilst reducing some of the problems associated with traditional least squares fitting algorithms.

?> **Quantiphyse** is another recommended tool for analysis of CEST data - more details can be found at https://quantiphyse.readthedocs.io/en/latest/

<div id="pagetoc" start="2" end="2"></div>

## Referencing

If you use BayCEST in your research, please make sure that you reference at least the first of the articles listed below, and ideally the complete list.

> Chappell, M. A., Donahue, M. J., Tee, Y. K., Khrapitchev, A. A., Sibson, N. R., Jezzard, P., & Payne, S. J. (2012). Quantitative Bayesian model-based analysis of amide proton transfer MRI. Magnetic Resonance in Medicine. doi:10.1002/mrm.24474

> Chappell, M., Groves, A., Whitcher, B., & Woolrich, M. (2009). Variational Bayesian Inference for a Nonlinear Forward Model. IEEE Transactions on Signal Processing, 57(1), 223–236.

## Running BayCEST

To run BayCEST you will need:

 - Sampled CEST z-spectra (nifti image).
 - Mask to indicate what region of the data to be processed (nifti image)
 - A file specifying details of the pools to be included in the model: poolmat.
 - A file detailing the different samples contained within the data: dataspec.
 - A file detailing the pulse used for saturation: ptrain.

BayCEST can be called from the command line with the following information:

 - `--data=` Input data image file.
 - `--mask=` Mask image file.
 - `--output=` Output directory name (a new folder will be generated with this name).
 - `--pools=` Details of the pools within the model: poolmat.
 - `--spec=` Details of the samples in the data: dataspec.
 - `--ptrain=` Details of the saturation pulse shape: ptrain.

BayCEST also has a few advanced options:

 - `--spatial` Turn on the spatial smoothing prior, this is recommended if processing in vivo data, it is generally not appropriate if there is no spatial structure in the data e.g. simulations.
 - `--t12prior` Incorporate uncertainty/variability in T1 and T2 value in the estimation. This treats T1 and T2 for each of the pools as extra parameters to be estimated from the data, but with tightly defined priors to avoid over fitting but allow T1 and T2 variability to be reflected in the results.

## Poolmat: the pool specification matrix

It is necessary to specify details about each of the pools that you wish to include within the model, including the water pool. The key details required are:

 - Centre frequency of the pool: for water in Hz for others specify relative to water (in ppm).
 - Exchange rate expected for the pool (in Hz).
 - T1 value for the pool (in s).
 - T2 value for the pool (in s).

This should take the form of an ascii matrix where each row corresponds to one pool, and each column is a parameter in the order: `Frequency | rate | T1 | T2`.

The first row should refer to the water pool and the freqeuncy should be in Hz according to the field strength, otherwise a ppm value is expected. The exchange rate value for the water pool will be ignored, but should be included, i.e. enter a zero in that column.

Example poolmats:
 - [`cest_apt.mat`](physiological/baycest_cest_apt.mat ':ignore') - APT _in vivo_: water plus amide at 3T.

 - [`cest_apt_mt.mat`](physiological/baycest_cest_apt_mt.mat ':ignore') - APT+MT _in vivo_: water plus amide and assymetric MT at 3T.

## Dataspec: data specification

Each individual volume in the data should contain a single sample from the z-spectrum. It is necessary to specify in the dataspec matrix for each sample:

 - Sampling frequency relative to water (in ppm).
 - B1 field strength of saturation (in Tesla).
 - The number of repetitions of the pulse used for saturation (integer).

The data specification matrix should be an ascii matrix with one row for each volume in the data and the columns representing (in order): `Frequency | B1 field | No. pulses`.

For example:

| | |
|-|-|
|0|0|1|
|3.5|1E-06|1|
|3.5|1E-06|1|

In this example the first point is unsaturated, the second occurs at the APT centre frequency and the final sample is symmetrically placed at the other side of the water centre. Here it has been assumed that only a single pulse has been applied, e.g. continuous saturation.


## Ptrain: pulse specification

The pulse shape used for saturation should be discretized into a series of approximate segments that are then used within the model calculations. This is provided to BayCEST as an ascii matrix where each row is a time point in the approximation, the first entry in the row is the the magnitude of the pulse at that time (in relative units, i.e. the largest value at the peak of the pulse should be 1.0, the whole pulse is scaled by the B1 value specified in the dataspec matrix) and the second entry is time value (in seconds). It is assumed that the pulse starts at zero at time zero. Only a single cycle of the pulse scheme need be specified, BayCEST will automatically produce a pulse train using the number of repeats specified in the dataspec matrix (and in a computationally efficient manner).

Example pulse matrices:

 - [`pulseshp_cont.mat`](physiological/baycest_pulseshp_cont.mat ':ignore') - Continuous pulse with 2 s duration.
 - [`pulseshp_gauss.mat`](physiological/baycest_pulseshp_gauss.mat ':ignore') - Gaussian pulse with a 40 ms duration and 50% duty cycle divided into 32 segments.

## Outputs

The primary outputs of BayCEST are estimates of the various parameters in the multi-pool model. These can be found within the output directory all beginning with `mean_`. Each of the pools are labelled by a letter in the order they appear in the poolmat matrix. The parameters returned are:

 - `mean_M0i_r` - the magnetization of pool i relative to water (`mean_M0a` will refer to the magnetization of the water pool in native units of the data).
 - `mean_kia` - the exchange rate of pool i with pool a (water). Note the exchanges between pools other than water are assumed to be negligible.
 - `mean_ppm_off` - the offset of the water centre frequency, due to B0 field inhomogeneity (in ppm).
 - `mean_ppm_i` - the offset of the pool centre frequency relative to water (in ppm). Note there will not be a result for pool a since this is water and its offset is given in `mean_ppm_off`.
 - `mean_T1i` and `mean_T2i` - T1 and T2 values for the pool (if using `--t12prior`). These should not necessarily be taken as accurate estimates for T1 and T2 values since the data is not well suited to such estimation, but may indicate where T1 and T2 variations occur. However, the uncertainty in T1 and T2 values will have been incororated into the uncertainty estimates on the other parameters. It is most likely that these will reflect the values entered in the poolmat matrix, this is normal.

A modelfit image is also generated which can be compared to the data to visualise the fit of the model to the data.

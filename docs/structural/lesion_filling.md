# Lesion Filling

Improve volume measurements by reducing intensity contrast within known lesion areas. It is intended for use with "small" lesions, such as those typical in Multiple Sclerosis.

## Overview

This tool takes a user-defined lesion mask (usually created manually) together with a structural image (e.g. T1-weighted image, but it could also be T2-weighted, PD, etc.) and a white matter mask in order to "fill" the lesion area in the structural image with intensities that are similar to those in the non-lesion neighbourhood (restricted to white matter only). It has been shown (see references) that using such a method as part of a pre-processing pipeline can improve the registration and segmentation of pathological brains (particular those with Multiple Sclerosis) and the resultant volumetric measures of brain tissue.

A white matter mask is necessary so that lesions that touch areas of non-brain tissue, such as the ventricles, only fill the lesions with intensities from immediately surrounding white matter.

## Referencing

If you use `lesion_filling` in your research, please make sure that you reference the article below.

>M. Battaglini, M. Jenkinson, and N. De Stefano. Evaluating and reducing the impact of white matter lesions on brain volume measurements. Human Brain Mapping, 33(9):2062–2071, 2012.

## Usage and options

The usage of the script is particularly simple and there are no configurable options.

```bash
lesion_filling 
Copyright(c) 2012-2019, University of Oxford (Mark Jenkinson & Taylor Hanayik)

Usage: 
lesion_filling [options] -i <intensity image> -l <lesion mask image> -o <output/filled image>

Compulsory arguments (You MUST set one or more of):
        -i,--in input image filename (e.g. T1w image)
        -o,--out        output filename (lesion filled image)
        -l,--lesionmask filename of lesion mask image

Optional arguments (You may optionally specify one or more of):
        -w,--wmmask     filename of white matter mask image
        -v,--verbose    switch on diagnostic messages
        -c,--components save all lesion components as volumes
        -h,--help       display this message
```

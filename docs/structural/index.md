# Structural MRI


FSL contains a range of tools for working with and segmenting structural MRI images.

- [BET](structural/bet.md) can be used to reliably perform brain extraction (a.k.a. skull-stripping) on images of any modality.
- [FAST](structural/fast.md) can be used to segment a brain image into different tissue types (usually grey matter, white matter, and cerebrospinal fluid).
- [FIRST](structural/first.md) and [MIST](structural/mist.md) can be used to generate segmentations of sub-cortical structures.
- [BIANCA](structural/bianca.md) identifies white matter hyperintensities (WMHs) or small white matter lesions in MRI images, and can work with a range of modalities including T1, T2 and T2-FLAIR.

The [`fsl_anat`](structural/fsl_anat.md) script incorporates several of the above tools into a general pipeline for processing anatomical images.

A range of other tools for analysing and working with structural image data are available within FSL:

 - [FSL-VBM](structural/fslvbm.md) can be used to compare voxelwise differences in the local grey matter related to (e.g.) one clinical score, or across populations.
 - [SIENA and SIENAX](structural/siena/index.md) can be used for single-time-point ("cross-sectional") and two-time-point ("longitudinal") analysis of brain change from T1 images, in particular the estimation of atrophy (volumetric loss of brain tissue).
- [lesion filling(structural/lesion_filling.md)] can be used to fill in small white matter pathologies (as already defined by a mask) with intensities that match surrounding healthy tissue, which is helpful to allow other structural tools to process the image as if no pathologies were present, since this often improves performance for registration and segmentation.

# SIENA FAQ

## What type of images can I use in SIENA/SIENAX?

Good quality structural images should be used (T1-weighted, T2-weighted, PD, etc) where the in-plane resolution is better than 2mm (ideally 1mm). The slice thickness is less critical and can be substantially larger than the in-place resolution; The error is almost independent of slice thickness in the range of 1mm to 6mm for 2D acquisitions. Grey/White-matter contrast is not critical to the accuracy of the results as the computations are done using the outer-grey matter surface and the ventricle surface.

## Can I get regional/local atrophy or change values using SIENA/SIENAX?

A regional breakdown of SIENAX results is available using the -r option. This gives peripheral GM volume and ventricular CSF volume, using standard-space masks. At present, research is being done to obtain finer regional discrimination. However, in the short term, there are no plans to release any code as to do that sensibly would require a much more sophisticated integration of nonlinear registration and structural segmentation. For the reasons given above it is likely that the regional measurements are less accurate than the numbers published for global measures (though within-subject repeatability should be similar as the masking should be consistent).
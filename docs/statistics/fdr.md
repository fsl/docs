# FDR

`fdr` is a simple program which takes in a p-value image and uses FDR (False Discovery Rate) theory to carry out multiple comparison correction. For details on FDR theory see [Tom Nichols' FDR page](https://www.nisox.org/Software/fdr/).

### Outputs

- The main output from FDR is the threshold to apply to the p-value image. Everything below this threshold is significant. If nothing is significant then the threshold will be 0. This threshold is written as text to standard output.
- A thresholded image (with `--othresh`).
- A "q-rate" image (with `-o`) which is effectively the FDR version of a corrected p-value (such that values less than 0.05 would typically be considered significant).

### Options

- `--positivecorr` uses a normalisation that assumes the samples are positively correlated - this is the default.
- `--independent` uses a normalisation that assumes all samples are independent (NB: the normalisation is the same as for `--positivecorr`).
- `--conservative` uses the more general normalisation of (1+1/2+1/3+...+1/N), suitable for any dependence between samples (**not recommended**).


## Examples

The p-value image fed into fdr can be produced in a number of ways.


### Use with `FEAT`

Change directory into the stats subdirectory. It is then necessary to convert a give cope and varcope image into a p-value image. For example:

a) to output the log(p) image:
	
	ttologp -logpout logp1 varcope1 cope1 `cat dof`

b) to create the p-value image:
	
	fslmaths logp1 -exp p1

c) to get the significance threshold:
	
	fdr -i p1 -m ../mask -q 0.05
	Probability Threshold is:  0.00339819 

> Note: The output tells you the FDR threshold, in this case p-values below 0.00339819 are significant.

d) to create a 1-p image for ease of display, thresholds that at 1-thresh and then remasks:

	fslmaths p1 -mul -1 -add 1 -thr .99660181 -mas ../mask thresh_1_minus_p1

> Note that if you are looking at a cope created by `FLAME` fixed-effects, the correct dof is not in the "dof" text file, but in the `tdof_t1` image.


### Use with `randomise`

Output from `randomise` can be fed directly into `fdr`; use the voxel-based thresholding, uncorrected for multiple comparisons, i.e. a `_vox_p_tstat` image. This images stores values as 1-p rather than p (where p is the p-value from 0 to 1). That is, 1 is most significant in a 1-p image (and it is arranged this way to make display and thresholding simple). Therefore `fdr` needs to know that it is a 1-p image. Thus an example would be:

	fdr -i grot_vox_p_tstat1 --oneminusp -m mask -q 0.05 --othresh=thresh_grot_vox_p_tstat1

where the input image, `grot_vox_p_tstat1`, contains 1-p values, as output by `randomise`. Note that the name of this image should be changed to whatever you have called your image.

The output from `fdr` gives you a p-value threshold (not a 1-p value) that can be applied to the p-values, as well as a thresholded image, `thresh_grot_vox_p_tstat1`, where all non-significant voxels are given a value of 1-p=0 and significant voxels still contain the original 1-p values.
# Statistics

In FSL, the primary tool for statistical analysis of task-based fMRI data is [`FEAT`](task_fmri/feat/index.md), which provides an interface for an entire analysis, from pre-processing (brain extraction, motion and slice-timing correction, temporal and spatial filtering, etc), through statistics using the general linear model (GLM), and inference with correction for multiple testing. FEAT can be used for single-session, multi-session, and multi-subject analyses.

While FEAT is a tool that enables complete analyses of task-based fMRI, FSL also provides various other tools that can be used for statistics of other imaging modalities, including resting-state fMRI, diffusion MRI, structural MRI (such as VBM), among others. It also offers tools for statistics that can be integrated into customized analyses. These include:

|Tool|Description|
|-|-|
|`Glm` (`Glm_gui` on Mac)|A graphical tool to help create design matrix and contrast files (*t* and *F*) which can then be used as input to other tools such as `randomise`, `PALM`, and `MELODIC`, and `fsl_regfilt`. For an overview of various common study designs, see [here](statistics/glm.md).|
|[`randomise`](statistics/randomise.md)|A command-line tool that allows inference using permutation testing with imaging data, including correction for multiple testing.|
|[`palm`](statistics/palm/index.md)|A command-line tool that extends the functionality of `randomise` for complex designs, including multivariate, repeated measures/longitudinal, as well as sib-pair designs. PALM also has methods for faster inference and additional methods for multiple testing correction.
|[`cluster`](statistics/cluster.md)|A command-line tool that thresholds a statistical map and produces a table indicating the clusters of contiguous voxels that survive that threshold. If information is available on the smoothness of the map, ``cluster`` can also provide p-values for each cluster calculated using the random field theory.|
|[`fdr`](statistics/fdr.md)|A command-line tool that adjusts a map of uncorrected p-values using the False Discovery Rate.|
|[`mm`](statistics/mm.md)|A command-line tool that models a statistical map as a mixture of Gaussians, thus providing an alternative method for thresholding and finding significant voxels.
|[`swe`](statistics/swe.md)|A command-line tool that uses the ''Sandwich'' Estimator for modelling and inference with longitudinal/repeated measures data.|

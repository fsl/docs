<hr>

?> Maintained by the [Analysis group](https://www.win.ox.ac.uk/research/copy_of_analysis-research/analysis-research) at the [Wellcome Centre for Integrative Neuroimaging](https://www.win.ox.ac.uk/).\
\
See a problem? [Get in touch](support.md#fsl-mailing-list) (or [fix it](https://git.fmrib.ox.ac.uk/fsl/docs/) if you are in the WIN)!

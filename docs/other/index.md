# Other topics

This section contains pages on a range of topics, including data sets and data formats, notes on MRI image orientation, performance considerations, and a list of [related software](other/other_software.md) which you may wish to use alongside FSL.

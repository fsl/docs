# Jülich histological (cyto- and myelo-architectonic) atlas references


<table>
<tr><th>Area</th><th>Label</th><th>Reference</th></tr>
<tr><th colspan=3>Frontal Lobe</th></tr>
<tr><td colspan=3><em>Broca's region</em></td></tr>
<tr><td>Area 44</td><td>Broca_44</td><td rowspan=2>Amunts, K. et al. (1999). J. Comp Neurol. 412, 319-341</td></tr>
<tr><td>Area 45</td><td>Broca_45</td></tr>

<tr><td colspan=3><em>Agranular premotor cortex</em></td></tr>
<tr><td>Area 6</td><td>premotor_6</td><td>Geyer, S. (2003). The Microstructural Border Between the Motor and the Cognitive Domain in the Human Cerebral Cortex (Wien: Springer)</td></tr>

<tr><td colspan=3><em>Primary motor cortex</td></tr>
<tr><td>Area 4a</td><td>PMC_4a</td><td rowspan=2>Geyer, S. et al. (1996). Nature 382, 805-807</td></tr>
<tr><td>Area 4p</td><td>PMC_4p</td></tr>

<tr><th colspan=3>Parietal Lobe</th></tr>
<tr><td colspan=3><em>Somatosensory cortex</em></td></tr>
<tr><td>Area 3a</td><td>PSC_3a</td><td rowspan=3>Geyer, S. et al. (2000). Neuroimage. 11, 684-696</td></tr>
<tr><td>Area 3b</td><td>PSC_3b</td></tr>
<tr><td>Area 1 </td><td>PSC_1 </td></tr>
<tr><td>Area 2 </td><td>PSC_2 </td><td>Grefkes, C. et al. (2001). Neuroimage. 14, 617-631</td></tr>

<tr><td colspan=3><em>Parietal operculum / SII</em></td></tr>
<tr><td>OP 1</td><td>SII_OP1</td><td rowspan=4>Eickhoff, S.B et al. (2006). Cereb. Cortex 16, 254-267.<br/>Eickhoff, S.B et al. (2006). Cereb. Cortex 16, 268-279</td></tr>
<tr><td>OP 2</td><td>SII_OP2</td></tr>
<tr><td>OP 3</td><td>SII_OP3</td></tr>
<tr><td>OP 4</td><td>SII_OP4</td></tr>

<tr><td colspan=3><em>Inferior parietal lobule</em></td></tr>
<tr><td>Area PF  </td><td>IPC_PF  </td><td rowspan=7>Caspers, C,. et al. (2013) Brain Structure & Function 23(3) 615-628 </td></tr>
<tr><td>Area PFcm</td><td>IPC_PFcm</td></tr>
<tr><td>Area PFm </td><td>IPC_PFm </td></tr>
<tr><td>Area PFop</td><td>IPC_PFop</td></tr>
<tr><td>Area PFt </td><td>IPC_PFt </td></tr>
<tr><td>Area Pga </td><td>IPC_PGa </td></tr>
<tr><td>Area PGp </td><td>IPC_PGp </td></tr>

<tr><td colspan=3><em>Intraparietal sulcus</em></td></tr>
<tr><td>hIP1</td><td>AIPS_IP1</td><td rowspan=2>Choi, H.J. et al. (2006). J Comp Neurol. 495(1), 53-69</td></tr>
<tr><td>hIP2</td><td>AIPS_IP2</td></tr>
<tr><td>hIP3</td><td>AIPS_IP3</td><td>Scheperjans, F.et al. (2008). Cerebral Cortex 18(4):846-67.<br/>Scheperjans, F. et al. (2008). Cerebral Cortex 18(9):2141-2157</td></tr>


<tr><td colspan=3><em>Superior parietal lobule</em></td></tr>
<tr><td>Area 5Ci</td><td>SPL_5Ci</td><td rowspan=7>Scheperjans, F. et al. (2008). Cerebral Cortex 18(4):846-67.<br/>Scheperjans, F. et al. (2008). Cerebral Cortex 18(9):2141-2157</td></tr>
<tr><td>Area 5L </td><td>SPL_5L </td></tr>
<tr><td>Area 5M </td><td>SPL_5M </td></tr>
<tr><td>Area 7A </td><td>SPL_7A </td></tr>
<tr><td>Area 7M </td><td>SPL_7M </td></tr>
<tr><td>Area 7P </td><td>SPL_7P </td></tr>
<tr><td>Area 7PC</td><td>SPL_7PC</td></tr>

<tr><th colspan=3>Occipital Lobe</th></tr>
<tr><td colspan=3><em>Visual cortex</em></td></tr>
<tr><td>Area 17 / V1</td><td>hOC1 </td><td rowspan=2>Amunts, K et al. (2000). Neuroimage. 11, 66-84</td></tr>
<tr><td>Area 18 / V2</td><td>hOC2 </td></tr>
<tr><td>hOC3v (V3v) </td><td>hOC3v</td><td rowspan=2>Rottschy, C. et al (2007). Human Brain Mapping, 28(1): 1045-59</td></tr>
<tr><td>hOC4v (V4)  </td><td>hOC4 </td></tr>
<tr><td>hOC5 (V5)   </td><td>hOC5 </td><td>Malikovic, A. et al. (2007). Cereb. Cortex 17, 562-574.<br/>Wilms, M. et al. (2005). Anat. Embryol. (Berl) 210, 485-495</td></tr>

<tr><th colspan=3>Temporal Lobe</th></tr>
<tr><td colspan=3><em>Auditory cortex</em></td></tr>
<tr><td>Area TE 1.0</td><td>PAC_TE10</td><td rowspan=3>Morosan, P. et al. Neuroimage 13, 684-701</td></tr>
<tr><td>Area TE 1.1</td><td>PAC_TE11</td></tr>
<tr><td>Area TE 1.2</td><td>PAC_TE12</td></tr>

<tr><td colspan=3><em>Hippocampus formation</em></td></tr>
<tr><td>Cornu ammonis    </td><td>Hippocampus_CA</td><td rowspan=5>Amunts, K. et al. (2005). Anat. Embryol. (Berl) 210, 343-352</td></tr>
<tr><td>Entorhinal cortex</td><td>Hippocampus_EC</td></tr>
<tr><td>Fascia dentata   </td><td>Hippocampus_FD</td></tr>
<tr><td>HATA - region    </td><td>Hippocampus_HATA</td></tr>
<tr><td>Subiculum        </td><td>Hippocampus_SUB </td></tr>

<tr><td colspan=3><em>Amygdala complex</em></td></tr>
<tr><td>Centro-median nucleus</td><td>Amygdala_CM</td><td rowspan=3>Amunts, K. et al. (2005). Anat. Embryol. (Berl) 210, 343-352</td></tr>
<tr><td>Latero-basal nucleus </td><td>Amygdala_LB</td></tr>
<tr><td>Superficial nucleus  </td><td>Amygdala_SF</td></tr>

<tr><th colspan=3>White matter</th></tr>
<tr><td>Acoustic radiation                </td><td>Fiber_ar  </td><td rowspan=13>Burgel, U. et al., (2006). Neuroimage 29, 1092-1105.<br/>Burgel, U. et al., (1999) Neuroimage. 10(5), 489-99</td></tr>
<tr><td>Callosal body                     </td><td>Fiber_cb  </td></tr>
<tr><td>Cingulum                          </td><td>Fiber_cing</td></tr>
<tr><td>Corticospinal tract               </td><td>Fiber_ct  </td></tr>
<tr><td>Fornix                            </td><td>Fiber_forn</td></tr>
<tr><td>Inferior occipito-frontal fascicle</td><td>Fiber_iof </td></tr>
<tr><td>Laternal geniculate body          </td><td>Fiber_lgb </td></tr>
<tr><td>Mamillary body                    </td><td>Fiber_mb  </td></tr>
<tr><td>Medial geniculate body            </td><td>Fiber_mgb </td></tr>
<tr><td>Optic radiation                   </td><td>Fiber_or  </td></tr>
<tr><td>Superior longitudinal fascicle    </td><td>Fiber_slf </td></tr>
<tr><td>Superior occipito-frontal fascicle</td><td>Fiber_sof </td></tr>
<tr><td>Uncinate fascicle                 </td><td>Fiber_uf  </td></tr>
</table>

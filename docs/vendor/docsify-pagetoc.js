/*
 * Docsify plugin which automatically generate a table of contents from HTML
 * header elements. To use, add the following to your markdown file:
 *
 * <div id="pagetoc"
 *      start="1"
 *      end="5"
 *      offset="1"
 *      ordered="false"></div>
 *
 * where:
 *    start:   Highest header level to include (e.g. 1 == "h1")
 *    end:     Lowest header level to include (e.g. 5 == "h5")
 *    offset:  Skip the first <offset> header elements
 *    ordered: "true" -> use <ol> elements
 */

(function () {
    const HEADERS = ["h1", "h2", "h3", "h4", "h5"];
    const START   = 1;
    const END     = 5;
    const OFFSET  = 0;
    const ORDERED = false;
    var   pageTOC = function (hook, vm) {

        hook.doneEach(function () {

            var toc = document.querySelector("#pagetoc");

            if (toc === null) {
                return;
            }

            var offset  = Number( toc.getAttribute("offset"))  || OFFSET;
            var start   = Number( toc.getAttribute("start"))   || START;
            var end     = Number( toc.getAttribute("end"))     || END;
            var ordered = Boolean(toc.getAttribute("ordered")) || ORDERED;
            var query   = HEADERS.slice(start - 1, end);
            var headers = document.querySelectorAll(query.join(","));
            headers     = Array.from(headers).slice(offset);

            if (headers.length == 0) {
                return;
            }

            var listType;
            var startList;
            var endList;

            if (ordered) listType = "ol";
            else         listType = "ul";

            startList = "<"  + listType + ">";
            endList   = "</" + listType + ">";

            var prevDepth = -1;
            var depth     = 0;
            var html      = "";

            for (let i = 0; i < headers.length; i++) {
                var hdr = headers[i];
                depth   = query.indexOf(hdr.nodeName.toLowerCase());
                var ref = hdr.querySelector("a").getAttribute("href");
                hdr     = "<a href=\"" + ref + "\">" + hdr.innerText + "</a>";

                var prefix = "";
                var suffix = "";
                var dist   = depth - prevDepth;
                var indent = Array(Math.abs(dist) + 1);

                if (i > 0) {
                    if      (dist == 0) prefix = "</li>";
                    else if (dist >  0) prefix =           indent.join(startList);
                    else if (dist <  0) prefix = "</li>" + indent.join(endList   + "</li>");

                }
                if (i == headers.length - 1) {
                    if      (dist == 0) suffix = "</li>";
                    else if (dist <  0) suffix = indent.join(endList + "</li>");
                    else if (dist >  0) suffix = indent.join(endList + "</li>");
                    suffix = Array(depth + 1).join(endList + "</li>");
                }

                html     += prefix + "<li>" + hdr + suffix + "\n";
                prevDepth = depth;
            }

            var list = document.createElement(listType);
            list.innerHTML = html;
            toc.appendChild(list);
        });
    };

    $docsify = $docsify || {};
    $docsify.plugins = [].concat(pageTOC, $docsify.plugins || []);
})();

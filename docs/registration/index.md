# Registration

Registration refers to the process of aligning images together, and is a fundamental step in all MRI image analysis tasks.  FSL contains a selection of high quality image registration tools, which can be used to linearly and non-linearly align two images together.

Most registration tasks begin with calculating a set of global translations, scalings, and rotations - this is referred to as a linear registration, and can be performed using [FLIRT](registration/flirt/index.md). Once two images are roughly aligned, it is common to calculate a non-linear registration to achieve fine-grained alignment of anatomy. Both [FNIRT](registration/fnirt/index.md) and its successor [MMORF](registration/mmorf.md) are high-performance and robust non-linear registration tools.

FSL also includwes [MSM](registration/msm.md), which implements an alternative surface-based registration framework, and is a core component of the [Human Connectome Project (HCP)](https://www.humanconnectome.org/) processing pipeline.

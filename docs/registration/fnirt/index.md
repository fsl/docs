# FNIRT

<div id="pagetoc"
     start="2"
     end="2"
     offset="0"
     ordered="false">
</div>

## Research Overview

It is frequently desirable to register different brains to each other, or to register a set of brains to a common space/template. This is for example the case when one wants to pool fMRI results across several subjects at a second level or when wanting to compare fractional anisotropy ([TBSS](diffusion/tbss.md)) or tissue composition ([FSLVBM](structural/fslvbm.md)) between groups. Registration algorithms can be divided into linear and non-linear depending on the type of deformations they permit. [FLIRT](registration/flirt/index.md) is an example of a software that performs linear registration, meaning that it will translate, rotate, zoom and shear one image to match it with another. Sometimes the differences between subjects are such that the linear transform is not sufficient to achieve good registration. The local deformations permitted by a non-linear method may then do a better job.

| flirted subject #1 | flirted subject #2 | MNI152 T1 | fnirted subject #1 | fnirted subject #2 |
| :----------------: | :----------------: | :-------: | :----------------: | :----------------: |
| ![flirted subject 1](flirted_subj1.jpg) | ![flirted subject 2](flirted_subj2.jpg) | ![MNI152 T1](template.jpg) | ![fnirted subject 1](fnirted_subj1.jpg) | ![fnirted subject 2](fnirted_subj2.jpg) |

The example above demonstrates how a linear transform is unable to account for the local changes around ventricles and sulci caused by atrophy. While the non-linear model has done a reasonable job of compensating for this.

## References

There is no current journal paper specifically for FNIRT. We recommend that people cite the following technical report as well as the [main FSL overview paper](README.md):

Andersson JLR, Jenkinson M, Smith S (2010) Non-linear registration, aka spatial normalisation. [FMRIB technical report TR07JA2](https://www.fmrib.ox.ac.uk/datasets/techrep/)
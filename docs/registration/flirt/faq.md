# FAQ

<div id="pagetoc"
     start="2"
     end="3"
     offset="0"
     ordered="false">
</div>

## My FLIRT registration doesn't work well - what can I do?

There are many reasons why a registration may not work well. Here is a general checklist of things to test and try in order to improve the registration results (please do not post a query to the FSL email list about registration results until you have gone through this list):

- Check that the input image looks fine and that the voxel size is correct by (a) looking at the images with FSLeyes (none of the views should look squashed or stretched) and (b) checking the voxel dimensions (pixdim) with `fslhd` (Note: voxel size can be fixed using `fslchpixdim`)
- Remove non-brain structures with BET from both images (Note that small errors in the BET results will only have a very small impact on the registration quality)
- Use the image with the best contrast and resolution as the reference image. If this gives you the registration in the opposite direction than you wanted then the result can be easily inverted using `InvertXFM` or `convert_xfm`.
- For 2D images (single slices) you must use one of the valid 2D degrees of freedom options (or -2D and appropriate schedules from the command line - see below)
- If there is large bias field (slow intensity variation - especially near the end slices) then try using fast to create a restored image (one with no bias field) and then register using the restored image.
- If there are relatively small errors in some crucial region of interest (e.g. ventricles) then the registration may benefit from using cost function weighting to enhance the importance of this region. To do this a weighting image must be made which has the value of 1.0 everywhere except in the region of interest where a higher weight (e.g. 10.0) should be used. Using this weighting volume in either the GUI or command line registration calls should improve the fit in this region.

## What determines the FOV and voxel size in FLIRT?

The reference image in FLIRT determines the Field of View (FOV) and voxel size of the output image. This works in either registration mode (where it is finding the transformation that aligns the input and reference images) and also in applyxfm mode (where it is applying a saved transformation to the input image). Note that only in registration mode does it use the intensity information from the reference image. To apply saved transformations, the GUI ApplyXFM can also be used which provides the option of specifying the number of voxels and voxel size directly.

## What cost function and/or degrees of freedom (DOF) should I use in FLIRT?

There are two main types of cost function: intra-modal (least squares and normalised correlation) and inter-modal (correlation ratio and mutual information-based options). If you are registering two images of different modality then you must use an inter-modal cost function, whereas for images of the same modality either can be used, although the intra-modal options may be more accurate. Within each category there is not much to choose from - it is a practical, experience-based decision. The recommended options (to try first) are: correlation ratio (which is the default) for inter-modal and normalised correlation for intra-modal.

The choice of DOF (Degrees of Freedom) depends largely on whether the images to be registered are intra/inter-subject and small/large FOV. When the images are of the same subject then 6 DOF is appropriate for large FOV and 3 DOF is appropriate for small FOV/single slice. If the scanner voxel size may have changed (due to calibration shifts) then it is appropriate to use 7 DOF instead of 6 (or 4 instead of 3) to compensate for global scale changes. When the images are of different subjects, including registering to standard space, then 12 DOF is appropriate for large FOV, and 6 DOF (using [2D schedule files](#what-are-flirt-schedule-files)) for small FOV.

Note that for difficult registrations there is a translation only schedule file which is effectively 3 DOF, but only includes x,y,z translations. This is useful for obtaining initial position estimates when matching small FOV to large FOV, and can then be further refined.

## How do I transform a mask with FLIRT from one space to another?

Transforming masks with FSL requires a little extra care. To steps are needed: (i) transform the mask into the new space, and (ii) rethreshold the transformed image to make it into a binary mask again.

Masks can be transformed from one space to another by using either one of the command line tools flirt or applywarp, or the ApplyXFM GUI.

The threshold used (with fslmaths) should be set depending on the intended use of the output mask. These guidelines should help in determining the correct value to use:

- a threshold near 1 (say 0.9) is conservative
    - only voxels in the new space that overlap by 90% with the original mask will be included in the new binary mask
- a threshold near 0 (say 0.1) is liberal
    - any voxel in the new space that overlaps by 10% or more with the original mask will be included in the new binary mask
- a threshold of 0.5 will (approximately) preserve the size of the original mask
    - any voxel in the new space that overlaps by 50% or more with the original mask will be included in the new binary mask

The best choice of threshold will depend on the application in which the mask is being used. For example, if the mask is being used to quantify values within an ROI and it is important to not include contamination from surrounding areas, then a high threshold should be used. Alternatively, if the mask is being used to define an exclusion ROI then it may be better to have a liberal border, which can be achieved with a low threshold.

### Example 1: transforming a mask from standard space to highres space (linear registration)

```
flirt -in standard_mask -ref highres -applyxfm -init standard2highres.mat -out highres_mask
fslmaths highres_mask -thr 0.9 -bin highres_mask
```
Note that in this case either flirt, applywarp or the ApplyXFM GUI could have been used for the first step.

### Example 2: transforming a mask from standard space to highres space (nonlinear registration)

```
applywarp -i standard_mask -r highres -w standard2highres_warp -out highres_mask
fslmaths highres_mask -thr 0.9 -bin highres_mask
```
Note that in this case the applywarp command must be used since there is a nonlinear transformation (warp) involved. It is also necessary to have the warp in the correct direction (`standard2highres_warp` and not `highres2standard_warp`), but if only the opposite direction is available (e.g. `highres2standard_warp`) then this can be inverted using the invwarp command.

### Example 3: transforming a mask between different resolution versions of standard space (e.g. 3mm to 2mm)

```
flirt -in mask3mm -ref $FSLDIR/data/standard/MNI152_T1_2mm -applyxfm -usesqform -out mask2mm
fslmaths mask2mm -thr 0.5 -bin highres_mask
```
Note that in this case the flirt command line must be used since the -usesqform flag aligns the images based on standard space coordinates, and not using a prior transformation matrix or warp.

In each case the transformation (flirt, applywarp or ApplyXFM GUI) forces the output to be floating point, even when the input is integer. Thresholding and binarising is done by fslmaths in the second call.

## How do I do 2D (or limited DOF) registration with FLIRT?

A simple 2D registration (including translation in x and y and rotation about the z axis) can be easily acheived by selecting the 2D-2D rigid body (3 parameter model) option under model/DOF in the GUI, or by using the -2D option on the command line (note that the dof does not need to be set in this case).

Registrations with different numbers of DOF or different combinations of parameters can only be achieved using schedule files and the command line version of flirt. If the input images are 2D it is still necessary to use the -2D option as well. For more details see the section on available [schedule files](#what-are-flirt-schedule-files).

Note that when the FOV is limited, but still 3D (multiple slices) then 2D or limited DOF transformations are normally required in order for the registration to be robust. In this context a limited FOV is normally around 50% brain coverage or less.

## Can I register to an image but use higher/lower resolution (voxel size)?

The registration and resampling stages in FLIRT are totally independent. In the registration stage it tries to find the transformation that best aligns the images, using a customised global optimisation technique that operates over multiple resolutions. Once the best transformation has been found the original input image is resampled, using the transformation found previously, to match the reference image. That is, the final output image will contain intensities derived from the input image but will have a Field Of View (FOV) and voxel size that matches the reference image. If a higher or lower resolution version of the final image is required it is necessary to save the transformation from the registration stage and then apply it in a separate stage where a new reference is used to specify the desired voxel size and FOV.

In the FLIRT GUI, the transformation is automatically saved in a file with an extension of .mat and this transformation can be applied to the input image (for resampling) with the GUI ApplyXFM which allows the output image voxel size and FOV to be specified either directly or by using a reference image with the appropriate size. Note that if a reference image is used it does not have to be the same image as in the registration and in fact the contents of the image (the intensities) are not used at all - only the voxel size and FOV are used.

At the command line, the transformation can be saved using the -omat option. This file can then be used for resampling by specifying it with the -init and -applyxfm options. That is, the resampling is done using flirt with the following syntax:

```
flirt -in inimage -out outimage -ref refimage -applyxfm -init savedtransform.mat
```

In this form the reference image is used to specify the voxel size and FOV only - all intensities within it are ignored. To create a reference image of the appropriate size, if none already exists, use fslcreatehd to make a blank image (one filled with zeros) of appropriate dimensions. Note that in previous versions fslcreatehd did not create an image, only the .hdr part of an Analyze file.

Note that when changing the FOV rather than the voxel size, the bottom left corner remains fixed. Hence, resampling to a smaller FOV will tend to cut-off the portions of the image with large x, y or z coordinates (near the right/top). In order to resample to a smaller FOV but keep say the Centre of Volume (COV) in the centre of both images it is necessary to add an extra translation to the transformation file. This can be done by adding the appropriate offsets (in mm) to the values in the right hand column (first row is x, second is y, third is z) of the transformation (.mat) file - which is in plain text. The appropriate offset to keep the COV constant is half of the difference in the respective FOVs (in mm).

## How do I do a two-stage registration using the command line?

The command line calls made in a two-stage registration of imageA to imageB to imageC are as follows:

```
flirt [''desired options''] -in ''imageA'' -ref ''imageB'' -omat ''transf_A_to_B.mat''
flirt [''desired options''] -in ''imageB''-ref ''imageC'' -omat ''transf_B_to_C.mat''
convert_xfm -omat ''transf_A_to_C.mat'' -concat ''transf_B_to_C.mat'' ''transf_A_to_B.mat''
flirt -in ''imageA'' -ref ''imageC'' -out ''imageoutput'' -applyxfm -init ''transf_A_to_C.mat''
```

The above steps perform two registrations (the first two steps) saving the respective transformations as .mat files, then concatenate the transformations using `convert_xfm,` then apply the concatenated transformation to resample the original image using flirt. Note that the first two calls to flirt would normally require the cost function or degrees of freedom (dof) to be set in the desired options. In the final call to flirt the option -interp is useful for specifying the interpolation method to be used (the default is trilinear).

Also note that the FLIRT GUI outputs the command line calls used to effect the two stage registration, and will be similar to the above, although they will include specification of many of the default settings.

## What are FLIRT schedule files?

A FLIRT schedule file controls the way that the optimisation is run inside FLIRT. It consists of a list of statements in a basic scripting language that was created for FLIRT. Several schedule files are contained in the directory $FSLDIR/etc/flirtsch and provide different levels of degrees of freedom control, such as specific settings for 2D registrations. It is only possible to perform 2D registration on the command line using schedule files (via the -schedule option). A list of currently provided schedule files is:

- sch2D_3dof - standard 2D registration with 3 DOF (1 rotation + 2 translation)
- sch2D_5dof - expanded 2D registration with 5 DOF (1 rotation + 2 translation + 2 scale)
- sch2D_6dof - expanded 2D registration with 6 DOF (1 rotation + 2 translation + 2 scale + 1 skew)
- sch3Dtrans_3dof - translation only 3D registration (x, y and z translation searched together)
- xyztrans.sch - same as sch3Dtrans_3dof
- ztransonly.sch - single z-axis translation registration
- simple3D.sch - reduced search form of normal 3D schedule (close to local optimisation only)
- pairreg*.sch - used for siena/sienax to register using the skull scaling as a constraint

No support is provided for users to write their own schedule files, but [some limited documentation](registration/flirt/schedule.md) is available for those who want to understand more or try their hand at writing new schedule files.

## How do I get the value of the cost function?

You can measure the cost function between two images using:

```
flirt -in inpimage -ref refimage -schedule $FSLDIR/etc/flirtsch/measurecost1.sch -init inp2ref.mat
```

You can also select any cost (similarity) metric you want by adding the -cost option.

The cost value is the first number of the first line that is printed. If you want to automatically select only this then you can do:

```
flirt .... | head -1 | cut -f1 -d' '
```

with the above flirt command.

## How do I make my own study-specific template image with FLIRT?

A study-specific template is an average image created from a set of structural images which represent the particular study group. For the population in general the standard template image, created from 152 individuals, is supplied as $FSLDIR/etc/standard/avg152T1. However, for many applications this image will not represent the study group very accurately, say due to differing age or disease. Therefore it is desirable in these cases to create a study-specific template image. An affine template image (as is the avg152T1) can be created using the following steps:

- Choose a reference image from among the set
- Register each image in the set to the reference image, using flirt, and saving the output images
    - e.g. `flirt -in im3 -ref imref -dof 12 -out imout3 -omat im3_to_imref.mat`
- Average the output images together using fslmaths
    - e.g. `fslmaths imout1 -add imout2 -add imout3 -add imout4 ... -add imoutN -div N avg_im -odt float`

The final image, `avg_im` is the average, or template, image.

Note that it is advisable to check each registration manually, "by eye", in order to make sure that the average template image is not corrupted with a poor registration result.

More sophisticated schemes for making the target image less sensitive to the choice of the reference image can also be carried out. The simplest of these is to redo the registration and averaging using the initial average image, from above, as the reference. This process can be iterated several times if desired. Another alternative is to perform all pair-wise registrations and choose the best consensus registration, possibly using `convert_xfm` and rmsdiff to detect and reject "outlier" registrations.

## What is the format of the matrix used by FLIRT, and how does it relate to the transformation parameters?

Initially, please note that the coordinate system used internally within FSL is a scaled mm system (the voxel coordinates multiplied by the voxel dimensions) but is also of fixed handedness, which is achieved by swapping the x voxel coordinate (x goes to N-1-x) if the voxel-to-mm mapping in the qform/sform has positive determinant.

The affine matrix used by FLIRT is constructed in the following way:

```
mat = rot_mat * skew_mat * scale_mat
```

The scale_mat has the form:

```
sx 0  0  0
0  sy 0  0
0  0  sz 0
0  0  0  1
```

where sx, sy, sz are the three scaling parameters.

The skew_mat has the form:

```
1   kxy kxz 0
0   1   kyz 0
0   0   1   0
0   0   0   1
```

where kxy, kxz, kyz are the three skew parameters.

The rot_mat has the form:

```
rot_mat = Rx*Ry*Rz
```

where Rx has the form:

```
1  0  0  0
0  c  s  0
0 -s  c  0
0  0  0  1
```

where c=cos(theta) and s=sin(theta) and similarly for Ry and Rz (with appropriate axis/dimension swapping).

The FSL convention for transformation matrices uses an implicit centre of transformation - that is, a point that will be unmoved by that transformation, which is an arbitrary choice in general. This arbitrary centre of the transformation for FSL is at the mm origin (0,0,0) which is at the centre of the corner voxel of the image.

When using the transformation parameters from FLIRT, there is an additional complication in that the parameters are calculated in a way that uses a different centre convention: the centre of mass of the volume. The effect of this is that each of the three matrices above end up with an adjustment in the fourth column (top three elements only) that represents a shift between the corner origin and the centre of mass, while the rest of the matrix (first three columns) is unaffected. Once that is done the matrices are multiplied together, as indicated above, and you get your final matrix.

The full 12 parameters are often listed by FLIRT in the following order:

    rx ry rz tx ty tz sx sy sz kxy kxz kyz

where rx, ry, rz are the rotation angles in radians (for the matrices Rx, Ry and Rz respectively) and tx, ty, tz are the translations in mm.

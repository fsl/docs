# FLIRT User Guide

<div id="pagetoc"
     start="2"
     end="3"
     offset="0"
     ordered="false">
</div>

## Available GUIs

FLIRT comes with a main GUI as well as three supporting guis:

- ApplyXFM - for applying saved transformations and changing FOVs
- InvertXFM - for inverting saved transformations
- ConcatXFM - for concatenating saved transformations

These support GUIs are self-explanatory and perform functions that are detailed in the command line versions. Only the main FLIRT GUI is described in the rest of this page.

## Main FLIRT GUI

### Main Options

The simplest use of FLIRT is to register two single volumes together. This is done by choosing the **Input image -> Reference image** mode in the top box, then filling in the **Reference image** and **Input image** boxes with the appropriate images. The result is a registered image which will be saved to the location specified in the **Output image** box. All other options/boxes can be left at their default values.

The second mode of operation is a two stage registration which takes an input **Low res image** and two target images. It initially registers the low res image to a **High res image** and then registers this high res image to the final **Reference image**. The two resulting transformations are concatenated and then applied to the original low res image to create an **Output image** that is a version of the low res image transformed (resliced) to the reference image space.

### Secondary Images

Apply the estimated transform to other (secondary) images, which were originally aligned with the input/low-res image, in order to align them with the reference image.

### Model/DOF

Restrict the transformation type. For 3D to 3D mode the DOF can be set to 12 (affine), 9 (traditional), 7 (global rescale) or 6 (rigid body). In 2D to 2D mode only 3 DOF (rigid body) transformations are allowed.

## Advanced Options

The four categories of options are:

- **Search** - select the angular range over which the initial optimisation search stage is performed.

- **Cost Functions** - select the desired cost function from a range of inter- and intra-modal functions.

- **Interpolation** - select the interpolation method to be used in the final (reslice) transformation (it is not used for the estimation stage - trilinear interpolation is always used for the estimation of the transformation). The options for this final interpolation method are: Tri-Linear; Nearest Neighbour and * Sinc. If Sinc is chosen, further window parameters (type of windowing function and window width) can also be specified.

- **Weighting Volumes** - impose voxel-wise weighting to reference and/or input images, to affects the cost function. The weighting images must be the same size as the image they are weighting (e.g. refweight and reference images) and the voxel values of the weighting image represent how much weighting that particular voxel is given in the cost function. Therefore, by setting weights to zero, some areas of the image can be effectively ignored, which is useful in masking out pathologies so that they do not affect the registration. In this way very accurate registrations can be made between pathological and "normal" images. This cannot be achieved by masking the images prior to registration, as that induces artificial boundaries which bias the registration. Furthermore, some areas can be given extra weighting (such as the ventricles) so that the registration is most accurate near these structures, but still uses information from the rest of the image (e.g. the cortical surface) to improve the robustness of the registration.

![A figure explaining how FLIRT transforms work](./flirt_user_guide_xfmhelp.gif)

## FLIRT Command-line Program and Utilities

This document gives a brief description of flirt and the various command-line programs available in the FLIRT component of FSL. A description of the available GUI interfaces is also available.

For each of the programs described here, a usage message which describes the full list of available options can be obtained by running the program with no options.

### flirt

`flirt` is the main program that performs affine registration. The main options are: an input (`-in`) and a reference (`-ref`) volume; the calculated affine transformation that registers the input to the reference which is saved as a 4x4 affine matrix (`-omat`); and output volume (`-out`) where the transform is applied to the input volume to align it with the reference volume. In addition, FLIRT can also be used to apply a saved transformation to a volume (`-applyxfm`, `-init` and `-out`) or to apply a transform that aligns the NIFTI mm coordinates (`-applyxfm`, `-usesqform` and `-out`; but not `-init`). For these usages the reference volume must still be specified as this sets the voxel and image dimensions of the resulting volume.

#### Features:

_Cost Function Weighting_

Weighting volumes can be specified using `-refweight`, `-inweight` (or both). This allows the cost function to have a different weighting at each voxel, which is useful for excluding areas (weight=0) of no interest, or increasing the weighting around important structures such as the ventricles. Note that this is different from masking the original images, as masking introduces artificial boundaries whereas weighting does not.

_Degrees of Freedom_

Choose from 6,7,9 or 12 Degrees of Freedom (DOF) for full 3D registrations. Also includes a 3DOF 2D-to-2D registration mode which is selected using the -2D option. Note that it does not perform any search in 2D mode, and cannot deal with 2D to 3D registrations. More flexible DOF options are provided by the specific schedule files provided in `$FSLDIR/etc/flirtsch`.

_Interpolation Methods_

This includes Nearest Neighbour, a family of Sinc-based methods (three window types - rectangular, Hanning and Blackman) with configurable window width, and spline (a highly efficient method, with similar output characteristics to sinc). The interpolation is only used for the final transformation (and in `applyxfm`), not in the registration calculations.

_Cost Functions_

This includes the within-modality functions Least Squares and Normalised Correlation, as well as the between-modality functions Correlation Ratio (the default), Mutual Information and Normalised Mutual Information. In addition, there is the [BBR](registration/flirt/bbr.md) cost function which utilises a segmentation of the reference image to define a boundary, and it is the intensity differences in the input image, across the transformed boundary, that contribute to the cost.

### epi_reg

This is a script designed to register EPI images (typically functional or diffusion) to structural (e.g. T1-weighted) images. The pre-requisites to use this method are: (1) a structural image that can be segmented to give a good white matter boundary; and (2) an EPI that contains some intensity contrast between white matter and grey matter (though it does not have to be enough to get a segmentation).

This script will either use an existing white-matter segmentation of the structural image, or create one itself, to define a white-matter boundary. To use an existing white-matter segmentation you can use the `--wmseg` option. Alternately, there needs to be an image with the same basename as the input `--t1brain` image, but ending with `_wmseg`. We recommend that the structural image is bias-corrected separately beforehand if there is obvious bias field present.

The script is also capable of using fieldmaps to perform simultaneous registration and EPI distortion-correction. For this it requires extra images (in the format as given by [`fsl_prepare_fieldmap`](registration/fugue.md#making-fieldmap-images-for-feat)) as well as extra information about the EPI sequence (which your operator/radiographer/technician should be able to provide, but make sure you record the values). The inputs echospacing and pedir both refer to the EPI image (not the fieldmap) and are the same as required for [FEAT](task_fmri/feat/index.md), but be careful to use the correct units.

```
Usage: epi_reg [options] --epi=<EPI image> --t1=<wholehead T1 image> --t1brain=<brain extracted T1 image> --out=<output name>

Optional arguments
  --fmap=<image>         : fieldmap image (in rad/s)
  --fmapmag=<image>      : fieldmap magnitude image - wholehead extracted
  --fmapmagbrain=<image> : fieldmap magnitude image - brain extracted
  --gdc=<image>          : Gradient-distortion corection warpfield
  --wmseg=<image>        : white matter segmentation of T1 image
  --echospacing=<val>    : Effective EPI echo spacing (sometimes called dwell time) - in seconds
  --pedir=<dir>          : phase encoding direction, dir = x/y/z/-x/-y/-z
  --weight=<image>       : weighting image (in T1 space)
  --nofmapreg            : do not perform registration of fmap to T1 (use if fmap already registered)
  --noclean              : do not clean up intermediate files
  -v                     : verbose output
  -h                     : display this help message

e.g.:  epi_reg --epi=example_func --t1=struct --t1brain=struct_brain --out=epi2struct --fmap=fmap_rads --fmapmag=fmap_mag --fmapmagbrain=fmap_mag_brain --echospacing=0.0005 --pedir=-y

Note that if parallel acceleration is used in the EPI acquisition then the *effective* echo spacing is the actual echo spacing between acquired lines in k-space divided by the acceleration factor.
```

### convert_xfm

`convert_xfm` is a utility that is used to convert between different transformation file formats. It can read and write ascii 4x4 matrices. In addition, it can be used to concatenate two transforms (using `-concat` with the second transform) or to find the inverse transformation (using `-inverse`).

### img2imgcoord

`img2imgcoord` is a utility that calculates the corresponding coordinate positions (in voxels or mm) within a destination volume given the original coordinates in the source volume and the transformation from the source to the destination volume (either linear/affine transformations or non-linear warps). This is useful for finding corresponding anatomical/functional locations. If the option `-mm` is used then both input and output coordinates will be in mm coordinates, otherwise (with `-vox`) both coordinates will be in voxel coordinates. For conversion between voxel and mm coordinates it is necessary to use either `img2stdcoord` or `std2imgcoord` (see below).

Note that the source coordinates can either be input via a file or via a pipe (and for the latter the "-" symbol is used as the filename). The format in either case is three numbers per line, space separated.

Example usage:

`cat coordfile.txt | img2imgcoord -src example_func -dest highres -xfm example_func2highres.mat -`

_Warning_: currently there is a buglet in the coordinate conversion tools img2imgcoord, img2stdcoord and std2imgcoord such that the last coordinate is repeated if the input is a file. To avoid this use the pipe input format or suppress the final line:

`cat coordfile.txt | img2imgcoord -src example_func -dest highres -xfm example_func2highres.mat -`
`img2imgcoord -src example_func -dest highres -xfm example_func2highres.mat coordfile.txt | sed '$d'`

### img2stdcoord

`img2stdcoord` is a similar utility to img2imgcoord except that the destination volume coordinates are treated as mm (standard space) coordinates. This uses the "standard space" image's coordinate mapping information (qform/sform in the NIfTI format) to convert to mm coordinates. The coordinates for the source image can be either in voxel coordinates (default, or by explicitly using `-vox`) or in mm coordinates (using `-mm`). Example usage:

`echo 92 80 59 | img2stdcoord -img highres -std standard -warp highres2standard_warp -`

This is useful in conjunction with the tool [Atlasquery](utilities/dataset_clitools.md#atlasq) that can report atlas labels for coordinates in standard space.

It is no longer the case (now that FSL uses the NIfTI format) that the image specified with `-std` need to be a "standard space" image. Any image can be used, and the mapping to mm coordinates will be done as it is done in `FSLeyes` (using the qform/sform information or an FSL default if they are not set). This utility, and `std2imgcoord` are therefore useful for converting between voxel and mm coordinates within the same image, as well as for mapping coordinates between spaces.

Converting from voxel to mm coordinates within the same image can be done with the command:

`echo X Y Z | img2stdcoord  -img IMAGENAME -std IMAGENAME -vox -`

where here we convert just a single coordinate (replace X, Y, Z, and IMAGENAME with appropriate numbers/filename) but multiple coordinates can also be done by replacing the echo X Y Z with cat COORDFILE (again, replacing COORDFILE with the appropriate filename).

### std2imgcoord

`std2imgcoord` is the complementary utility to `img2stdcoord`. It works the same way but transfers coordinates from "standard space" to the other image (IMG) space. It can also convert between mm and voxel coordinates within the same image. See the entry on `img2stdcoord` above.

### applyxfm4d

`applyxfm4D` is a utility that transforms a 4D time series by applying known affine transformations to them. It can be used with a single transformation applied to all, or using a directory of transformation files in the form MAT_XXXX (where XXXX stands for the volume number, starting with 0000). Sinc interpolation is used internally. Appropriate options (`-applyxfm` and `-init`) to FLIRT can be used to apply transformations to single volumes with other interpolation methods. A reference volume is required in order to determine the matrix size and FOV for the final volume. If the required matrices do not start with MAT_ a different prefix can be specified with the `-userprefix` option.

### rmsdiff

`rmsdiff` is a utility that calculates the Root Mean Square deviation (in millimetres) between two transformations. That is, it compares two transformations (normally two possible registrations of the same volume pair) to see how much they differ. This is useful to compare alternative registrations. It calculates the average using an analytic formula applied over an 80mm sphere with the origin at the centre of gravity of the image (for which it requires the input image to be specified).

An alternative usage is to provide a mask (as the fourth argument) which is then used to specify the ROI, rather than using the sphere. In this mode it gives two outputs: (1) the maximum movement (in mm) over all voxels in the ROI/mask; and (2) the RMS movement (in mm) over all voxels in the ROI/mask. With this calculation it does not require an origin to be set.

### avscale

`avscale` is a utility that displays the decomposed elements of an affine matrix. It displays the rotation/translation matrix, the individual axis scalings, the individual skews, the average scaling, and the forward and backward halfway transformations. In order to set the centre of rotation it requires the input volume (also called the reslice volume).

## FLIRT Examples

The simplest usage of `flirt` is to register two images together as:

`flirt -in invol -ref refvol -out outvol -omat invol2refvol.mat -dof 6`

where `invol`, `refvol`, `outvol` are the input, reference and output volume filenames respectively, `invol2refvol.mat` is the filename for the saved ascii transformation matrix. Naturally, any filenames you wish to use can be chosen.

Note that `-dof` was used as the default would otherwise be 12. The default cost function is Correlation Ratio, which normally works well for all images.

Also note that the `.mat` extension is not compulsory and any filename and extension can be used. The transformation files are simply stored as ascii matrices, and so other conventions can be used (which might be better for MATLAB users to avoid confusion) although the .mat extension is the default within FEAT.

To apply a saved transformation to another image use:

`flirt -in newvol -ref refvol -out outvol -init invol2refvol.mat -applyxfm`

Note that the previous transformation matrix is used with the `-init` command and that the size of the otput volume is determined by `refvol` although its contents are **not** used.

To perform a 3 DOF 2D (single slice) registration:

`flirt -in inslice -ref refslice -out outslice -omat i2r.mat -2D`

To perform a 6 DOF 2D registration:

`flirt -in inslice -ref refslice -out outslice -omat i2r.mat -2D -schedule ${FSLDIR}/etc/flirtsch/sch2D_6dof`

The schedule file specifies what transformations/DOF are allowed and how the optimisation is performed. Note that several other schedule files could be used - including 3D translation only schedules etc. These are all stored in `${FSLDIR}/etc/flirtsch`

## CONVERT_XFM Examples

To invert a saved transformation:

`convert_xfm -omat refvol2invol.mat -inverse invol2refvol.mat`

To concatenate two transformations:

`convert_xfm -omat AtoC.mat -concat BtoC.mat AtoB.mat`

Note that the transform after the `-concat` is treated as the second transformation in the concatenation.

## Using FLIRT to Register a Few FMRI Slices

### Introduction

If you need to take an FMRI data set where you only have a few slices (ie the field-of-view - FOV - is small in Z) then it is very hard to get a good registration with either the subject's structural or directly to a standard space image. This page describes our recommendations in such cases.

- Take your FMRI data, with only a few slices. We will refer to an example volume from this time series as **example_func**.
- Take a single whole-brain "functional" image (let's call this **whole_func**). This will normally be the same kind of image as **example_func** (eg EPI), but will cover the whole brain. It should contain, as a subset of its slices, the same slices as un **example_func**. In general there should be no rotation between **example_func** and **whole_func**.
- Now (optionally) take a high-resolution structural image, e.g. T1-weighted (let's call this **highres**).
- Finally, identify your standard space image (let's call this **standard**).

Now you are ready to register these all together.

### Registration

- First, register example_func to whole_func:

`flirt -ref whole_func -in example_func -schedule ${FSLDIR}/etc/flirtsch/ztransonly.sch -out example_func2whole_func -omat example_func2whole_func.mat`

The custom FLIRT schedule file `ztransonly.sch` allows only translations in Z (in order for the registration to be robust), so there must be no rotation or within-slice translation between these images.

- Now register whole_func to highres:

`flirt -ref highres -in whole_func -out whole_func2highres -omat whole_func2highres.mat`

- Now register highres to standard:

`flirt -ref standard -in highres -out highres2standard -omat highres2standard.mat`

- Now combine the transforms:

`convert_xfm -concat whole_func2highres.mat -omat example_func2highres.mat example_func2whole_func.mat`
`convert_xfm -concat highres2standard.mat -omat example_func2standard.mat example_func2highres.mat`

- Now you could use these to transform stats images (eg produced by FEAT) in the space of the original func data into standard space:

`flirt -ref standard -in thresh_zstat1 -applyxfm -init example_func2standard.mat -out thresh_zstat12standard`

### Using with FEAT

If you want to put these transforms into a FEAT directory so that running group stats with FEAT will work well, then do the following (instead of using the simple default registration carried out by FEAT): Run a first level FEAT analysis without registration - this creates an output FEAT directory. Place in your FEAT directory appropriate images called `whole_func.hdr/.img`, `highres.hdr/.img` and `standard.hdr/.img`. (highres should probably have been brain-extracted already using BET, and standard should probably be `${FSLDIR}/data/standard/MNI152_T1_2mm_brain`)) Carry out the above `flirt` and `convert_xfm` commands exactly as written. This should leave you with correctly named transform files which FEAT will automatically use when carrying out group statistics.

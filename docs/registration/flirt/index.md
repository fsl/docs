# FLIRT

<div id="pagetoc"
     start="2"
     end="2"
     offset="0"
     ordered="false">
</div>

## Research Overview

FLIRT (FMRIB's Linear Image Registration Tool) is a fully automated robust and accurate tool for linear (affine) intra- and inter-modal brain image registration.

**New in FSL5**: the BBR method was implemented for improved EPI to structural registration, with built in fieldmap-based distortion-correction (it will also run without fieldmaps).

The different FLIRT programs are:

- Flirt - FLIRT GUI + InvertXFM, ApplyXFM, ConcatXFM
- flirt - command-line FLIRT program
- misc flirt utilities - associated command-line programs

## General Advice

Use the image with the best quality (tissue contrast and resolution) as the Reference image, otherwise poor registrations may occur (the inverse transformation can be found afterwards using the InvertXFM gui or the convert_xfm command-line utility). Do use the default settings (especially in the GUI advanced options) as they have been carefully chosen. Do use it for registering to the MNI standard brains as it has been extensively tested with these images. For images showing pathology, consider using binary cost function weighting images - a zero for areas of a pathological nature and one elsewhere (do not put zero outside the brain). Consider using the Group facility (GUI only) for running the registration on a collection of images (this is also easily achieved with the command-line version by scripting).

## Template Images

Both linearly and non-linearly generated MNI152 template images (whole head, extracted brain, brain mask and skull) are included in `fsl/data/standard`, courtesy of the MNI.

## Non-linear Registration

Also see the FSL non-linear registration tool, [FNIRT](registration/fnirt/index.md). Note that this requires an initial affine registration to be performed first using FLIRT.

## References

If you use FLIRT in your research, please make sure that you reference at least the first of the articles listed below, and ideally the complete list.

- Jenkinson, M., Bannister, P., Brady, J. M. and Smith, S. M. Improved Optimisation for the Robust and Accurate Linear Registration and Motion Correction of Brain Images. NeuroImage, 17(2), 825-841, 2002.
- Jenkinson, M. and Smith, S. M. A Global Optimisation Method for Robust Affine Registration of Brain Images. Medical Image Analysis, 5(2), 143-156, 2001.
- Greve, D.N. and Fischl, B. Accurate and robust brain image alignment using boundary-based registration. NeuroImage, 48(1):63-72, 2009.

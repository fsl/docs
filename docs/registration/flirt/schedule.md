# FLIRT Schedule Language Reference

<div id="pagetoc"
     start="2"
     end="3"
     offset="0"
     ordered="false">
</div>

This document describes the commands that can be used within a schedule file for flirt via the -schedule option.

## Variables

There are two types of data variables that are manipulated in a schedule file: scalar and matrix data.

### Scalar data

Any constant can be used as a scalar value in the schedule file. In addition token names can be used, and the following token names are currently defined:

```
MAXDOF 
```

the user-defined maximum degrees of freedom (via the -dof option).

```
MINSAMPLING 
```

the minimum sampling that is sensible, as calculated from the input volumes - defined as Max(Min(refvol voxel dims),Min(testvol voxel dims)).

### Matrix data

Most data in a schedule file is manipulated in matrix form. Each matrix has 17 columns, with the first column representing the cost function value and the remaining 16 a row-major reshaped version of the 4 by 4 affine matrix that corresponds to that cost function measurement.

The matrices that can be accessed are: S P U UA UB ... UZ. Note that S P and U have special functions while UA UB ... UZ are provided for user manipulation.

In addition, a subset of rows of a given matrix can be used in place of any matrix. The form of this reference is: mat:r1-r2 where mat is the matrix name (e.g. UB), r1 is the first row, and r2 the last row to be used. Note that some parts of this may be omitted so that mat:r1- specifies the matrix from row r1 to the end, and mat:r1 just specifies a single row ( r1). NB: The first row of any matrix has an index of 1.

Matrix U is special as it is used to store the results of the current optimisation.

Matrices S and P are used to store the (corresponding) results of the 8mm search algorithm.

## Commands

```
# 
```

Comment line

```
clear mat 
```

Clears the matrix mat.

```
copy mat1 mat2 
```

Copies (appends) the contents of matrix mat1 into matrix mat2. It does not affect the contents of matrix mat1.

```
print mat 
```

Prints the contents of matrix mat to the screen.

```
save mat filename 
```

Saves an ascii copy of the contents of matrix mat to the file named by filename.

```
read mat filename 
```

Reads an ascii file ( filename) and stores the contents in matrix mat.

```
printparams mat 
```

Prints the contents of matrix mat to the screen as parameter values (which are different from the row-major reshaped 4 by 4 affine matrix - see readparams).

```
saveparams mat filename 
```

Saves an ascii copy of the contents of matrix mat, as parameter values, to the file named by filename.

```
readparams mat filename 
```

Reads an ascii file (filename) where each line represents 1 cost value followed by 12 parameter values (a representation of the rotations, translations, scales and skews, which is different from the 4x4 matrix element representation). It then converts these parameters to the reshaped affine matrices and stores the contents in matrix mat. The order of parameters is this: rx ry rz tx ty tz sx sy sz kxy kxz kyz (for rotations, translations, scales and skews respectively).

```
setrow mat v1 v2 ... v16 
```

Adds a row to the end of matrix mat with the value given by the reshaped matrix: v1 v2 ... v16. Note that only the 16 matrix values are used, not the cost value (which is initialised to 0). These are the elements of the 4x4 affine matrix (reshaped) and not the parameter representation.

```
aligncog mat 
```

Takes all transforms in mat, aligns the cog returning the result in U.

```
alignpaxes mat 
```

Takes all transforms in mat, aligns the principle axes, returning the result in U (NB: previous transform values are ignored).

```
setoption option v1 v2 ... 
```

Sets the value of certain defined options. The name of the option is specified as the first parameter, then all remaining parameters are treated as a vector of scalars. Note that no matrix arguments can be used here. At present the available options are smoothing, fuzzyfraction, tolerance, rescaletolerance, paramsubset, nosubset and boundguess.

```
sort mat 
```

Sorts the rows of matrix mat into ascending order based on the first column entries of each row (the cost function values).

```
dualsort mat1 mat2 
```

Sorts the rows of matrix mat1 and mat2 into ascending order based on the first column entries mat1 only (the cost function values).

```
search [ dof ] 
```

Performs the 8mm search algorithm on the currently defined image pair. The result is returned in matrices S and P where S contains the optimised values (which are sorted), and P the pre-optimised values from the search grid. The rows of the two matrices correspond to each other. The optional argument dof specifies the number of degrees of freedom for the search (only 6 or 7 are currently sensible).

```
optimise dof mat [ perturbation_vector rel/abs max_iterations ] 
``` 

Performs a single optimisation on the currently defined image pair. Number of degrees of freedom are specified by dof and initial parameter values are taken from mat. In addition a perturbation vector can be applied to the parameters, using either relative rel or absolute abs addition (relative is in terms of specially chosen parameter perturbations so that a 1 unit relative change should be a small but measurable change). The perturbation vector may be of any length, terminated by either rel or abs - if the vector is longer than the number of parameters, extra values are ignored. The final parameter (which must follow either rel or abs) is the maximum number of iterations that the optimisation is allowed. Note that the minimum of dof and the user-defined maximum degrees of freedom ( MAXDOF) will always be used.

```
measurecost dof mat [ perturbation_vector rel/abs ] 
```

Measures (or re-measures) the cost function specified by the transformations stored in the matrix mat. It stores the results in U. Other syntax is the same as for optimise.

```
setscale scale 
```

Sets the scaling (subsampling) of the current image pair. Permissable values of scale are: 8, 4, 2 and 1.

```
if val1 op val2 
```

Determines whether the next line only is executed. The test condition compares two scalars: val1 and val2 using the boolean operation op. There are 6 permissable values for op, which are: < , > , == , != , <= , >=

## Example

The default flirt schedule file is:

```
# 8mm scale
setscale 8
setoption smoothing 8
clear S
clear P
search
# 4mm scale
setscale 4
setoption smoothing 4
clear U
clear UA
clear UB
clear US
clear UP
# remeasure costs at this scale
measurecost 7 S 0 0 0 0 0 0 rel
copy U US
clear U
measurecost 7 P 0 0 0 0 0 0 rel
copy U UP
dualsort US UP
# optimise best 3 candidates (pre and post 8mm optimisations)
clear U
optimise 7 US:1-3  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 4
optimise 7 UP:1-3  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 4
# also try the identity transform as a starting point at this resolution
clear UQ
setrow UQ  1 0 0 0  0 1 0 0  0 0 1 0  0 0 0 1
optimise 7 UQ  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 4
sort U
copy U UA
# select best 4 optimised solutions and try perturbations of these
clear U
copy UA:1-4 U
optimise 7 UA:1-4  1.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 4
optimise 7 UA:1-4 -1.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 4
optimise 7 UA:1-4  0.0   1.0   0.0   0.0   0.0   0.0   0.0  rel 4
optimise 7 UA:1-4  0.0  -1.0   0.0   0.0   0.0   0.0   0.0  rel 4
optimise 7 UA:1-4  0.0   0.0   1.0   0.0   0.0   0.0   0.0  rel 4
optimise 7 UA:1-4  0.0   0.0  -1.0   0.0   0.0   0.0   0.0  rel 4
optimise 7 UA:1-4  0.0   0.0   0.0   0.0   0.0   0.0   0.1  abs 4
optimise 7 UA:1-4  0.0   0.0   0.0   0.0   0.0   0.0  -0.1  abs 4
optimise 7 UA:1-4  0.0   0.0   0.0   0.0   0.0   0.0   0.2  abs 4
optimise 7 UA:1-4  0.0   0.0   0.0   0.0   0.0   0.0  -0.2  abs 4
sort U
copy U UB
# 2mm scale
setscale 2
setoption smoothing 2
clear U
clear UC
clear UD
clear UE
clear UF
# remeasure costs at this scale
measurecost 7 UB 0 0 0 0 0 0 rel
sort U
copy U UC
clear U
optimise 7  UC:1  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 4
copy U UD
setoption boundguess 1
if MAXDOF > 7
 clear U
if MAXDOF > 7
 optimise 9  UD:1  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 1
copy U UE
if MAXDOF > 9
 clear U
if MAXDOF > 9
 optimise 12 UE:1  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 2
sort U
copy U UF
# 1mm scale
setscale 1
setoption smoothing 1
setoption boundguess 1
clear U
# also try the qsform as a starting point at this resolution
setrowqsform UF
optimise 12 UF:1-2  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 1
# in addition, try qsform as the final transformation, not just an initialisation
clear UG
setrowqsform UG
measurecost 12 UG:1  0.0   0.0   0.0   0.0   0.0   0.0   0.0  rel 1
sort U
```
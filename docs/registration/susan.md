# SUSAN

## Research Overview

SUSAN noise reduction uses nonlinear filtering to reduce noise in an image (2D or 3D) whilst preserving the underlying structure. It does this by only averaging a voxel with local voxels which have similar intensity.

If you use SUSAN in your research, please make sure that you reference the article:

> S.M. Smith and J.M. Brady. SUSAN - a new approach to low level image processing. International Journal of Computer Vision, 23(1):45–78, May 1997.

## Susan GUI

### Main Options

**Dimensionality**: by default a 3D image will be processed in full 3D mode. If you want to process each slice separately, select 2D.

**Brightness threshold**: this allows SUSAN to discriminate between noise and the underlying image. Ideally, the value should be set greater than the noise level and less than the contrast of the underlying image. Edges of contrast smaller than this threshold will tend to be blurred by SUSAN whereas those of greater contrast will not be.

**Mask SD**: this determines the spatial extent of the smoothing. The mask is basically Gaussian with standard deviation (in image units - e.g. mm) set by the user. However, for a small, fast, flat response 3x3 or 3x3x3 voxel mask, set SD to 0.

Note that for small spatial extents, Susan will automatically switch to a flat kernel to ensure that some smoothing occurs. This may result in an increase in apparent smoothing compared to a small Gaussian kernel.

### Advanced Options

**Use median when no neighbourhood is found**: by default, when the local neighbourhood of similar brightness voxels is empty, a local median filter is used. This allows the correction of impulse ("salt-and-pepper") noise. This feature can be turned off if desired. In this case, when no neighbourhood is found, the original intensity of the voxel of interest remains unchanged.

**Separate images to find USAN from**: if the local area to be smoothed over is to be found from an image (or images) other than the main input image, this can be input here.

### susan Command Line Program

This is the program called by the GUI. Type `susan` to get the usage help.
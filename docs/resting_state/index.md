# Resting state fMRI

Functional connectivity is typically defined as:

>_the observed temporal correlation (or other statistical dependencies) between two electro- or neurophysiological measurements from different parts of the brain._

For resting state fMRI this definition means that functional connectivity can inform us about the relationship between BOLD signals obtained from two separate regions of the brain. The underlying assumption is that if two regions show similarities in their BOLD signal variability over time, they are functionally connected.

There are a wide variety of different functional connectivity analyses, and new approaches are still being developed. These functional connectivity methods can be broadly divided into _voxel-based_ and _node-based_ methods. The key aspect that **voxel-based methods** have in common is that they all estimate a functional connectivity value for each voxel in the brain (i.e., they describe functional connectivity in terms of spatially distributed effects). Therefore, regardless of differences in how individual voxel-based methods estimate functional connectivity, all of these methods result in a map (or multiple maps) of the brain containing values for all voxels. As an alternative, **node-based methods** estimate connectivity patterns between predefined regions. Here, _nodes_ are different brain regions, and _edges_ are the connections (or connection strengths) between the nodes. Node-based connectivity analyses do not result in brain maps but are instead a form of graph-based connectivity modelling.

In FSL, [MELODIC](resting_state/melodic.md) offers the main voxel-based functional connectivity method and [FSLNets](resting_state/fslnets.md) offers the main node-based functional connectivity method. [Dual regression](resting_state/dualregression.md) can be used in conjunction with either MELODIC (to estimate and statistically compare subject-specific component maps) or with FSLNets (to estimate subject-specific node timeseries). Please find a brief summary introduction to each approach below, where more in-depth information for each tool is linked.

[Independent component analysis (ICA, performed in FSL using MELODIC)](resting_state/melodic.md) is a data-driven, exploratory method that is adopted in a wide variety of fields and applications. The aim of ICA is to decompose a multivariate signal into a set of features that represent some structure that is present in the data (called _components_). Hence, ICA assumes that the observed data is a mixture of multiple underlying components that cannot be directly observed, but that can be separated. When applying ICA, each resulting component is described as a spatial map (which reflects where in the brain a certain signal portion is being detected), and a timeseries (describing how the signal evolved over time).

[Node-based connectivity analyses (performed in FSL using FSLNets)](resting_state/fslnets.md) have several steps in common:

1. Defining the nodes, i.e., grouping voxels together into areas that are to be considered as functionally homogeneous regions.
2. Extracting the timeseries from each node (for example with dual regression). The timeseries represent the BOLD signal fluctuations over the course of the scan in each node.
3. Calculating the connectivity (_edges_) between all pairs of nodes using the extracted timeseries.
4. Building a **network matrix** (_netmat_). For example, if the node-based analysis contains 100 regions (_nodes_ of the graph), you can build a 100 by 100 matrix that describes all possible pair-wise connections (_edges_ of the graph). That is, the element on the 9th row and 25th column in the connectivity matrix describes the strength of functional connectivity between node 9 and node 25. A group-level node-based analysis typically has one network matrix per subject.

[Dual regression](resting_state/dualregression.md) uses the group-ICA maps and applies two subsequent regression analyses using the original preprocessed dataset from each subject in order to derive subject-specific maps. In order to statistically compare ICA components and detect differences in the resting state networks between subjects or groups of subjects, it is necessary to obtain subject-wise component maps. The idea behind dual regression is to use the group-ICA maps as a template model of the overall network structure within each subject, and to find the subject-maps that best fit this model.


> Some of the content in these pages is based on _An Introduction to Resting State fMRI Functional Connectivity_ by Janine Bijsterbosch, Stephen M. Smith, and Christian F. Beckmann, and published by Oxford University Press (2017).

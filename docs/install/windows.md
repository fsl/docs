# Installation on Windows


We recommend using Linux or macOS for using FSL. However FSL can be used on Windows via the Windows Subsystem for Linux (WSL, recommended) or by running the Linux version of FSL inside a virtual machine (VM).


## System requirements

- Windows 10 or newer. You need to have Windows 10 version 2004 (Build 19041) or higher. You can find out your Windows version / build by opening the *Settings* app, and navigating to *System* > *About*.

- Dual core CPU (recommended minmum)
- 8GB RAM minimum (16GB+ recommended)
- Minimum 20GB of free disk space


## Install WSL / Ubuntu

Follow these steps to install a WSL Ubuntu Linux system within Windows. The remainder of these instructions assume that you are installing Ubuntu, but advanced users may wish to install a different flavour of Linux.


1. Open a **Windows PowerShell (as administrator)** window, copy+paste these commands into the PowerShell, press enter, and follow any prompts given:

        wsl --update
        wsl --shutdown
        wsl --set-default-version 2
        wsl --install -d Ubuntu-22.04

   If you experience any problems, consult the [Microsoft WSL documentation](https://learn.microsoft.com/en-us/windows/wsl/install).

2. Restart your computer.

3. Start the **Ubuntu** application to open an Ubuntu terminal (also known as a *shell*).

4. Set up a username and password for Ubuntu - this only needs to be done the first time you start Ubuntu. This can be different from your Windows username/password. **Make sure you remember your username and password**, as you will need it later on.


## Install FSL dependencies

Some system dependencies need to be installed into Ubuntu before FSL can be installed. Copy+paste these commands into the Ubuntu terminal to install these dependencies. You may be prompted for a password - enter the password that you set up earlier:

    sudo apt update
    sudo apt -y upgrade
    sudo apt-get install dc python3 mesa-utils gedit pulseaudio libquadmath0 libgtk2.0-0 firefox libgomp1


[filename](common_install.md ':include')


## Allocate enough RAM

> This step is only required if you are using a computer with **8GB or less** of RAM.

If you are using a computer with 8GB or less of RAM, you may need to perform some additional steps to ensure that enough RAM is allocated to the WSL subsystem.

1. Open a **Windows PowerShell (as administrator)** window, and type:

         wsl --shutdown
         notepad "$env:USERPROFILE/.wslconfig"

2. Notepad should open up a file named `.wslconfig` - enter the following lines into the file, and save it:

         [wsl2]
         memory=6GB # Limits VM memory to 6GB

3. Restart WSL/Ubuntu and confirm that the available RAM has increased by running this command:

         free -mh

4. Due to how WSL2 works, you must close WSL (by running `wsl --shutdown` in a PowerShell window) between uses of FSL, to ensure RAM is freed up for your Windows sessions. You may also wish to change the memory value in the `.wslconfig` file, e.g. to `memory=4GB`, if you experience degraded performance when using Windows applications.


## Install an X server

> This step is only required if you are using **WSL1**, or installing an old version of FSL (**6.0.7.11** or older).

If you are using WSL version 1, or installing an older version of FSL, you need to install a separate application, called a *X11 server*, in order to use graphical FSL programs. We currently recommend installing [VcXsrv](https://sourceforge.net/projects/vcxsrv/), although several alternatives are available, including [MobaXterm](https://mobaxterm.mobatek.net/) and [Xming](https://sourceforge.net/projects/xming/).


If you are using **WSL2**, you need to disable the built-in GUI server - you can do this as follows:

1. Open a **Windows PowerShell (as administrator)** window, and type:

         wsl --shutdown
         notepad "$env:USERPROFILE/.wslconfig"

2. In the Notepad window, enter the following text, and save the file. If `[wsl2]` is already present, don't add it again - just add `guiApplications=false` line underneath `[wsl2]`:

         [wsl2]
         guiApplications=false


Now, to install VcXsrv:

1. Download and run the installer from https://sourceforge.net/projects/vcxsrv/. This will install an application called **XLaunch**.

2. In order for VcXsrv to work, you will need to modify your Ubuntu configuration profile. If you are using **WSL1**, copy+paste the following code into the Ubuntu shell:

        echo "export DISPLAY=:0" >> ~/.bashrc

   Or, if you are using **WSL2**, copy+paste the following code into the Ubuntu shell:

        echo "export DISPLAY=\$(grep nameserver /etc/resolv.conf  | awk '{print \$2; exit}'):0" >> ~/.bashrc
        echo "export LIBGL_ALWAYS_INDIRECT=1" >> ~/.bashrc

3. Start the **XLaunch** app. You need to make sure that *Native OpenGL* is **deselected**, and *Disable access control* is **selected**, in the *Extra > Settings* panel.

4. Type `glxgears` into the Ubuntu shell, and press enter . A window with three spinning gears should open.

If `glxgears` doesn't work, it is possible that Windows Defender Firewall has blocked the VcXsrv process. Go to *Control panel* > *System and security* > *Windows defender firewall* > *Advanced settings* > *Inbound rules* and make sure that the VcXsrv rules are *not* set to *block* - if they are you will need to edit the VcXsrv rule and change it from *block* to *allow*.

You must start **XLaunch** before running any graphical applications (including FSLeyes). Alternatively, follow these steps to configure **XLaunch** to start automatically whenever you turn on your computer:

1. Start the **XLaunch** app as described above. On the final window, click on the **Save configuration** button, and save the configuration file to `Documents/config.xlaunch`.

2. Open the **Run command** prompt by holding down the Windows key and pressing R (*Win+R*). Type in `shell:startup`, and push enter. This will open your startup folder in an Explorer window.

3. Open a second Explorer window and navigate to your *Documents* folder.

4. Drag and drop (or copy and paste) the `config.xlaunch` file from your *Documents* folder into the startup folder.

After following these steps, **XLaunch** should start automatically whenever you turn on your computer.


?> If you have a high-DPI display, you may need to perform some additional steps to ensure that Linux applications are displayed at the correct resolution. [This StackExchange post](https://superuser.com/a/1370548) has some useful pointers on setting up your system.


## Installing FSL into a virtual machine (VM) instead of WSL


If you are unable to use the Windows Subsystem for Linux (WSL) as outlined above, another option for using FSL on a Windows computer is to use a virtual machine (VM). You can use the freely available [VirtualBox](https://www.virtualbox.org/), or [VMWare Workstation](https://www.vmware.com/uk/products/workstation-pro.html) if you have a license.

There are many online tutorials which describe how to install and set up a virtual machine, e.g.:
 * https://www.howtogeek.com/796988/how-to-install-linux-in-virtualbox/
 * https://www.arcserve.com/blog/dead-simple-guide-installing-linux-virtual-machine-windows

FSL can be installed into most modern Linux distributions - we recommend an Ubuntu LTS release, such as 20.04 or 22.04.

Once you have installed a Linux VM, you can install FSL in the same way as for other platforms - when downloading the `fslinstaller.py` script, be sure to use a web browser **inside the VM**.

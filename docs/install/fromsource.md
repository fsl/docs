# Creating a partial FSL installation from source

!> These instructions are intended for advanced users who are proficient at compiling C++ code and managing UNIX systems. For most FSL users we strongly recommend following the [official installation instructions](install/index.md).

!> These instructions will install unreleased and untested versions of the FSL tools - follow them at your own risk.


This page describes how to manually create a partial FSL installation, compiling C++ tools from source, and containing only the tools that you require.

You should ask yourself if you specifically need to compile the FSL tools from source - you may be able to create a partial FSL installation from our pre-compiled packages using conda. Instructions on how to do this can be found [here](install/conda.md).

?> Before following these instructions, ensure that you have read through and understood the page on [local FSL development](development/local.md).


## 1. Install a conda base environment

FSL installations are based on the [conda](https://conda.io/) environment and package manager. While it is possible to set up a FSL installation without using conda, this is a much more complicated and error-prone process, so is not recommended. More detalis on conda can be found in the [installation notes](install/conda.md), but for the purposes of this task we will use `micromamba` as it is a quick and lightweight means of obtaining a base conda environment.  If you wish to use a different base conda installation (e.g. anaconda, miniconda, mambaforge, etc), then you will need to adjust these instructions accordingly.

Start by deciding where you want your FSL installation to live. Then download and install `micromamba` into that directory.

 - If you are following these instructions in a docker container, make sure that the "core" UNIX utilities (e.g. `tar`, `curl`, `bzip2`) are installed.
 - Change the value of `FSLDIR` to wherever you want to install FSL.
 - Change the value of `MMURL` to a `micromamba` installer for your platform - refer to the [micromamba installation instructions](https://mamba.readthedocs.io/en/latest/installation/micromamba-installation.html) for details.


```bash
export FSLDIR=~/fsl
export MAMBA_ROOT_PREFIX=${FSLDIR}
MMURL=https://micro.mamba.pm/api/micromamba/linux-64/latest
mkdir ${FSLDIR}
cd ${FSLDIR}
curl -Ls ${MMURL} | tar -xvj bin/micromamba
eval "$(bin/micromamba shell hook --shell $(basename ${SHELL}))"
micromamba activate ${FSLDIR}
```

## 2. Create a "base" `${FSLDIR}` directory

Now you need to install the "base" FSL system (the build system and basic utilities), along with a C++ compiler, Python, and a few other libraries and C++ dependencies.

```bash
CHANNELS=(-c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/
          -c conda-forge)
PKGS=(python=3.12 fsl-base make git
      cxx-compiler c-compiler zlib
      openblas boost-cpp libgd libpng
      nlohmann_json expat)
micromamba install -y -p ${FSLDIR} ${CHANNELS[@]} ${PKGS[@]}
```

Now run these commands to configure your current shell session for compilation of FSL projects:

```bash
export FSLDEVDIR=${FSLDIR}
source ${FSLDIR}/etc/fslconf/fsl-devel.sh
```

?> The FSL libraries and executables will be installed to `${FSLDEVDIR}` - you can change this to somewhere other than `${FSLDIR}` if you would like to keep things isolated (e.g. if you are modifying and testing the code).


## 3. Download FSL data sets

If you need the MNI152 templates and FSL atlases, and/or you wish to use
FIRST for sub-cortical segmentation, you will need to download the FSL data sets. This will download about two gigabytes of data:

```bash
PKGS=(fsl-data_standard
      fsl-data_first_models_317_bin
      fsl-data_first_models_336_bin
      fsl-data_atlases)
micromamba install -y -p ${FSLDIR} ${CHANNELS[@]} ${PKGS[@]}
```


## 4. Define the list of FSL projects you want to install

At the moment we don't have a single list of all projects that comprise a FSL installation, nor of the order in which they must be built - we will try and provide this in the future.

As an example, the following set of projects will give you an FSL installation with `fslmaths`, `fast`, `first`, `bet`, `fnirt`, `flirt`, `topup`, and a CPU version of `eddy`:

```bash
PROJECTS=(znzlib newran misc_tcl libgdc cprob baycest
          armawrap utils newnifti misc_c misc_scripts
          miscmaths giftiio newmesh newimage gps swe
          susan slicetimer relax meshclass mcflirt libvis
          lesions first_lib fast4 bint basisfield avwutils
          warpfns randomise mm fslvtkio bet2 topup
          shapemodel mvdisc fnirt cluster eddy flirt first)
```


## 5. Download the FSL source code

Now we need to download all of the project source code from the FSL Gitlab server:

```bash
cd ${FSLDIR}/src
for project in ${PROJECTS[@]}; do
  git clone https://git.fmrib.ox.ac.uk/fsl/${project}.git
done
```

## 6. Compile each project

Most FSL projects use a simple `Makefile`-based build system, so we just need to run `make install` in each directory, in the correct order.

```bash
# Compile up to 16 files in parallel -
# adjust this to suit your system.
export MAKEFLAGS="-j 16"

for project in ${PROJECTS[@]}; do
  make clean
  make install
done
```


## 7. Use your FSL installation

Now you should be able to call your FSL tools - they are located in `${FSLDIR}/bin/` (unless you set `${FSLDEVDIR}` to a different location). Note that this method of installing FSL differs from an installation created by `fslinstaller.py` as all of the executables will be in `${FSLDIR}/bin/`, and there will not be any entry points in `${FSLDIR}/share/fsl/bin/` - see [here](install/configuration.md#fsl-executable-directory-fsldirbin-and-fsldirsharefslbin) for more details.


For future sessions, you will need to add this to your shell profile (e.g. `~/.bashrc`):


```bash
export FSLDIR=~/fsl-dev
export PATH=${FSLDIR}/bin:${PATH}
. ${FSLDIR}/etc/fslconf/fsl.sh
```

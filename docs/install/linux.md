# Linux


FSL can be installed into most modern Linux distributions - we recommend an Ubuntu LTS release, such as 22.04 or 24.04.


[filename](common_install.md ':include')

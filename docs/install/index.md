# Installation and configuration


<div id="pagetoc" start="2" end="2"></div>


FSL is available ready to run for macOS (Intel and M1/M2/M3) and Linux, with Windows computers being supported via the Windows Subsystem for Linux (WSL). Source code is downloaded and installed part of a standard installation, if you wish to compile the code yourself.

The installation procedure will download a large amount of data, so make sure you have a power source, a good internet connection, and some time to spare.

Click on one of the following links for detailed instructions on installing FSL:


 * [Linux](install/linux.md)
 * [macOS](install/macos.md)
 * [Windows](install/windows.md)


## Updating an existing installation


From FSL 6.0.7 onwards, it is possible to update a FSL installation in-place. Simply run the `update_fsl_release` command in a terminal, and follow the prompts.


## Using FSL on a cluster / with GPUs


Certain FSL tools are able to take advantage of GPUs and HPC cluster systems for improved performance, and for running analyses at scale.


### Cluster aware tools


Several of the more compute-intensive tools can take advantage of cluster computing, via [SLURM](https://slurm.schedmd.com/overview.html) or [Some of Grid Engine](https://github.com/daimh/sge) (or Altair&reg; Grid Engine&reg;). We would largely recommend using SLURM if you are building a cluster from scratch, as packages are readily available for the major Linux distributions.

 * [FEAT](task_fmri/feat/index.md) will run multiple first-level analyses in parallel if they are setup all together in one GUI setup. At second level, if full FLAME (stages 1+2) is selected then all the slices are processed in parallel.
 * [MELODIC](resting_state/melodic.md) will run multiple single-session analyses (or single-session preprocessing if a multi-session/subject analysis is being done) in parallel if they are setup all together in one GUI setup.
 * [TBSS](diffusion/tbss.md) will run all registrations in parallel.
 * [BEDPOSTX](diffusion/bedpostx.md) low-level diffusion processing will run all slices in parallel.
 * [FSLVBM](structural/fslvbm.md) will run all registrations in parallel, both at the template-creation stage and at the final registrations stage.
 * [POSSUM](utilities/possum.md) will process all slices in parallel.

All the above tools interact with a compute cluster via a single central script `fsl_sub`; if no cluster is available then this script silently runs all the requested jobs in series. To customise FSL for your local compute cluster and clustering software, refer to the [`fsl_sub` documentation](https://git.fmrib.ox.ac.uk/fsl/fsl_sub/).


### GPU / CUDA capable tools


Some FSL tools are able to be accelerated with CUDA-compatible NVIDIA GPUs - this includes `bedpostx_gpu`, `eddy`, `probtrackx2_gpu`, and `mmorf`. These tools are compiled against the CUDA 10.2 toolkit, but should work with any GPU that supports CUDA 10.2 or newer.

The CUDA toolkit is statically linked into the executable files, so all that is needed to run them is a NVIDIA GPU and CUDA driver that suppports CUDA 10.2 or newer - you do **not** need to install the CUDA Toolkit in order to run these tools.

If you need to run any of these tools on an older GPU which does not support CUDA 10.2, you can recompile the code against the CUDA Toolkit of your choice.


## Information for advanced users


The FSL installation procedure is similar on all platforms, and involves downloading and running a Python script called [`fslinstaller.py`](https://git.fmrib.ox.ac.uk/fsl/conda/installer), which can be run with any version of Python 2.7 or newer.  This script does the following:

1. Downloads a [miniconda](https://docs.conda.io/en/latest/miniconda.html) installer, and installs it to `${FSLDIR}` - the default installation location is `~/fsl/`.

2. Downloads a YAML `environment.yml` file from [https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/](https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/), containing a conda environment specification for the latest FSL version (or the version requested by the user).

3. Installs the FSL environment by running: `${FSLDIR}/bin/conda env update -n base -f environment.yml`

4. Modifies the user's shell configuration so that FSL is accessible in their shell environment.


The `fslinstaller.py` script accepts a number of options which can be used to customise the installation (e.g. installation directory, shell profile configuration, etc). Run `python fslinstaller.py --help` for more details. You can find more information on configuring/customising your FSL installation [here](install/configuration.md).


FSL can be installed into [docker/singularity containers](install/container.md), and custom FSL installations can be created by using [conda directly](install/conda.md).


## Installing older versions of FSL

The `fslinstaller.py` script can be used to install versions of FSL from 6.0.6 and newer.  By default the latest version will be installed, but you can choose a different version with the `-V` flag, e.g.:

```
python fslinstaller.py -V 6.0.6
```

If you need to install a version of FSL which is older than 6.0.6, you need to use a different installer script - you can download a legacy version of the `fslinstaller.py` script from [here](https://git.fmrib.ox.ac.uk/fsl/installer/-/raw/3.3.0/fslinstaller.py?inline=false).

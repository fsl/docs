# FSL utilities

FSL contains a large number of utilties for working with neuroimaging data. Many tools which work on images will work on both 3D and 4D data. For each of these programs, type just the program name to get the usage help.

`fslascii2img` - convert ASCII text file to image file.

`fsl2ascii` - convert image files to ASCII text file (or more than one file if data is 4D). Each output file contains the values of all the intensities in the image in a fixed order with x varying the fastest, then y, then z. That is, starting with intensity at the voxel coordinate (0,0,0) and then (1,0,0) and then (2,0,0) ... (Nx-1,0,0) and then (0,1,0) and then (1,1,0) ... (Nx-1,Ny-1,0) and then (0,0,1) ... and finishing with (Nx-1, Ny-1, Nz-1).

`fslcc` - run cross-correlations between every volume in one 4D data set with every volume in another (for investigating similarities in ICA outputs).

`fslchfiletype` - used to change the file type of an image (e.g. from `NIFTI_GZ` to `NIFTI`). The first argument is the desired file type (one of `NIFTI`, `NIFTI_GZ`, `NIFTI_PAIR`, `NIFTI_PAIR_GZ`, `NIFTI2`, `NIFTI2_GZ`, `NIFTI2_PAIR`, or `NIFTI2_PAIR_GZ`) and the second is the input file. If no third argument is given then the input file is converted in place. This in-place conversion removes the original files: e.g. for an ANALYZE file called `stdimg` then `fslchfiletype NIFTI_GZ stdimg` would replace `stdimg.hdr` and `stdimg.img` with `stdimg.nii.gz`. Note that having multiple copies of an image with the same basename and different filetypes (e.g. `stdimg.nii.gz` and `stdimg.hdr` and `stdimg.img`) creates many problems for determining what images to read, and in general will cause FSL programs to stop.

`fslcomplex` - a utility that allows 3D or 4D complex image files to be split or constructed from corresponding real components (either Cartesian or polar). The initial flag indicates what kind of processing is done. In addition, two optional arguments at the end specify the first and last 3D volumes to be processed when the input is 4D (default is to do all volumes).

`fslcreatehd` - creates a new image header along with a zero intensity data image. There are two forms of input: one takes a list of minimal information on the command line, the other takes in an xml-style file, as written by `fslhd -x`. The information required in the first version is: x/y/z/tsize for image dimensions, x/y/zvoxsize for voxel dimensions (eg in mm), tr in seconds for time between volumes (for 3D data set to 0), x/y/zorigin for voxel co-ordinate origin (eg for standard space - otherwise leave as 0 0 0), datatype for the voxel data type (the commonest are: 1=binary, 2=unsigned char, 4=signed short, 8=signed int, 16=float). Note that this is different from the previous versions in that an entire image is created, and will overwrite any `.img` file contents of the same image name.

`fsledithd` - allows the header information in and image to be edited in a text-based xml-style format (like the output of `fslhd -x` but with redundant fields removed and some help text provided). Note that the default text editor used is `nano`, but other editors can be specified by the second argument.

`fslfft` - outputs the Fast-Fourier Transform (or inverse) for a complex input volume.

`fslhd` - report every field of an Analyze or Nifti header (note that the fields are different although some are common, e.g. pixdims). The option `-x` produces an xml-style output which can be used in conjunction with `fslcreatehd`. The reported values are those used internally in FSL programs and are sometimes different from the raw values stored in the file to avoid incorrect settings (e.g. dimN has a minimum value of 1, not 0).

`fslinfo` - report a basic subset of an Analyze or Nifti header.

`fslinterleave` - interleave two inputs to form a combined image.

`fslmaths` - simple but powerful program to allow mathematical manipulation of images. Includes spatial and temporal filtering, statistic conversion (e.g. z to p-values), diffusion tensor decomposition, and TFCE calculation.

`fslmeants` - output the average timeseries of a set of voxels, or the individual time series for each of these voxels.

`fslmerge` - concatenate image files into a single output. This concatenation can be in time, or in X, Y or Z. All image dimensions (except for the one being concatenated over) must be the same in all input images. For example, this can be used to take multiple 3D files (eg as output by SPM) and create a single 4D image file.

`fslnvols` - report how many time points are in the input 4D data set.

`fslpspec` - outputs the spectral power density of an input 4D data set.

`fslroi` - extract region of interest (ROI) from an image. You can a) take a 3D ROI from a 3D data set (or if it is 4D, the same ROI is taken from each time point and a new 4D data set is created), b) extract just some time points from a 4D data set, or c) control time and space limits to the ROI. Note that the arguments are minimum index and size (not maximum index). So to extract voxels 10 to 12 inclusive you would specify 10 and 3 (not 10 and 12).

`fslslice` - split a 3D file into lots of 2D files (along z-axis).

`fslsplit` - split a 4D file into lots of 3D files (eg for inputting to SPM).

`fslstats` - report certain summary statistics for an input 3D/4D image. Of particular value is the "robust intensity range" which calculates values similar to the 2% and 98% percentiles, but ensuring that the majority of the intensity range is captured, even for binary images. In addition, it can take an aribitrary mask as input (values need to be greater than 0.5 to be part of the mask) so that statistics are only calculated within the mask and can also generate histogram data.

`fslval` - report a particular parameter (given a particular keyword eg "dim4") from an image header. To see the list of keywords run fslhd on the header and look at the first column.


## Orientation tools


### `fslreorient2std`

This is a simple tool designed to reorient an image to match the orientation of the standard template images (MNI152) so that they appear "the same way around" in FSLeyes. It requires that the image labels are correct in FSLeyes before this is run. It is also not a registration tool, so it will not align the image to standard space, it will only apply 90, 180 or 270 degree rotations about the different axes as necessary to get the labels in the same position as the standard template.


### `fslcpgeom`

This tool can be used to copy certain parts of the header information (image dimensions, voxel dimensions, voxel dimensions units string, image orientation/origin or qform/sform info) from one image to another.


### `fslorient`

This is an advanced tool that reports or sets the orientation information in a file. Note that only in NIfTI files can the orientation be changed - Analyze files are *always treated as "radiological"* (meaning that they could be simply rotated into the same alignment as the MNI152 standard images - equivalent to the appropriate `sform` or `qform` in a NIfTI file having a negative determinant).

In NIfTI files it is possible to independently report or modify the `qform` or `sform` fields (see the [orientation page](other/orientation.md) for background information on NIfTI orientation). However, many FSL tools will **try to keep `qform` and `sform` matrices the same** whenever one would otherwise be unset. Therefore it is not possible, for instance, to delete only the `qform`, as if the `sform` is set then doing this will result in the `qform` being set equal (as close as possible) to the `sform.`


This is currently done to aid interoperability with other packages. However, if both `qform` and `sform` are given different values then these are preserved by the output routines. This command does not change the data storage at all - only the orientation information in the header and so should be used with great caution. Only modify header information if you understand the [information about the NIfTI orientation](other/orientation.md) or are following (exactly) a set of steps prescribed by someone who does. The option `-deleteorient` is useful for the first stage of correcting orientation/label information in images where the conversion to NIfTI does not provide the correction information. Note that the first, and by far the best, solution to this problem is to find a method of converting the images to NIfTI in a way that correctly stores the information (see above) but if this really is not possible then a combination of `fslorient` and `fslswapdim` can be used to fix it (see below).


### `fslswapdim`

This is an advanced tool that re-orders the data storage to permit changes between axial, sagittal and coronal slicing. When used in this mode the same left-right convention (also called coordinate handedness or radiological/neurological convention) will be maintained as long as no warning is printed. The new orientation of the data is specified by selecting what the new axes should represent. This can either be done in terms of the old axes (`x`, `y`, `z`, `-x`, `-y`, `-z`) or in terms of anatomical labels when this information is available (in a nifti image where either the sform or qform code is non-zero). The anatomical labels are `RL`, `LR` `AP`, `PA`, `SI`, `IS`.

Note that the anatomical label version will not allow the left-right convention to be changed. Ordinarily `fslswapdim` will change the orientation information in the header as well as reordering the data. This is so that the anatomical labels stay attached to the same parts of the image and not fixed to the voxel coordinates. Hence, reordering a coronal image to axial slicing should keep the labels correctly attached to the relevant parts of the image, as long as they were correct initially.

If the initial labels are incorrect (see the labels in FSLeyes) then `fslorient` needs to be used in conjunction with `fslswapdim` in order to correct this. When `fslswapdim` is given arguments that will change the left-right orientation it will issue a warning that the left-right orientation is being flipped. It will also try to modify the orientation information in the header (only when either the `sform` or `qform` code is non-zero), but not in a way that swaps this left-right orientation in the header. Hence there is a net change in orientation as the data is swapped while the header is not. If the swap occurs on the x-axis then nothing is done to the header at all. Otherwise, the axis which is being swapped, together with the x-axis, have their orientation changed.

In this way the handedness of the header is preserved, the labels associated with the y-axis and z-axis follow the image change, but the x-axis labels do not. It is recommended that if a left-right swap in storage is desired (and this should only be done if the reconstruction is initially incorrect, and cannot be fixed by finding any better conversion method) then the arguments `-x y z` should be used as this is the simplest form of swapping since it only affects the x-axis data and no labels or header information is changed.

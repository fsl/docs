# `fsl_sub` - Grid submission wrapper

`fsl_sub` is a command-line tool which provides a consistent interface to various cluster grid backends, with a fall back to running tasks locally where no cluster is available. `fsl_sub` is installed as part of FSL, but can also be installed and used independently of FSL. `fsl_sub` provides a plugin interface to support different grid backends - there are currently two plugins, both of which are installed as part of FSL:

 - [`fsl_sub_plugin_sge`](https://git.fmrib.ox.ac.uk/fsl/fsl_sub_plugin_sge) for [Sun/Son of/Univa Grid Engine](https://gridengine.sourceforge.io/SGE/)
 - [`fsl_sub_plugin_slurm`](https://git.fmrib.ox.ac.uk/fsl/fsl_sub_plugin_slurm) for [Slurm](https://www.schedmd.com/)

Type `fsl_sub --help` for an overview of all options.

Full documentation for `fsl_sub`, including instructions on configuring it for your grid system, is available at https://git.fmrib.ox.ac.uk/fsl/fsl_sub.

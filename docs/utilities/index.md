# Other tools

FSL contains a large number of tools and utilities which are not tied to any specific MRI modality, or which do not fit into any of the other categories within this documentation suite. This section describes some of the more important tools, including [FSL-MRS](utilities/fsl_mrs.md), [`fsl_sub`](utilities/fsl_sub.md) and [FSLeyes](utilities/fsleyes.md).

# FSL Python libraries

Most FSL tools are intended to be used from a UNIX terminal. However, FSL also contains a selection of Python libraries which can be used to write MRI analysis pipelines in Python. These libraries are all installed as part of a standard FSL installation, and can be accessed via the `${FSLDIR}/bin/fslpython` Python interpreter.

<div id="pagetoc" start="1" end="1" offset="1"></div>

# fslpy

`fslpy` is a Python library which contains a collection of utilities used within FSL and by FSLeyes. API documentation for `fslpy` can be found at https://open.win.ox.ac.uk/pages/fsl/fslpy/

One of the most useful features of `fslpy` is the `fsl.wrappers` package, which contains Python functions that "wrap" a range of FSL commands. These wrapper functions can be useful if you would prefer to write your analysis scripts in Python instead of as `bash` scripts. For example, you can use the `fsl.wrappers.bet` command to perform brain extraction, e.g.:


```python
from fsl.wrappers import bet
bet('struct', 'struct_brain')
```

More information is available in the [`fslpy` API documentation](https://open.win.ox.ac.uk/pages/fsl/fslpy/fsl.wrappers.html).


# file-tree

`file-tree` is a Python library designed to allow you to separate the directory structure of your pipeline inputs/outputs from the actual pipeline code.

For example, let's say that you have a collection of T1 images organised like so:

```
subjectA/
    T1w.nii.gz
subjectB/
    T1w.nii.gz
subjectC/
    T1w.nii.gz
```

You can describe the structure of your data-set using a file (e.g. `mydata.tree`), containing a placeholder for the subject ID, and giving both the input and desired output files unique identifiers:

```
subject{subject}
    T1w.nii.gz (t1)
    T1w_brain.nii.gz (bet_output)
    T1w_brain_mask.nii.gz (bet_mask)
```

Now you can use `file-tree` and `fsl.wrappers` to run BET on each of your T1 images - all of the paths for your input and output files are automatically generated for you:

```python
from fsl.utils.filetree import FileTree
from fsl.wrappers.bet import bet

tree = FileTree.read('mydata.tree')
tree.update_glob('t1', inplace=True)

for t1_tree in tree.iter('t1'):
    bet(input=t1_tree.get('t1''),
        output=t1_tree.get('bet_output', make_dir=True),
        mask=True)
```

Learn more about `file-tree` by visiting its [API documentation](https://open.win.ox.ac.uk/pages/fsl/file-tree/).


# fsl-pipe

`fsl-pipe` is a Python library which builds upon file-tree, and allows you to write an analysis pipeline in a _declarative_ manner. A pipeline is defined by:

- A file-tree which defines the directory structure of the inputs and outputs of the pipeline.
- A set of recipes (Python functions) describing how each of the pipeline outputs should be produced.

`fsl-pipe` will automatically determine which recipes should be executed, and in what order, to produce whichever output file(s) you request. Recipes can either run locally, be distributed using [dask](https://www.dask.org/), or be submitted to a cluster using [fsl-sub](https://git.fmrib.ox.ac.uk/fsl/fsl_sub/).


```python
#!/usr/bin/env python

from fsl.wrappers import bet

from file_tree import FileTree
from fsl_pipe import pipe, In, Out

@pipe
def brain_extract(t1 : In, bet_output : Out, bet_mask : Out):
    bet(t1, bet_output, mask=True)

tree = FileTree.read('data.tree').update_glob('t1')
pipe.cli(tree)
```

For more details visit the [fsl-pipe API documentation](https://open.win.ox.ac.uk/pages/fsl/fsl-pipe/).

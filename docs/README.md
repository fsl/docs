# FSL - FMRIB Software Library


<hr>

**All material for the [FSL Course](https://open.win.ox.ac.uk/pages/fslcourse/website/index.html) is available online, including full lecture recordings and practical overviews.**


<hr>

**Instructions for installing FSL can be found [here](install/index.md).**

<hr>


FSL is a comprehensive library of analysis tools for FMRI, MRI and diffusion brain imaging data. It runs on macOS (Intel and M1/M2/M3), Linux, and Windows via the Windows Subsystem for Linux, and is very easy to install. Most of the tools can be run both from the command line and as GUIs ("point-and-click" graphical user interfaces). To quote the relevant references for FSL tools you should look in the individual tools' manual pages, and also please reference one or more of the FSL overview papers:


1. M.W. Woolrich, S. Jbabdi, B. Patenaude, M. Chappell, S. Makni, T. Behrens, C. Beckmann, M. Jenkinson, S.M. Smith. Bayesian analysis of neuroimaging data in FSL. NeuroImage, 45:S173-86, 2009

2. S.M. Smith, M. Jenkinson, M.W. Woolrich, C.F. Beckmann, T.E.J. Behrens, H. Johansen-Berg, P.R. Bannister, M. De Luca, I. Drobnjak, D.E. Flitney, R. Niazy, J. Saunders, J. Vickers, Y. Zhang, N. De Stefano, J.M. Brady, and P.M. Matthews. Advances in functional and structural MR image analysis and implementation as FSL. NeuroImage, 23(S1):208-19, 2004

3. M. Jenkinson, C.F. Beckmann, T.E. Behrens, M.W. Woolrich, S.M. Smith. FSL. NeuroImage, 62:782-90, 2012

<hr>

!> **New documentation site!** \
\
These documentation pages replace the [old FSL wiki](https://fsl.fmrib.ox.ac.uk/fsl/oldwiki/). During the content migration, some sections may have been missed. If you can't find what you are looking for here, try searching through the old FSL wiki, which can be found at https://fsl.fmrib.ox.ac.uk/fsl/oldwiki/

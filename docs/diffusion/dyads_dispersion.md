# How to calculate dispersion for a collection of vectors

Below is the calculation made in `bedpostx` to calculate the dyads dispersion.

If $\mathbf{x_1},...,\mathbf{x_n}$ denote the sample dyads, then we can construct the average dyadic tensor $\mathbf{M}$ (average of the rank-1 tensors implied by the dyads) as follows:

$$\mathbf{M} = \frac{1}{n}\sum_{i=1}^n \mathbf{x_i}\mathbf{x_i}^T.$$

The dispersion $\rho$ is defined as one minus the largest eigenvalue of $\mathbf{M}$:

$$\rho = 1-\lambda_1.$$

To understand why this is a measure of dispersion, consider dot products between sample dyads and the largest eigenvector $\mathbf{v_1}$. Since we have $\mathbf{Mv_1}=\lambda_1\mathbf{v_1}$, then:

$$
\begin{array}{rcl}
\rho & = & 1-\mathbf{v_1}^TM\mathbf{v_1} \\
& = & 1-\mathbf{v_1}^T\frac{1}{n}\sum_{i=1}^n \mathbf{x_i}\mathbf{x_i}^T\mathbf{v_1} \\
& = & 1-\frac{1}{n}\sum_{i=1}^n <\mathbf{x_i}, \mathbf{v_1}>^2

\end{array}
$$


Thus, the dispersion is related to the average square dot product between the samples and their average orientation (similar to a variance term).

# Diffusion MRI - FMRIB's Diffusion Toolbox (FDT)

FDT (FMRIB's Diffusion Toolbox) is a software tool for analysis of diffusion weighted images.

## Contents

* [TOPUP](diffusion/topup/index.md)
* [EDDY](diffusion/eddy/index.md)
* [EDDY QC](diffusion/eddyqc.md)
* [DTIFIT](diffusion/dtifit.md)
* [TBSS](diffusion/tbss.md)
* [BedpostX](diffusion/bedpostx.md)
* [ProbtrackX](diffusion/probtrackx.md)
* [XTRACT](diffusion/xtract.md)
* [AutoPTX](diffusion/autoptx.md)
* [Utilities](diffusion/utils.md)
* [FAQ](diffusion/faq.md)


## FDT diffusion processing overview

FDT is part of FSL (FMRIB's Software Library). FDT has an easy-to-use graphical user interface (GUI) and its component programmes can also be run from the command line. FDT includes tools for data preprocessing, local diffusion modelling and tractography. Each stage in FDT is run separately. To call the FDT GUI, either run `Fdt` (`Fdt_gui` on Mac or Windows), or run `fsl` and press the FDT button.

A typical processing pipeline (and approximate time required for each stage, based on an Intel 2.66GHz processor, and a 60-direction whole-brain dataset of dimensions 128x128x70, at 2 mm isotropic resolution) would consist of:

1. Any study or scanner-specific pre-processing (e.g., conversion from DICOM to NIFTI using for example `dcm2niix`, removal of images affected by large artifacts). This would be done manually by the user.

2. Check your diffusion data ([see below](#check-your-data).

3. `topup` - Susceptibility-induced distortion correction (fieldmap estimation) using [topup](diffusion/topup/index.md).

4. `bet` - Brain extraction using [BET](structural/bet.md) on the distortion-corrected b0 (output of [topup](diffusion/topup/index.md)).

5. `eddy` - Distortion correction using eddy. `eddy` simultaneously corrects for eddy currents and subject motion. It can also use a fieldmap estimated by [topup](diffusion/topup/index.md) and correct for susceptibility-induced distortions. (~hours-days on CPU or less if multi-threaded or few hours on GPU).

After running eddy, all the diffusion-weighted images should be in alignment with each other and be undistorted. We can now either run a microstructural analysis or tractography. For a microstructural analysis the further pipeline would be:

- `dtifit` - Fitting of diffusion tensors on eddy-corrected data (~1 minute).

- `TBSS` - Comparison of the fractional anisotropy maps (or others) between subjects (see [TBSS](diffusion/tbss.md)).

Or run tractography we need to:

- `bedpostx` - Fitting of the probabilistic diffusion model on corrected data (~15 hours single-threaded on CPU or less if multi-threaded or ~30 minutes run on GPU).

- [Registration](#registration-within-fdt) to structural reference image and/or standard space - (3-6 minutes).

- `probtrackx` - Probabilistic tractography run on the output of `bedpostx` (execution time depends very much on what the user wishes to do. Generating a connectivity distribution from a single voxel of interest takes about 1 second).

- `xtract` - An automated tractography tool, built on `probtrackx`, that allows reconstruction of a set of major pathways in humans and macaques.

### Diffusion data in FSL

Diffusion data is usually stored in a 4D NIFTI file, where each volume is acquired with a different b-value and/or gradient orientation. To run FDT tools we need:

- Diffusion weighted data (`data.nii.gz`): A 4D series of data volumes. This will include diffusion-weighted volumes and volume(s) with no diffusion weighting.

- Gradient directions (`bvecs`): An ASCII text file containing a list of gradient directions applied during diffusion weighted volumes. The order of entries in this file must match the order of volumes in the input data series.

The format is
```text
x_1 x_2 x_3 ... x_n
y_1 y_2 y_3 ... y_n
z_1 z_2 z_3 ... z_n
```

Vectors are normalised to unit length within the `dtifit` code. For volumes in which there was no diffusion weighting, the entry should still be present, although the direction of the vector does not matter! For technical details see this [FAQ entry](diffusion/faq.md).

- b values (`bvals`): An ASCII text file containing a list of b values applied during each volume acquisition. The b values are assumed to be in s/mm^2 units. The order of entries in this file must match the order of volumes in the input data and entries in the gradient directions text file.

The format is

```text
b_1 b_2 b_3 ... b_n
```

### Check your data

To check whether your bvecs and bvals are correct, it is recommended to run a quick check on the raw data. This can be run after preprocessing or even before if the subject did not move too much:

1. Brain extraction using BET.

2. Run `dtifit`.

3. View the principal eigenvector (V1) in FSLeyes to check the vectors are correctly oriented with respect to the anatomy. If there is a problem then the bvecs need to be modified (change signs and/or permute order of components) in order to get the V1 vectors correctly oriented (repeat `dtifit` after modifying bvecs and view V1 vectors again until they are correct) before proceeding with any further processing.

Another useful tool for checking data quality is `eddy_qc`, which requires you to first run `eddy`.

### Registration within FDT

If tractography results are to be stored in any space other than diffusion space then registration must be run.

Registration within FDT uses [FLIRT](registration/flirt/index.md), but there is also an option for using [FNIRT](registration/fnirt/index.md) nonlinear registration to standard space. When using the GUI, registration can only be applied after `bedpostx` has been run. Typically, registration will be run between three spaces:

 - Diffusion space (using the `nodif_brain` image in the `bedpostX` directory).
 - Structural space (using a structural image, e.g., the space of a high-resolution T1-weighted image of the same subject).
 - Standard space (by default, the MNI152 brain stored within `$FSLDIR/data/standard`).

Note that the structural (T1-weighted) image must have had [BET](structural/bet.md) applied. The `nodif_brain` image should be the brain extracted version of the `nodif` image that is stored in the `bedpostX` directory. Create this image using `fslroi` then `bet` on the data if it does not already exist. (it is important that the user checks the quality of `bet` results on these images and adjust the settings in `bet` where appropriate).

Transformation matrices, and their inverses, will be derived from diffusion to structural space and from structural to standard space. Relevant matrices will be concatenated to produce transformation matrices between diffusion and standard space. The resulting matrices are stored within the `xfms` subdirectory of the bedpostX directory and named as follows:

 - `diff2str.mat` - from diffusion to structural space
 - `str2diff.mat` - from structural to diffusion space
 - `diff2standard.mat` - from diffusion to standard space
 - `standard2diff.mat` - from standard to diffusion space
 - `str2standard.mat` - from structural to standard space
 - `standard2str.mat` - from standard to structural space

By default, transformation matrices between diffusion and structural space are derived using 6 degrees of freedom, the correlation ratio cost function, and normal search; transformation matrices between structural and standard space are derived using 12 degrees of freedom, the correlation ratio cost function and normal search. These parameters may be adjusted if required using the drop down menus in the registration panel.

In the GUI, it is possible to use nonlinear [FNIRT](registration/fnirt/index.md) registration between structural and standard space. In this case, the user needs to provide a non-brain-extracted structural image, as this will make FNIRT more accurate.

Note that diffusion images will typically have geometric distortions due to the effect of field inhomogeneities that are not affecting the structural image. Therefore, an affine [FLIRT](registration/flirt/index.md) registration to structural space may not be accurate enough. We recommend that the user corrects for these distortions either using fieldmap acquisitions or using the [topup](diffusion/topup/index.md) tool. This must be done in the pre-processing and prior to registration.


## Referencing

If you use FDT in your research, please make sure that you reference the relevant articles amongst the following:

### When using BedpostX

Standard ball & stick model (model1)

> [Behrens 2003a] T.E.J. Behrens, M.W. Woolrich, M. Jenkinson, H. Johansen-Berg, R.G. Nunes, S. Clare, P.M. Matthews, J.M. Brady, and S.M. Smith. Characterization and propagation of uncertainty in diffusion-weighted MR imaging. Magn Reson Med, 50(5):1077-1088, 2003.

> [Behrens 2007] T.E.J. Behrens, H. Johansen-Berg, S. Jbabdi, M.F.S. Rushworth, and M.W. Woolrich. Probabilistic diffusion tractography with multiple fibre orientations. What can we gain? NeuroImage, 23:144-155, 2007.

Multi-shell ball & stick model (model2)

> [Jbabdi 2012] S. Jbabdi, S.N. Sotiropoulos, A. Savio, M. Grana, T.E.J. Behrens. Model-based analysis of multishell diffusion MR data for tractography: How to get over fitting problems. Magn Reson Med, doi: 10.1002/mrm.24204, 2012.

Multi-shell ball & zepellins model (model3)

> [Sotiropoulos 2016] S.N. Sotiropoulos, M. Hernandez-Fernandez, A.T. Vu, J.L. Andersson, S. Moeller, E. Yacoub, C. Lenglet, K. Ugurbil, T.E.J. Behrens, S. Jbabdi. Fusion in diffusion MRI for improved fibre orientation estimation: An application to the 3T and 7T data of the Human Connectome Project. Neuroimage 134:396-409, 2016.

GPU version

   > [Hernandez 2013] Hernandez M, Guerrero GD, Cecilia JM, Garcia JM, Inuggi A, Jbabdi S, Behrens TEJ, Sotiropoulos SN, (2013) Accelerating Fibre Orientation Estimation from Diffusion Weighted Magnetic Resonance Imaging Using GPUs. PLoS ONE 8(4):e61892

### When using ProbtrackX

> [Behrens 2003a] T.E.J. Behrens, M.W. Woolrich, M. Jenkinson, H. Johansen-Berg, R.G. Nunes, S. Clare, P.M. Matthews, J.M. Brady, and S.M. Smith. Characterization and propagation of uncertainty in diffusion-weighted MR imaging. Magn Reson Med, 50(5):1077-1088, 2003.

> [Behrens 2007] T.E.J. Behrens, H. Johansen-Berg, S. Jbabdi, M.F.S. Rushworth, and M.W. Woolrich. Probabilistic diffusion tractography with multiple fibre orientations. What can we gain? NeuroImage, 23:144-155, 2007.

GPU version

> [Hernandez 2019] M. Hernandez-Fernandez, I. Reguly, S. Jbabdi, M. Giles, S. Smith, S.N. Sotiropoulos. Using GPUs to accelerate computational diffusion MRI: From microstructure estimation to tractography and connectomes. Neuroimage, 188:598-615, 2019.

### When using connectivity-based parcellation

> [Behrens 2003b] T.E.J. Behrens, H. Johansen-Berg, M.W. Woolrich, S.M. Smith, C.A.M. Wheeler-Kingshott, P.A. Boulby, G.J. Barker, E.L. Sillery, K. Sheehan, O. Ciccarelli, A.J. Thompson, J.M. Brady, and P.M. Matthews. Non-invasive mapping of connections between human thalamus and cortex using diffusion imaging. Nature Neuroscience, 6(7):750-757, 2003.

> [Johansen-Berg 2004] H. Johansen-Berg, T.E.J. Behrens, M.D. Robson, I. Drobnjak, M.F.S. Rushworth, J.M. Brady, S.M. Smith, D.J. Higham, and P.M. Matthews. Changes in connectivity profiles define functionally distinct regions in human medial frontal cortex. Proc Natl Acad Sci U S A, 101(36):13335-13340, 2004.

### When using `qboot`

> [Sotiropoulos 2011] S.N. Sotiropoulos, I. Aganj, S. Jbabdi, G. Sapiro, C. Lenglet, T.E.J. Behrens. Inference on Constant Solid Angle Orientation Distribution Functions from Diffusion-Weighted MRI, p. 609, OHBM, Canada, 2011.

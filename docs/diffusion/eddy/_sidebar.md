* [FSL](/)
  * [Diffusion](diffusion/index.md)
    * [EDDY](diffusion/eddy/index.md)
      * [What are eddy currents?](diffusion/eddy/index.md#what-are-quoteddy-current-induced-distortionsquot)
      * [How does eddy work](diffusion/eddy/index.md#how-does-eddy-solve-that)
      * [The Gaussian Process](diffusion/eddy/index.md#the-gaussian-process)
      * [What data does eddy need?](diffusion/eddy/index.md#what-data-does-eddy-need)
      * [Eddy's distortion model](diffusion/eddy/index.md#what-distortion-model-does-eddy-use)
      * [Beyond eddy currents](diffusion/eddy/index.md#what-corrections-does-eddy-perfom-apart-from-for-eddy-currents)
      * [Users guide](diffusion/eddy/users_guide/index.md)
      * [Frequently asked questions](diffusion/eddy/FAQ/index.md)
      * [Referencing eddy](diffusion/eddy/index.md#referencing)

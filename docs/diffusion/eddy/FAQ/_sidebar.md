* [FSL](/)
  * [Diffusion](diffusion/index.md)
    * [EDDY](diffusion/eddy/index.md)
      * [What are eddy currents?](diffusion/eddy/index.md#what-are-quoteddy-current-induced-distortionsquot)
      * [How does eddy work](diffusion/eddy/index.md#how-does-eddy-solve-that)
      * [The Gaussian Process](diffusion/eddy/index.md#the-gaussian-process)
      * [What data does eddy need?](diffusion/eddy/index.md#what-data-does-eddy-need)
      * [Eddy's distortion model](diffusion/eddy/index.md#what-distortion-model-does-eddy-use)
      * [Beyond eddy currents](diffusion/eddy/index.md#what-corrections-does-eddy-perfom-apart-from-for-eddy-currents)
      * [Users guide](diffusion/eddy/users_guide/index.md)
      * [Frequently asked questions](diffusion/eddy/FAQ/index.md)
        * [GPU requirements](diffusion/eddy/FAQ/index.md#what-are-the-gpu-requirements)
        * [`--acqp` contents?](diffusion/eddy/FAQ/index.md#how-do-i-know-what-to-put-into-my-acqp-file)
        * [`--acqp` meaning?](diffusion/eddy/FAQ/index.md#does-it-matter-what-i-put-into-my-acqp-file)
        * [`--acqp` why??](diffusion/eddy/FAQ/index.md#so-does-it-never-matter-at-all-what-i-put-into-my-acqp-file)
        * [I don't have a `.json` file](diffusion/eddy/FAQ/index.md#i-don-t-have-a-json-file-what-do-i-put-in-my-acqp-file)
        * [`--acqp` - need more than two rows?](diffusion/eddy/FAQ/index.md#do-i-ever-need-more-than-two-rows-for-eddy-in-my-acqp-file)
        * [`--acqp` - meaning of more than two rows?](diffusion/eddy/FAQ/index.md#what-does-it-mean-if-i-have-more-than-two-rows-in-my-acqp-file)
        * [Will eddy rotate my `bvecs`?](diffusion/eddy/FAQ/index.md#will-eddy-rotate-my-bvecs-for-me)
        * [My data has ots of movement](diffusion/eddy/FAQ/index.md#what-would-a-good-eddy-command-look-like-for-data-with-lots-of-movement)
        * [I want to run slice-to-volume](diffusion/eddy/FAQ/index.md#what-if-i-want-to-run-slice-to-volume-but-don-t-have-a-json-file)
        * [b0 and DWI volumes are mis-registered](diffusion/eddy/FAQ/index.md#i-look-at-my-eddy-corrected-data-and-it-looks-like-there-is-a-mis-registration-between-the-b0-and-the-dwi-volumes)
      * [Referencing eddy](diffusion/eddy/index.md#referencing)

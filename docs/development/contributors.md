# Contributors

## Financial support

The [UK EPSRC](http://www.epsrc.ac.uk/) and the [Wellcome Trust](http://www.wellcome.ac.uk/) are the largest providers of funding for the FMRIB Analysis Group, and for this we are extremely grateful. We are also very grateful for significant financial support from [UK MRC](http://www.mrc.ac.uk/), [UK BBSRC](http://www.bbsrc.ac.uk/), [Pfizer](http://www.pfizer.com/home/), [GlaxoSmithKline](http://www.gsk.com/) and [NIH](http://www.nih.gov/).

## FSL Developers

The following people have made significant contributions to the development of FSL v6.0: 

Fidel Alfaro Almagro, Jesper Andersson, Peter Bannister, Matteo Bastiani, Christian Beckmann, Timothy Behrens, Michael Chappell, Michiel Cottaar, Gwenaëlle Douaud, Ivana Drobnjak, Eugene Duff, Sean Fitzgibbon, David Flitney, Ludovica Griffanti, Adrian Groves, Taylor Hanayik, Samuel Harrison, Moises Hernandez Fernandez, Saad Jbabdi, Mark Jenkinson, Bradley Macintosh, Paul McCarthy, Duncan Mortimer, Tom Nichols, Brian Patenaude, Stephen Payne, Emma Robinson, Reza Salimi, Steve Smith, Stamatios Sotiropoulos, Yee Kai Tee, John Vickers, Eelke Visser, Matthew Webster, Anderson Winkler, Mark Woolrich, Yongyue Zhang.

The following people have made significant contributions to the development of FSL v5.0: 

Jesper Andersson, Peter Bannister, Christian Beckmann, Timothy Behrens, Michael Chappell, Gwenaëlle Douaud, Ivana Drobnjak, Eugene Duff, David Flitney, Adrian Groves, Saad Jbabdi, Mark Jenkinson, Duncan Mortimer, Tom Nichols, Brian Patenaude, Reza Salimi, Steve Smith, Stamatios Sotiropoulos, John Vickers, Matthew Webster, Mark Woolrich, Yongyue Zhang.

## Atlas and Standard Space Template Contributors

We are very grateful to the following for contributing to the various brain atlases and standard space templates now included with FSL: David Kennedy and Christian Haselgrove, Centre for Morphometric Analysis, Harvard; Bruce Fischl, Martinos Center for Biomedical Imaging, MGH; Janis Breeze and Jean Frazier, Child and Adolescent Neuropsychiatric Research Program, Cambridge Health Alliance; Larry Seidman and Jill Goldstein, Department of Psychiatry of Harvard Medical School; Simon Eickhoff, Karl Zilles and Katrin Amunts, Research Center Jülich; Louis Collins, Andrew Janke and Alan Evans, Brain Imaging Center, Montreal Neurological Institute; Jack Lancaster and Diana Tordesillas Gutiérrez, Research Imaging Center, UTHSCSA, Texas; Heidi Johansen-Berg and Timothy Behrens, FMRIB; Susumu Mori, Johns Hopkins University; Joern Diedrichsen, Human Motor Control Lab, Bangor; Narender Ramnani, Cognitive Neuroscience Laboratory, Royal Holloway; Roger Gunn and Ilan Rabiner, Imanova; Anne Lingford-Hughes, Imperial College London; Paul Matthews, GlaxoSmithKline.

## Collaborators

We are also grateful to the following for very useful collaborations and past contributions: Andreas Bartsch, Marco Battaglini, Mike Brady, Janis Breeze, Jacqueline Chen, Stuart Clare, Mark Cohen, Louis Collins, Marilena De Luca, Nicola De Stefano, Simon Eickhoff, Alan Evans, Bruce Fischl, Peter Fox, Jean Frazier, David Glahn, Jill Goldstein, Doug Greve, Michael Hanke, Peter Hansen, Christian Haselgrove, Peter Jezzard, Heidi Johansen-Berg, David Kennedy, Peter Kochunov, Angela Laird, Jack Lancaster, Didier Leibovici, Donna Lloyd, Clare Mackay, Jonathan Marchini, Paul Matthews, Karla Miller, Susumu Mori, Jeanette Mumford, Rami Niazy, Bruce Pike, Russ Poldrack, Brian Ripley, Daniel Rueckert, James Saunders, Larry Seidman, Andy Stenger, Irene Tracey, Liqun Wang, Richard Wise, Karl Zilles and the FMRIB Physics, Pain and Disease Groups.

## Other software

FSL uses the following third party software, for which we are very grateful: [Boost](http://www.boost.org/), [BWidget](http://sourceforge.net/projects/tcllib/), [BWidgetExtras](ftp://ftp.deepsoft.com/pub/deepwoods/Other/BWExtras-1.0.0.tar.gz), [Cephes](http://netlib.org/cephes/), [GD](http://www.libgd.org/), [GDCHART](http://www.fred.net/brv/chart/), [libpng](http://www.libpng.org/pub/png/libpng.html), [Newmat](http://www.robertnz.net/nm_intro.htm), [Qt](http://www.trolltech.com/), [Qwt](http://qwt.sourceforge.net/), [Sphere tesselation](http://www.cs.unc.edu/~jon/), [Tcl/Tk](http://www.tcl.tk/) and [zlib](http://www.zlib.net/). In order to view the licensing documentation associated with these packages, look in the appropriate fsl/src or fsl/doc/freeware directory.
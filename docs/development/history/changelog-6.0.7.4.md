# FSL 6.0.7.4, 29th September 2023

## fmrib-unpack 3.7.0 -> 3.7.1

- *MNT: Don't crash if fillVisits(mode) is applied to empty data*

  * Don't crash if fillVisits(mode) is applied to empty data, and don't crash on invalid expressions.
  * Fix logic error handling the `--no_download` flag.
  * Migrate from `setup.py` to `pyproject.toml`.

- *MNT: Administrative and documentation updates*

  Most notably, a brief example on merging two data sets together using a UKB bridging file


### Detailed history

- MNT: Don't crash if fillVisits(mode) is applied to empty data, and don't crash on invalid expressions
- TEST: ignore logger
- MNT: Replace setup.py with pyproject.toml. Add nbclassic to dependencies for demo notebook
- MNT: Use nbclassic instead of notebook
- BF: Wrong logic
- DOC: Changelog
- CI: Clean up pipeline logic
- BF: Bug in pyproject
- TEST: import nbclassic, server mgmt functions have been moved to jupyter_server
- CI: No longer have requirements files
- CI: No longer have requirements files
- DOC: Example on using bridge file to merge two data sets
- MNT: generate demo notebook, bump version
- MNT: version
- MNT: Update internal copies of UKB schema files
- MNT: only pass infer_datetime_format in old pandas versions
- MNT: Regenerate notebooks
- MNT: Missing demo data file
- MNT: Re-generate notebooks again

## fsleyes 1.8.3 -> 1.9.0

- *ENH: Keyboard shortcut to toggle all but first/bottom overlay*

  New keyboard shortcut (control+shift+``f``) to toggle visibility of all overlays except for the first/bottom one, by Christopher G. Schwarz (@CGSchwarzMayo) (!406, [GitHub PR](https://github.com/pauldmccarthy/fsleyes/pull/118>).

- *Add 'fslview' displaySpace, and change orientation warnings behavior*

  Contributed by Christopher G. Shwarz ([Github PR](https://github.com/pauldmccarthy/fsleyes/pull/117))

  * New `fslview` `displaySpace` setting uses `pixdim-flip`, similar to fslview/fsl coordinate space behavior
  * Orientation labels are now displayed in all scenarios, except when `displaySpace` is set to `'scaledVoxel'`
  * The location panel orientation warning may be suppressed with a new `--hideOrientationWarnings` command-line flag

- *MNT: Attempt to find reference images for freesurfer/gifti meshes.*

  FreeSurfer stores undocumented metadata in generated GIfTI files (e.g. from `mris_convert lh.pial lh.surf.gii`); these files also frequently contain vertices defined in the FreeSurfer coordinate system rather than a RAS coordinate system, and so need to be transformed accordingly. This MR detects such files, and initialises the `.MeshOpts.coordSpace` setting to `'torig'` so that the surface should correctly align with its reference image. More information on this can be found at:

   - https://neurostars.org/t/transform-freesurfer-mesh-coordinates-to-volume-coordinates/23693/10
   - https://github.com/nipy/nibabel/discussions/1249

- *MNT: Update for compatibility with matplotlib 3.8*

  It is no longer possible to give a colourmap a name which is different from the key under which it is registered. So fsleyes colour maps with names the same as built-ins are now registered as `"fsleyes_{cmap}"`. The relevant matplotlib change is https://github.com/matplotlib/matplotlib/pull/25479


### Detailed history

- Add ctrl+shift+f hotkey: toggle all overlays > 1
- STY: Minor formatting
- DOC: Changelog, userdocs
- Add 'fslview' displaySpace, and change orientation warnings behavior      * fslview-compatible displaySpace uses pixdim-flip, similar to       fslview/fsl coordinate space behavior     * Orientation labels are now displayed when in pixdim (scaledVoxels)       or pixdim-flip (fslview) spaces, but a warning is displayed that       they may be incorrect.     * This warning may be suppressed with a new --hideOrientationWarnings
- RF: Invert logic to avoid double negative - setting is now showOrientationWarning. Command-line option is still --hideOrientationWarning though. Changed short form to -how from -now
- RF: "fslview-compatible" -> "FSLView mode"
- RF: Simply orientation label logic - show data labels only when display space is 'scaledVoxel', and show L/R/etc labels in all other situations
- RF: Schedule canvas redraws on idle loop to reduce unneceesary redraws
- RF: No need for another warning message
- MNT: Make sure canvas refreshes when labels change
- DOC: Note on FSLView mode, changelog, some troubleshooting comments on wxpython/GLX
- TEST: Unit tests for fslview mode
- MNT: Mention fslview mode in API docs
- MNT: Print message in scaledVoxel shell functions
- DOC: Another note on FSLView mode
- TEST: Don't hang tests via error dialog
- DOC: API note about how all bets are off when setting NiftiOpts.transform directly
- MNT: Adjust display space warning
- CI: default branch changed to main
- MNT: Update for compatibility with matplotlib 3.8 - it is no longer possible to give a colourmap a name which is different from the key under which it is registered. So fsleyes colour maps with names the same as built-ins are now registered as `"fsleyes_{cmap}"`.
- MNT: Need latest props
- DOC: Changelog
- CI: Change dev install regime, as pip install --pre seems to ignore dev packages installed from git repos or tarballs
- TEST: expected cmap names now prefixed
- MNT: Attempt to find reference images for freesurfer/gifti meshes. Try to detect freesurfer-generated giftis, and set coordSpace accordingly.
- TEST: Test find*Image functions
- MNT: Update minimum fslpy version
- DOC: Changelog
- MNT: String formatting
- BF: Make sure Action.destroy is called on tool menu items
- RF: Ability to set a file option (e.g. mesh --refImage) to "None"
- TEST: Update benchmark - mesh reference is now set for surface/nifti loaded from same directory. This is affecting the lightbox slice spacing in this particular test.
- RF: Account for small possibility of atexit handler being called just before nbproc.communicate
- TEST: Colliding cmaps now prefixed with 'fsleyes_'
- MNT: More detailed comment about nibabel quaternion threshold
- DOC: Changelog
- DOC: Notes on software-based GL rendering
- MNT: Version

## fsleyes-plugin-mrs 0.1.0 -> 0.1.1

- *MNT: Specify MRS layout as an entry point*

  FSLeyes 1.8 will support specifying custom layouts as entry points, just like how views, controls, and tools are specified. This allows FSLeyes to associate a specific layout with the library that it is defined within, and enable other plugins that are provided by that library.


### Detailed history

- MNT: Use new FSLeyes >=1.8 layout plugin feature
- MNT: Version
- Update docs and fix small bug.
- Update date in changelog.
- lint
- Update doc version.

## fslpy 3.14.1 -> 3.15.1

- *MNT: Make GIfTI metadata available*

  Adjust the `GiftiMesh` class so that all metadata stored in a GIfTI file is made available via the `Meta` interface.

- ENH: Wrapper function for FSL `cluster` command
- MNT: Pre-release administrative updates

### Detailed history

- MNT: GiftiMetaData.metadata is deprecated
- MNT: convert to f-strings
- MNT: Copy all GIFTI metadata into GiftiMesh metadata store
- CI: Update skip-tests match
- TEST: Don't consider "testdir" to be a test function
- DOC: Changelog
- RF: New Meta.meta property, nicer way to set/access metadata
- TEST: Test Meta.meta property
- ENH: Wrapper function for cluster
- DOC: Changelog
- BF: Convert to string
- TEST: Test cluster wrapper
- DOC: Changelog
- MNT: Pre-release version bump
- DOC: master->main
- MNT: Version
- TEST: Assume that "python" is available on the $PATH - tests run during conda build process may have a very long/invalid shebang line which causes the test to fail
- DOC: Changelog
- MNT: Version

## fsl-base 2306.1 -> 2309.2

- RF: Allow `USR*` `Makefile` variables to be set in the environment
- *ENH: new `open_fsl_report` command*

  - New `open_fsl_report` command, for opening HTML report pages which may contain javascript, such as PNM reports.
  - Replace `setup.py` file with a `pyproject.toml` file.

- DOC: Update changelog and version number
- *TEST: Re-organise unit tests*

  Re-organise unit tests - `feedsRun` now dynamically loads all `test_*py` scripts, and runs all `test_*` functions contained within. Also add a basic test for `open_fsl_report`.


### Detailed history

- RF: Allow USR* make variables to be set via environment variables
- DOC: Changelog
- MNT: Replace setup.py with pyproject.toml
- ENH: New open_fsl_report command, for opening HTML report pages which use javascript
- DOC: Changelog
- TEST: Re-organise tests - feedsRun will load all test_*py scripts, and run all test_* functions contained with. Add basic test for open_fsl_report
- RF: Allow args to be passed to open_fsl_report.main()
- TEST: fix test path, fix to update_fsl_release tests
- MNT: version
- DOC: Changelog
- TEST: Use exes in $FSLDIR - removing option to run tests in-source for the time being
- TEST: Fix update_fsl_release tests
- MNT: Add --yes option to update_fsl_release to skip confirmation prompt
- DOC: Changelog
- TEST: Disable query_installed_packages cache. Make sure that channels are set for env update operations.

## fsl-get_standard 0.2.0 -> 0.3.0


### Detailed history

- MNT: replace setup.py with pyproject.toml
- CI: run unit tests in local project pipeline
- DOC,ENH: Update README describing support for templates of different modality
- ENH,RF: Update fsl_get_standard to support different image modalities. Remove hard-coded image types - manifest files can now define any image types (although "whole_head", "brain", "brain_mask" should always be present). Refactor so that argparse handles invalid values for any of image type, modality, or resolution.
- TEST: Update unit tests
- MNT: Version attribute moved
- MNT: Compatibility, accounting for the exceedingly unlikely possibility that third party code is pulling verison from its original location

## fsl-installer 3.5.6 -> 3.5.7

- *ENH: Allow existing `ArgumentParser` to be passed to `parse_args`*

  This will allow e.g. the `update_fsl_release` script to add additional arguments.


### Detailed history

- ENH: allow existing ArgumentParser to be passed to parse_args
- DOC: Changelog

## fsl_mrs 2.1.12 -> 2.1.13

- *Enh: fMRS group level f-tests*

  2.1.13 (Tuesday 5th August 2023)
  --------------------------------
  - Add group level f-tests to the `fmrs_stats` tool. First level f-contrasts are not yet implemented.
  - Removed / retained indices after `fsl_mrs_proc unlike` (and related functions) are now listed in the NIfTI-MRS headers under the key "DIM_DYN Indices".


### Detailed history

- Added higher level ftests to the flameo wrapper
- Add ftests to fmrs_stats tool
- Update changelog
- Update doc.
- Update doc.
- Add tracking of indicies with unlike command.
- Typo

## fsl-pipe 1.0.0 -> 1.0.1

- Fix duplicate jobs when linked placeholders are used
- Check for warnings in tests
- Bump version: 1.0.0 → 1.0.1

### Detailed history

- Fix duplicate jobs when linked placeholders are used
- Check for warnings in tests
- Bump version: 1.0.0 → 1.0.1

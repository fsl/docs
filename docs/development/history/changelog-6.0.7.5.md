# FSL 6.0.7.5, 15th November 2023


## fsleyes 1.9.0 -> 1.10.0

- *RF: Changed the behaviour of the _Modulate alpha by intensity_ setting for mesh overlays*

  Now, if the _Hide clipped areas_ setting is enabled, the mesh transparency is modulated by intensity. But if the _Hide clipped areas_ setting is disabled, the mesh data colour is blended with its background colour.

- *Add `${FSLEYES_SITE_CONFIG_DIR}` as a place for colour maps and lookup tables*

  - Add the ability to store colour maps and lookup table files in a site-specific configuration directory, which can be set by a `${FSLEYES_SITE_CONFIG_DIR}` environment variable. Contributed by Rob Reid at https://github.com/pauldmccarthy/fsleyes/pull/121.
  - Change the way that FSLeyes layouts are stored - they are now saved as plain-text files in `<settings-dir>/layouts/`, or `${FSLEYES_SITE_CONFIG_DIR}/layouts/`.
  - Default command-line arguments can now be stored in `${FSLEYES_SITE_CONFIG_DIR}/default_arguments.txt`. If `<settings-dir>/default_arguments.txt` exists, it will take precedence.


### Detailed history

- RF: Allow mesh width to be changed if vertex data is set
- RF: Change modulate-by-alpha behaviour on mesh overlays - if modalpha and hide clipped, use alpha modulation. Or if mod alpha and not hide clipped, blend with flat colour.
- TEST: Update mesh modalpha tests
- TEST: Sanity check test for overlay display panel
- TEST: Missing benchmark
- CI: Make test failures available as CI artifacts
- BF: Using wrong register
- BF: Need a depth texture, otherwise z ordering is wrong in some situations
- BF: Whether or not to clip was being overwritten by the disacardClipped setting
- DOC: Note about default arguments
- MNT: avoid (harmless) crash when shutting down nb server
- Adds $FSL_SITE_CONFIG_DIR as a place for cmaps and luts
- s/FSL_SITE/FSLEYES_SITE/g
- RF: Simplify cmap/lut initialisation. The getCmapDir/getLutDir functions have been renamed to getCmapDirs/getLutDirs, and each return a list of map directories in order of priority
- DOC: Update colourmaps module documentation
- ENH: New ability to save layouts as files, similar to cmaps/luts
- MNT: Move key funcs into separate "utils" package.
- RF: Save custom layouts as files rather than in pickle from now on
- DOC: Update layouts module docs
- BF: Changed var name
- MNT: I've removed other functions in this refactor, so don't see the point in retaining+deprecating any. It's highly unlikely that these functions are used by third-party programs/scripts.
- TEST: Update layout/cmaps unit tests
- DOC: Update user documentation regarding $FSLEYES_SITE_CONFIG_DIR and custom layout files
- DOC: Changelog, some formatting fixes
- MNT: Allow site-wide default arguments
- DOC: Update docs on default args
- BF: userArgs was being left as non-existent file path
- DOC: Some formatting fixes
- TEST: Unit test for default arguments. Move some helper functions up to fsleyes.tests package

## fsl-fugue 2201.3 -> 2201.4

- ENH: GEHC gui options, kindly provided by Brice Fernandez

### Detailed history

- ENH: GEHC gui options, kindly provided by Brice Fernandez

## fsl-nets 0.8.0 -> 0.8.1

- BF: Use `np.nanpercentile` when calculating `nets.groupmean` plot limits in case there are any nans or infs

### Detailed history

- BF: Use nanpercentile when calculating groupmean plot limits in case there are any nans or infs
- DOC: Readme

## fsl-xtract 1.6.4.1 -> 1.6.4.2

- fix make_subplots error

### Detailed history

- fix make_subplots error

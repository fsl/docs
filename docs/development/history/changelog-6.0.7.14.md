# FSL 6.0.7.14, 8th October 2024

- [**file-tree** 1.4.0 -> 1.4.1](#file-tree)
- [**fsleyes** 1.12.4 -> 1.12.6](#fsleyes)
- [**fslpy** 3.21.0 -> 3.21.1](#fslpy)
- [**fsl-avwutils** 2209.2 -> 2209.3](#fsl-avwutils)
- [**fsl-base** 2407.0 -> 2408.0](#fsl-base)
- [**fsl-bint** 2111.0 -> 2111.1](#fsl-bint)
- [**fsl-fdt** 2202.8 -> 2202.10](#fsl-fdt)
- [**fsl-film** 2111.0 -> 2111.1](#fsl-film)
- [**fsl-filmbabe** 2111.0 -> 2111.1](#fsl-filmbabe)
- [**fsl-flameo** 2111.0 -> 2111.1](#fsl-flameo)
- [**fsl-get_standard** 0.3.0 -> 0.5.1](#fsl-get_standard)
- [**fsl-installer** 3.14.0 -> 3.15.1](#fsl-installer)
- [**fsl-mm** 2111.0 -> 2111.1](#fsl-mm)
- [**fsl-pipe** 1.0.3 -> 1.1.1](#fsl-pipe)
- [**fsl-ptx2** 2111.4 -> 2111.5](#fsl-ptx2)
- [**fsl-pyfix** 0.8.1 -> 0.9.0](#fsl-pyfix)
- [**fsl-sub-plugin-slurm** 1.6.4 -> 1.6.5](#fsl-sub-plugin-slurm)
- [**fsl-tbss** 2111.1 -> 2111.2](#fsl-tbss)

## <a id="file-tree" href="#file-tree">file-tree 1.4.0 -> 1.4.1</a>

- [*Link xarray and fix to_string output*](https://git.fmrib.ox.ac.uk/fsl/file-tree/-/merge_requests/39)
- [*V1.4.1*](https://git.fmrib.ox.ac.uk/fsl/file-tree/-/merge_requests/40)

**Detailed history**

- `BF: link across linked xarray dimensions`
- `Clean remnants of old linkages`
- `Sort keys when printing for more consistent output  Also, moved placeholders to string conversion to its own method`
- `Explain changes`
- `Add section for v1.4.1`
- `Bump version: 1.4.0 → 1.4.1`

## <a id="fsleyes" href="#fsleyes">fsleyes 1.12.4 -> 1.12.6</a>

- [*MNT: Add support to FEAT cluster panel for browsing f-test results*](https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes/-/merge_requests/452)
- [*BF: Fix bug in handling of `-ll` / `-lh` options.*](https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes/-/merge_requests/453)
  These boolean flag options were incorrectly marked as expecting to be given a value, which was causing issues in the first pass of arguments to identify overlay files.
- [*MNT: Some more messages to filter*](https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes/-/merge_requests/456)
- [*MNT: Finally it appears to be possible to set the macOS application menu title*](https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes/-/merge_requests/457)
  I suspect that this has been possible since the release of wxPython 4.x, but I just never got around to trying it.

**Detailed history**

- `BF: Don't set high clipping value, otherwise max voxel may be clipped`
- `MNT: Require latest fslpy`
- `ENH: Update cluster panel to allow browsing of F-test results`
- `MNT: Add widget accessors for testing`
- `TEST: Add dummy FEAT analysis and cluster panel unit test`
- `BF: -ll / -lh options incorrectly marked as expecting a value`
- `DOC: Update FIX label file format`
- `DOC: Update lightbox cli examples`
- `MNT: Some more messages to filter`
- `MNT: Finally it appears to be possible to set the macOS application menu title`

## <a id="fslpy" href="#fslpy">fslpy 3.21.0 -> 3.21.1</a>

- [*MNT: Support FIX label files with classification probabilities*](https://git.fmrib.ox.ac.uk/fsl/fslpy/-/merge_requests/464)
  Versions of FIX >= 1.069 save classification probabilities to the `fix4melview` label files. I never knew this, as the version of FIX on the FMRIB cluster was never updated beyond version 1.067. This MR updates the `loadFixLabels` and `saveFixLabels` functions accordingly.

**Detailed history**

- `MNT: Modify loadLabelFile to accept files with classificationprobabilities`
- `TEST: Test loadLabelFile with classification probabilities`
- `MNT: Modify saveLabelFile to support saving classification probabilities`
- `TEST: Test saveLabelFile with classification probabilities`
- `MNT: Typos`

## <a id="fsl-avwutils" href="#fsl-avwutils">fsl-avwutils 2209.2 -> 2209.3</a>

- [*Remove border padding from `-roc` option*](https://git.fmrib.ox.ac.uk/fsl/avwutils/-/merge_requests/24)
  The `-roc` option had an undocumented hard-coded border padding. We have removed this border padding for consistency with other tools.
- [*Convert nifti voxel coorinates to newimage (internal) coordinates for `-tfceS`*](https://git.fmrib.ox.ac.uk/fsl/avwutils/-/merge_requests/25)
  Most fslmaths functions that rely on coordinates will transform nifti voxel coordinates to the internal FSL coordinates. This transform was absent from the `-tfceS` option and has been updated now.
- [*Ensure that arguments to fslmaths `-thr` options are numeric*](https://git.fmrib.ox.ac.uk/fsl/avwutils/-/merge_requests/26)
  Previously, images could mistakenly be provided as inputs to `-thr` and `-uthr` options. This resulted in a silent bug where the threshold was set to 0. We now ensure that arguments are numeric, and have made the help text more clear.
- [*MNT: Remove dependency on avscale*](https://git.fmrib.ox.ac.uk/fsl/avwutils/-/merge_requests/27)
  `fslreorient2std` currently calls `avscale` which is in the `flirt` project. To avoid circular project dependencies ( and simplify the code ) `fslorient` is now called instead.

**Detailed history**

- `Update border value to zero`
- `ensure newimagevox`
- `actually _use_ the transformed coord`
- `add isNumber check to thr inputs`
- `show when number or image needed`
- `add thr and uthr tests`
- `Remove unnecessary file cleanup`
- `MNT: Remove dependency on avscale`

## <a id="fsl-base" href="#fsl-base">fsl-base 2407.0 -> 2408.0</a>

- [*RF: Re-write object file dependency generation logic*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/112)
  Now, instead of pre-generating a `depend.mk` file containing source dependencies, a single `.deps/<file>.d` file is generated for each source file at the time of compilation. This makes the build system more flexible, for example allowing other steps such as code generation to be executed before compilation.

**Detailed history**

- `MNT: USe -isystem instead of -I for FSL and CUDA include dirs, so they are not considered in dependency generation`
- `RF: Prefix FSL dir variables with "FSL_" so reduce the risk of them being clobbered by project makefiles.`
- `RF: Tweak construction of NVCCFLAGS`
- `RF: Rewrite autodep generation. Now, instead of pre-generating a depend.mk file containing source dependencies, a single .d file is generated for each source file at the time of compilation.`
- `DOC: Readme`
- `RF: Allow project to specify SRCDIR, within which all sources are located`
- `DOC,MNT: Changelog, version`

## <a id="fsl-bint" href="#fsl-bint">fsl-bint 2111.0 -> 2111.1</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/bint/-/merge_requests/2)

**Detailed history**

- `MNT: Normalise indentation, remove unnecessary debug and use of "OUT" macro from miscmaths.h`

## <a id="fsl-fdt" href="#fsl-fdt">fsl-fdt 2202.8 -> 2202.10</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/fdt/-/merge_requests/26)
- [*dtifit: update savetensor flag to save tensors by default*](https://git.fmrib.ox.ac.uk/fsl/fdt/-/merge_requests/27)
  Made the savetensor flag default to True (so its presence technically has no effect, just keep the legacy code working)

  Added a new notensor flag to avoid saving the tensor.

**Detailed history**

- `MNT: Remove use of OUT macro from miscmaths.h`
- `Made the savetensor flag default to True (technically has no effect) Added a new notensor flag to avoid saving the tensor.`

## <a id="fsl-film" href="#fsl-film">fsl-film 2111.0 -> 2111.1</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/film/-/merge_requests/4)

**Detailed history**

- `MNT: Remove use of OUT macro from miscmaths.h`

## <a id="fsl-filmbabe" href="#fsl-filmbabe">fsl-filmbabe 2111.0 -> 2111.1</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/filmbabe/-/merge_requests/3)

**Detailed history**

- `MNT: Remove use of OUT macro from miscmaths.h`

## <a id="fsl-flameo" href="#fsl-flameo">fsl-flameo 2111.0 -> 2111.1</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/flameo/-/merge_requests/3)

**Detailed history**

- `MNT: normalise indentation`
- `MNT: Remove use of OUT macro from miscmaths.h`

## <a id="fsl-get_standard" href="#fsl-get_standard">fsl-get_standard 0.3.0 -> 0.5.1</a>

- [*ENH: Allow the `$FSL_STANDARD` environment variable to be set to an external directory containing standard template files*](https://git.fmrib.ox.ac.uk/fsl/get_standard/-/merge_requests/6)
  Now, the `$FSL_STANDARD` environment variable can either be the name of a directory inside `$FSLDIR/data/`, or a path to any other directory containing a standard template data set.
- [*ENH: New `--standard` option*](https://git.fmrib.ox.ac.uk/fsl/get_standard/-/merge_requests/7)
  This MR adds a new `--standard` command-line option, which allows users/scripts to select templates on the command line rather than via the `$FSL_STANDARD` environment variable. The `$FSL_STANDARD` variable will be honoured if set, but if `--standard` is provided it will be used instead.

  The `--standard` option accepts any value that the `FSL_STANDARD` environment variable accepts - one of:
   - a template dataset identifier (the name of a sub-directory within `$FSLDIR/data/`)
   - the path to a directory containing a template data set

  Examples:
  ```
  # use OMM templates from $FSLDIR/data/omm/Oxford-MM-1
  $ fsl_get_standard --standard omm/Oxford-MM-1
  /usr/local/fsl/data/omm/Oxford-MM-1/OMM-1_T1_head.nii.gz

  # use OMM templates from local clone of omm repository
  $ fsl_get_standard --standard ~/my-local-projects/oxford-mm-templates/Oxford-MM-1
  /home/paulmc/my-local-projects/oxford-mm-templates/Oxford-MM-1/OMM-1_T1_head.nii.gz
  ```

  This MR also makes the `fsl_get_standard` interface a little more flexible - previously, the default modality/resolution values were set to the first modality/resolution that was listed in the dataset `manifest.txt` file. This could result in invalid values, e.g. if a particular image type did not have the default modality/resolution. Now, if the modality and/or resolution is not specified, the first file in `manifest.txt` _for the requested image type_ is selected.

  Finally, the `$FSLDIR/data/standard/FMRIB58_FA_1mm.nii.gz` and `$FSLDIR/data/standard/FMRIB58_FA-skeleton_1mm.nii.gz` files have been added to the default MNI152 manifest, e.g.:

  ```
  $ fsl_get_standard FA
  /usr/local/fsl/data/standard/FMRIB58_FA_1mm.nii.gz

  $ fsl_get_standard FA_skeleton
  /usr/local/fsl/data/standard/FMRIB58_FA-skeleton_1mm.nii.gz
  ```
- [*MNT: Allow default templates to be selected with `standard`/`mni152`/`MNI152`*](https://git.fmrib.ox.ac.uk/fsl/get_standard/-/merge_requests/8)

**Detailed history**

- `ENH: The FSL_STANDARD variable can now be set to an external directory containing standard template files`
- `TEST: Test setting FSL_STANDARD to an external directory. Test custom image types`
- `DOC: Readme`
- `RF: Set modality/resolution defaults according to the user selection, as a template dataset may not have all resolutions/modalities for all image types. Change Manifest.imageTypes/modalities/resolutions properties to methods, and allow modalities/resolutions to be restricted`
- `MNT: Add MNI152 FA images to default template`
- `RF: Don't set default values - the Manifest.get() method does this anyway`
- `ENH: New --standard option, allowing the FSL_STANDARD env var to be overridden`
- `TEST: Re-arrange tests, add test for --standard cli option`
- `TEST: Test setting FSL_STANDARD to a sub-directory, for supporting different versions of a template dataset`
- `RF: Use modality=DTI and image type=FA/FA_skeleton, otherwise we would end up with modality=V1/V2/V3/L1/L2/...`
- `TEST: Fix FA / DTI tests`
- `MNT: function docs`
- `MNT: Allow default templates to be selected with "standard"/ "mni152"/ "MNI152". Better error message when there is a problem with the manifest file`
- `DOC: readme`

## <a id="fsl-installer" href="#fsl-installer">fsl-installer 3.14.0 -> 3.15.1</a>

- [*Update installation registration process*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/97)
  Incorporates !96.
   - Send registration information encoded in `application/x-www-form-urlencoded` format, rather than `application/json`
   - Share CSRF token with server for forgery protection
   - Send target computer locale, rather than local time stamp, as part of registration information
   - Change `uname` call so that local host name is not included
   - Only read OS information from `/etc/os-release`, rather than the first file matching `/etc/*-release`
- [*MNT: Work around conda/macOS version detection problem*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/98)
  The macOS version is being mis-detected by some miniconda installations (specifically the python build included with the miniconda installation) on some macOS environments. Setting the `SYSTEM_VERSION_COMPAT=0` environment variable seems to be a viable work-around.

  - https://github.com/conda/conda/issues/13832
  - https://eclecticlight.co/2020/08/13/macos-version-numbering-isnt-so-simple/

**Detailed history**

- `Incorrect indentation`
- `Only capture information from /etc/os-release otherwise you will pull in the same information from multiple sources. This is guaranteed to exist on platforms running systemd.`
- `Filter uname to remove local hostname`
- `New web registration: Don't pass timestamp - registration server will add this information Add locale information for stats about install host's configured language`
- `Add little script to enable a test registration to the FSL server without going through a full install`
- `Add support for cookie preservation during registration process`
- `Extra space`
- `Rename post function to reflect that it does more than just post Add python 2 equivalent for URL encoder Open requires string, not PosixPath`
- `RF,STY: Call uname -mrsv rather than uname -a to omit hostname. Other formatting tweaks.`
- `TEST: Fix use of unquote function`
- `DOC: function description`
- `RF: Don't modify input dict`
- `TEST: Fix registration tests`
- `MNT: Set SYSTEM_VERSION_COMPAT=0 to hopefully avoid weird conda/macOS version issue`
- `MNT: comment fix`

## <a id="fsl-mm" href="#fsl-mm">fsl-mm 2111.0 -> 2111.1</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/mm/-/merge_requests/4)

**Detailed history**

- `MNT: normalise indentation`
- `MNT: Remove use of OUT macro from miscmaths.h`

## <a id="fsl-pipe" href="#fsl-pipe">fsl-pipe 1.0.3 -> 1.1.1</a>

- [*Ensure jobs in `JobsList` are always sorted*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/17)
  This means that I don't have to call `filter` just to sort the jobs in `split_pipeline`, which allows `split_pipeline` to run without ever checking for existing files on disk.
- [*Fix batching when `name` or `extra_vars` are set*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/18)
  `name` parameters are now correctly combined.
  `extra_vars` parameters are now turned into a tuple before hashing.
- [*V1.1*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/19)
  Bump the version number to v1.1.0
- [*BF: Allow Var for placeholders with singular values*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/20)
  Based on bug report from Hossein, who wants to do this in TBSS-pipe
- [*V1.1.1*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/21)

**Detailed history**

- `Describe change`
- `Ensure jobs are always sorted in JobList`
- `Test whether split_pipeline runs without checking input files.  This reproduces an error reported by Fidel.`
- `Remove filter from split_pipeline  This is no longer needed as the jobs are sorted anyway.`
- `Ensure that exists is really not called in split_pipeline  Checked using pytest-mock`
- `Use requirements_dev.txt in tests`
- `Add coverage tool to development requirements`
- `Clarify error message`
- `Merge multiple submit params name for batch job`
- `Ensure submit params are hashable before calling set()`
- `Add CHANGELOG section to 1.1.0`
- `Remind to update CHANGELOG`
- `Bump version: 1.0.3 → 1.1.0`
- `BF: Allow Var for placeholders with singular values`
- `Fix singular var`
- `Bump version: 1.1.0 → 1.1.1`

## <a id="fsl-ptx2" href="#fsl-ptx2">fsl-ptx2 2111.4 -> 2111.5</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/ptx2/-/merge_requests/18)

**Detailed history**

- `MNT: Remove use of OUT macro from miscmaths.h`

## <a id="fsl-pyfix" href="#fsl-pyfix">fsl-pyfix 0.8.1 -> 0.9.0</a>

- [*BF: Fix bug in `zstattofuncratio` feature calculation*](https://git.fmrib.ox.ac.uk/fsl/pyfix/-/merge_requests/23)
  The `zstattofuncratio` feature could occasionally be negative, due to negative values in `func_mean` (which could occur, for example, as a result of resampling or smoothing). Changing the `fslmaths` operation order eliminates the possibility of negative values in the output. Reported and fixed by Takuya Hayashi.
- [*DOC: Clarify usage of `-h` option with respect to filtering of CIfTI time series data*](https://git.fmrib.ox.ac.uk/fsl/pyfix/-/merge_requests/24)
  The `-h` option is not only used to perform high-pass filtering of motion confounds prior to regression. It is also used to perform high-pass filtering of CIfTI time series data. The command-line usage has been updated accordingly.
- [*RF: Use `mc/prefiltered_func_data_mcf_conf_hp.nii.gz` as motion confounds if it exists; save classifier probabilities to `fix4melview` file*](https://git.fmrib.ox.ac.uk/fsl/pyfix/-/merge_requests/25)
   * If the `mc/prefiltered_func_data_mcf_conf_hp.nii.gz` already exists in the input data directory, use it as the motion confounds, instead of loading and high-pass filtering the MCFLIRT results from `mc/prefiltered_func_data_mcf.par`. This is required to support HCP scripts which perform their own filtering of motion confounds, and save them to `mc/prefiltered_...._hp.nii.gz`.
    Reported on the FSL mailing list: https://www.jiscmail.ac.uk/cgi-bin/wa-jisc.exe?A2=FSL;199b80ab.2409
   * Classifier probabilities are now saved to the `fix4melview` label file. The original FIX was adjusted in 1.069 to do this, but this feature was never ported to pyfix. This was probably because the version of FIX on the FMRIB cluster was never updated beyond version 1.067.

**Detailed history**

- `BF: Change fslmaths operation order to eliminate the possibility of negative values in output`
- `MNT: Use reg/standard.nii.gz, if it exists, for vein mask registration, otherwise fall back to fsl_get_standard`
- `MNT: Correct CLI documentation r.e. -h - it is applied to CIFTI data regardless of the use of -m`
- `TEST: Test CIfTI cleaning`
- `TEST: Relax fft feature threshold`
- `RF: If mc/prefiltered_func_data_mcf_conf_hp.nii.gz already exists in the input data, use it as the motion confounds instead of loading the MCFLIRT results from mc/prefiltered_func_data_mcf.par`
- `RF: Save classifier probabilities to fix4melview file`
- `MNT: Update fslpy pin for new saveLabelFile(probabilities) option`
- `TEST: Test handling of motion parameter estimates`
- `RF: Output filtered_func_data.ica path (melodic), rather than <analysis>.ica path (Melodic_gui)`

## <a id="fsl-sub-plugin-slurm" href="#fsl-sub-plugin-slurm">fsl-sub-plugin-slurm 1.6.4 -> 1.6.5</a>

- [*BF: Test for being an instance of list not alias*](https://git.fmrib.ox.ac.uk/fsl/fsl_sub_plugin_slurm/-/merge_requests/8)
  The correct test here should be `extra_args` being of type `list` rather than an _alias_ for the `list` class.

**Detailed history**

- `BF: Test for being an instance of list not alias`
- `Add handling of a missing default coprocessor class. Fix the selection of this default class Refactor coprocessor selection Refactor log file name definition Add SMT option`
- `Clean up of jobtime handling`
- `Minor lint clean up`
- `Update copyright notice`
- `Hyperlinks to install/build and changes files`
- `Switch to --mem for requesting RAM to side-step SMT issues`
- `Note additional environment variable`

## <a id="fsl-tbss" href="#fsl-tbss">fsl-tbss 2111.1 -> 2111.2</a>

- [*MNT: Remove use of `OUT` macro from `miscmaths.h`*](https://git.fmrib.ox.ac.uk/fsl/tbss/-/merge_requests/4)

**Detailed history**

- `MNT: Remove use of OUT macro from miscmaths.h`

# FSL 6.0.7.11, 29th April 2024


## fsleyes 1.10.4 -> 1.11.0

- RF: Automatically find `$FSLDIR` if it is not set
- TEST: Remove small chance of naming collisions between test plugin modules
- *MNT: Adjust jupyter notebook startup time*

  Startup time seems to be a bit longer under Python 3.12.

- *BF: Fix issue with high voxels being clipped in probabilistic atlases*

  Fixed by adding a small offset to the upper clipping range when loading a probabilistic atlas via the atlas panel.

- *ENH: New lightbox view feature to configure slices in terms of voxel coordinates*

  * A new feature available on lightbox views, under the *Tools* menu, which allows the displayed slices to be configured in terms of voxel coordinates rather than proportions.
  * A corresponding command-line option, `--asVoxels`, which causes the `--zrange` and `--sliceSpacing` options to be interpreted as voxel coordinates (relative to the selected image, which defaults to the last image).
  * A new command-line option `--sampleSlices`, which controls how lightbox slices are sampled. This is used by `--asVoxels`, and is really an internal setting not expected to be of much use directly, but is exposed on the command-line for completeness.
  * Fixed a Python 3.12 compatibility issue with command-line argument parsing.


### Detailed history

- MNT: add Dockerfile for python 3.12
- CI: drop py38 from testing, add py312
- ENH: Attempt to automatically derive $FSLDIR from the location of sys.executable, if it is not set
- Change wxpy build process  - I suspect that some of my old kludges are no longer required  - Build from pypi tarball instead of github clone  - Bundled sip files are incompatible with py312, so need to be re-generated (via build.py sip)  - Using same process as in conda-forge wxpy recipe
- TEST: Remove small chance of naming collisions between test plugin modules
- MNT: Notebook server seems to need a bit more time to start up under py312
- TEST: Allow more startup time in testing
- MNT: Am seeing warnings about unimplemented methods, but can't find any documentation on what these methods are supposed to actually do
- MNT: Numpy is going to disallow integer overflow casting in the future
- CI: Change test env setup procedure so we don't end up with development versions of anything from pypi (e.g. numpy 2.0.0rc1)
- BF: Add a small offset to uipper clipping range when loading a probabilistic atlas via the atlas panel
- ENH: New LightBoxCanvasOpts.sampleSlices property which can control sampling slices from the beginning of each slice region, instead of from the centre
- ENH,BF: GUI widget for sampleSlices. Add a small fixed offset to make sure that adjacent slice positions don't resolve to the same slice
- ENH: New LightBoxSamplePanel, added as an option under Tools, allowing the user to control lightbox slices in terms of voxels
- RF: Make sampleSlices logic a bit more complicated - calculate slice locations such that zlo falls at the beginning of a slice. This allows the starting slice to be selected regardless of slice spacing.
- BF: Prevent lightbox slice labels like "-0"
- ENH: Widget and button for lightbox sampling
- DOC: Overview of lightbox sample dialog, and lightbox internals
- ENH: CLI option for sampleSlices
- RF: Move voxel->zrange logic into LightBoxOpts, so it can be called from parseargs
- MNT: Making a horrible hack horribler - internal changes affecting ArgumentParser._get_option_tuples in python 3.12.3
- MNT: Give MockCanvasPanel a reference to the displayCtx, as the LightBoxOpts.setSlicesFromVoxels needs access to it
- MNT: Minor hack in LightBoxCanvas to prevent it from modifying pre-calculated Z range
- ENH: New --asVoxels command-line option which allows lightbox zrange/spacing to be specified as voxel coordinates
- DOC: Changelog (note prospective release date change)
- BF: Only apply asVoxels whed it has been passed as a cli argument (the applySpecial functions are always called)
- BF: Initialise slice properties once on init, then freeze if requested
- TEST: Tests/benchmarks for new lightbox options
- DOC: Typo
- TEST: More benchmarks
- DOC: unused argument
- MNT: initialise lbsample action/menu item
- TEST: basic test for lbsample

## fsl_mrs 2.1.19 -> 2.1.20

- *ENH: Command line quantification options*

  Changes for improved quantification interface and command line options.
  - Minor improvements in MRSI plotting and preprocessing routines using average spectra.
  - Added default relaxation values for 1.5 tesla.
  - You can now specify custom T1 and T2 values to `fsl_mrs` using the `--t1-values` and `--t2-values`.
  - The (t1/t2/tissue fraction) values used in the qunatification stage are now output as a `quantification_info.csv` file.
  - `quantification_info.csv` can be passed to any of `--tissue_frac`, `--t1-values` and `--t2-values` as inputs.


### Detailed history

- ENH: Command line quantification options

## fsl-mmorf v0.3.2 -> v0.3.3

- *MNT: Remove dead code and unused test files*

  Will salvage what we can from the tests at a future date.

- *RF: Merge various pairs of cost function classes which have large amounts of shared code*

  The `CostFxnCompoundBiasFieldVarianceScaled` and `CostFxnCompoundWarpFieldVarianceScaled` classes are identical with the exception of a single value used during initialisation. This MR merges the two into a single class called `CostFnxnCompoundVarianceScaled`, which is templated on the `WarpField` and `BiasField` classes.

  Similarly, the `CostFxnBendingEnergy` and `CostFxnBendingEnergyBiasfield` classes have quite a lot of duplicated code, and differ primarily in their implementation of the `CostFxn::cost()` function. These two classes have similarly been merged into a single `CostFxnBendingEnergy` class, which is templated on the `WarpFieldBSpline` and `BiasFieldBSpline` classes.

- *MNT: Remove duplicated code across `CostFxn` classes*

  Many functions are duplicated across the various `CostFxn` classes. This MR proposes a new `CostFxnSplineUtils` module which contains a collection of stand-alone functions for performing calculations on sparse diagonal matrices.

- MNT: New `CostFxnJacobianUtils` module containing code shared by the `CostFxnLogJacobian` cost function classes.
- RF: New `WarpUtils` module which contains commonly used functions for linear/nonlinear transformations
- *MNT: New `CostFxnSSDWarpFieldUtils` module containing code shared by the `CostFxnSSDWarpField` cost function classes.*

  This MR is based on !7, which should be merged before this is merged.

  Merges the `CostFxnSSDWarpFieldSymmetricMaskedExcluded` and `CostFxnSSDWarpFieldSymmetricMaskedExcludedDiagHess` classes into a single class, templated on a boolean parameter which dictates how the hessian is calculated. Most of the logic has been converted into standalone functions, located in a new module called `CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils`.

- *MNT: Add sane error messages for invalid configurations.*

  The large assert statment at the beginning of `RegistrationCoordinatorMultimodal` was useful for preventing invalid registration configurations from running, but was not very useful for helping end users debug why their code was failing. This has now been supplemented with more helpful config checking in `main` that should guide users to where in their config they have made any errors.

- *MNT: Merge `CostFxnTensor*` cost functions into a single class, templated on `CostFxn::HessType`.*

  Common logic has been moved into standalone functions within a new `CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils` module.

  Also moved the `create_empty_jtj` function (defined in several locations) into the `CostFxnSplineUtils` module.


### Detailed history

- Spelling
- Update README
- Update README
- Update README.md to point to the fsl version of the MMORF repo.
- Update README.md
- MNT: Remove dead code and unused test files - will salvage what we can from the tests at a future date.
- RF: Merge CostFxnBendingEnergy and CostFxnBendingEnergyBiasfield into a single class, templated on WarpFieldBSpline / BiasFieldBSpline
- RF: Merge CostFxnCompound[BiasField|WarpField]VarianceScaled classes into a single class templated on WarpField/BiasField
- RF: Make initial cost a construction parameter; hard-code values in registration coordinator for the time being
- MNT: Remove repeated code - both if/else blocks are equivalent
- RF: New module which will contain some stand-alone functions used by various cost functions for spline-field-based calculations. Currently contains calculate_sub_jte
- RF: Move calculate_offsetes and calculate_sub_jtj_symmetrical functions (separate diaghess implementation for the latter) to CostFxnSplineUtils
- RF: Move calculate_sub_jtj_non_symmetrical (separate diaghess impl) to CostFxnSplineUtils
- RF: Move calculate_sub_grad to CostFxnSplineUtils
- RF: Move spline_as_vec to a CostFxnSplineUtils
- RF: Move all splineutils funcs into a nested namespace, to make it clearer where they are defined
- RF: Remove multiple redundant definitions of affine_transform function, move to a new "AffineUtils" module
- MNT: Remove redundant/unused add_positions function definitions
- RF: Rename AffineUtils -> WarpUtils, and rename affine_transform -> apply_affine_transform
- RF,BK: Move warp transform functions from WarpFieldBSplineto WarpUtils; will update client code in next commit
- RF: Replace all uses of WarpFieldBSpline apply* function with calls to standalone functions in WarpUtils. Replace separate apply_warp+affine_transform with a single call to apply_nonlinear_then_affine_transform
- MNT: Pass const references to add_positions instead of passing by value
- MNT: Add sane error messages for invalid configurations.
- RF: Move code common to the CostFxnLogJacobian classes into a separate module of standalone functions
- RF: Rename to more specific CostFxnLogJacobianSingularValuesUtils
- RF: Move hessian calculation routines into logjacobian utils. Choice of whether to calculate full hessian or approximation can be controlled via a boolean parameter to the hess() function
- RF: Merge the CostFxnLogJacobianSingularValues* classes by adding a boolean template parameter which controls whether full/diagonal hessian calculation is used
- BF: Sparse diag offset calculation differs depending on whether full or diag hess calculation is being performed
- BF: Missed include guard
- RF: Template CostFxnLogJacobian classes on an enum rather than a bool, as it makes the code more readable
- RF: Move all common CostFxnSSDWarpField* code to a separate Utils module
- RF: Merge CostFxnSSDWarpField* classes into a single class, templated on a boolean paramter which controls how hessian is calculated
- MNT: Update includes (missed during manual merge conflict resolution)
- RF: Template CostFxnSSDWarp classes on the CostFxn::HessType enum rather than a bool, as it makes the code more readable
- RF: Merge CostFxnTensor* classes into a single class, templated on the CostFxn::HessType enum. Move all common logic to standalone functions in a new CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils module
- RF: Remove duplicate definitions of create_empty_jtj function, move into CostFxnSplineUtils
- MNT: Bump version, update CHANGELOG

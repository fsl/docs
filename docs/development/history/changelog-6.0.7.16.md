# FSL 6.0.7.16, 12th December 2024

- **New** spmic-cudimot
- Removed fsl-cudimot
- [**fsl-base** 2408.0 -> 2411.0](#fsl-base)
- [**fsl-data_omm** 1.0 -> 1.1](#fsl-data_omm)
- [**fsl-fugue** 2201.4 -> 2201.5](#fsl-fugue)
- [**fsl-utils** 2203.4 -> 2203.5](#fsl-utils)
- [**fsl-xtract** 2.0.5 -> 2.0.6](#fsl-xtract)

## <a id="fsl-base" href="#fsl-base">fsl-base 2408.0 -> 2411.0</a>

- [*BF: Adjust `Makefile` logic so that `${FSLDEVDIR}/include` takes precedence over `${FSLDIR}/include`*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/113)
- [*BF: Fix bug in C compilation rule*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/114)
  The rule was using `${C}` for the C compiler instead of `${CC}`

**Detailed history**

- `BF: Adjust fslversion to check for different variants of conda env update commands when searching through env update history for "official" updates`
- `MNT: Allow some make vars to be overridden by the env/prior to inclusion of default.mk`
- `RF: Adjust construction of compiler flags so that $FSLDEVDIR/include takes precedence over  $FSLDIR/include`
- `DOC: Changelog, readme`
- `BF: Typo in .c build rule - variable is ${CC}, not ${C}`

## <a id="fsl-data_omm" href="#fsl-data_omm">fsl-data_omm 1.0 -> 1.1</a>

- [*Add skeletonised FA for TBSS*](https://git.fmrib.ox.ac.uk/fsl/oxford-mm-templates/-/merge_requests/15)
- [*Correct naming convention of warps*](https://git.fmrib.ox.ac.uk/fsl/oxford-mm-templates/-/merge_requests/16)
  NB! This is a breaking change! Essentially, the filenames have been swapped for MNI->OMM and OMM->MNI transformations to reflect *the direction in which they transform data*, rather than the direction in which the warp is moving points.
- [*MNT: Add manifest files for use by `fsl_get_standard`*](https://git.fmrib.ox.ac.uk/fsl/oxford-mm-templates/-/merge_requests/17)
  Add `manifest.txt` files to allow the OMM templates to be used by the `fsl_get_standard` tool (https://fsl.fmrib.ox.ac.uk/fsl/docs/#/utilities/dataset_clitools?id=fsl_get_standard).

  The intent of `fsl_get_standard` is to allow users and scripts to dynamically select standard templates, instead of hard-coding paths to (e.g.) `$FSLDIR/data/standard/MNI152_T1_2mm.nii.gz`. Assuming that the OMM templates are installed into `$FSLDIR/data/omm/Oxford-MM-1/`, users/scripts can select the OMM templates by setting the `FSL_STANDARD` environment variable, e.g.:

  ```
  $ fsl_get_standard
  /usr/local/fsl/data/standard/MNI152_T1_2mm.nii.gz
  $ export FSL_STANDARD=omm/Oxford-MM-1
  $ fsl_get_standard
  /usr/local/fsl/data/omm/Oxford-MM-1/OMM-1_T1_head.nii.gz
  ```

  Or by using the `--standard` option, e.g.:

  ```
  $ fsl_get_standard --standard omm/Oxford-MM-1
  /usr/local/fsl/data/omm/Oxford-MM-1/OMM-1_T1_head.nii.gz
  ```
- [*Update transformation names for specificity*](https://git.fmrib.ox.ac.uk/fsl/oxford-mm-templates/-/merge_requests/18)
  Since there is a major breaking change in the naming convention (i.e., swapping the to/from meanings) I thought it would be better to just rename the files entirely.
  This will lead to scripts using these warps failing rather than mistakenly using the wrong transformations, which is preferable.

**Detailed history**

- `Add skeletonised FA for TBSS`
- `Correct naming convention of warps  NB! This is a breaking change! Essentially, the filenames have been swapped for MNI->OMM and OMM->MNI transformations to reflect *the direction in which they transform data*, rather than the direction in which the warp is moving points.`
- `MNT: Manifest files for use by fsl_get_standard`
- `RF: Use T2_FLAIR rather than T2`
- `Add QSM volume`
- `Update README`
- `Update transformation names for specificity  Since there is a major breaking change in the naming convention (i.e., swapping the to/from meanings) I thought it would be better to just rename the files entirely. This will lead to scripts using these warps failing rather than mistakenly using the wrong transformations, which is preferable.`

## <a id="fsl-fugue" href="#fsl-fugue">fsl-fugue 2201.4 -> 2201.5</a>

- [*Fix OSX Packing*](https://git.fmrib.ox.ac.uk/fsl/fugue/-/merge_requests/9)
  On OSX repeatedly switching between the Siemens/GEHC modes will break the layout/widget packing.

  This commit fixes by updating the label text instead.

**Detailed history**

- `BF: Fix packing issue on reported mode changes`
- `MNT: Removed obsolete OCMR/Varian mode`
- `MNT: Removed incomplete OCMR option`
- `MNT: Restored copyright`

## <a id="fsl-utils" href="#fsl-utils">fsl-utils 2203.4 -> 2203.5</a>

- [*MNT: Call `omp_set_num_threads` in addition to `openblas_set_num_threads`*](https://git.fmrib.ox.ac.uk/fsl/utils/-/merge_requests/16)
  BLAS threading is disabled by default in FSL tools, as it usually does not give any performance benefit. When using an OpenMP build of OpenBLAS, the `openblas_set_num_threads` function _used_ to call `omp_set_num_threads`. But this has changed at some point, and now one must explicitly call `omp_set_num_threads` in order to control the number of threads used by OpenBLAS.

**Detailed history**

- `MNT: Call omp_set_num_threads in addition to openblas_set_num_threads, as the latter no longer calls the former, and openblas calls may otherwise use multiple threads (op openblas-omp builds)`

## <a id="fsl-xtract" href="#fsl-xtract">fsl-xtract 2.0.5 -> 2.0.6</a>


**Detailed history**

- `Merge pull request #9 from SPMIC-UoN/xpy`
- `bug fix: was saving left scalar map prediction as left and right results`
- `Merge pull request #8 from SPMIC-UoN/xpy`
- `added KLD scipy version`
- `update normalisation`
- `Merge pull request #7 from SPMIC-UoN/xpy`
- `updated to include make_subcort_bp`
- `added normalisation in case of tract subsetting`
- `additional script to support subcortical blueprinting`
- `Updates to report to make it more user friendly`
- `Updated to use nomat2 for subcortex.`
- `Updated to use nomat2 for subcortex. Added intent code for CIFTI (3006)`
- `added hidden queue option`

## 6.0.6.1, 5th December 2022

### fsl-armawrap 0.6.0  	0.4.1     	-->	0.6.0

- TEST: make std namespace available to unit tests
- MNT: version
- DOC: changelog
- RF: Remove "using namespace std". Any issues should be fixed downstream
- MNT: version
- DOC: changelog
- TEST: add pyfeeds test
- DOC: changelog
- MNT: Clear arma_aligned flags to avoid struct padding issues
- CI: no longer test older than c++11
- TEST: new armadillo requires minimum c++11
- DOC: changelog, readme, armadillo license/notices
- MNT: Updated to armadillo 10.5.1
- MNT: add common test stage
- MNT: Tweak tests to enable out-of-source compliation/execution
- MNT: Don't assume specific compiler executable names
- TEST: Fix failed merge
- CI: Integrate common fsl/fsl-ci-rules into .gitlab-ci.yml pipelines (auto-generated fsl-ci-rules)
- MNT: Update install targets
- MNT: Version
- DOC: Update usage
- TEST: Adjust compile flags+include paths
- MNT: Re-arrange header files
- MNT: use GLOBALHFILES to install armadillo
- MNT: remove depend file
- RF: HFILES, not HDRFILES; other cosmetic tweaks
- DOC: readme updates
- MNT: FSL makefile for armawrap - depends on adjusted FSL makefile machinery in new base project
- TEST: Can't call a global "data" in c++17
- CI: test with/out optimisation
- TEST: same randomness when compiling direct against armadillo
- MNT: Explicitly mark destructor as noexcept(false) in c++>=11
- RF: Explicitly cast to int, rather than changing type, to avoid other complaints
- TEST: mistake in lcov alias
- TEST: set default compiler
- CI: Test different C++ standards
- TEST: Adjustments so that env-provided CXXFLAGS will be used by default, but can be overridden. Also use env-provided GCOV if specified
- MNT: version
- DOC: changelog
- BF: Fix bug in subview_assign - "-diag" was wrapping around because diag is an unsigned int. Only seems to occur when using -std newer than c++98
- TEST: fix usage of ARMAWRAP_INCLUDE_DIR
- DOC: changelog
- MNT: version
- RF: Adjust NEWMAT::SVD to explicitly fail if passed a matrix with less rows than columns (for compatibility with newmat behavuiour). Remove U truncation, as arma::svd_econ does it for us (whereas arma::svd does not)
- TEST: Test to make sure SVD works with non-square matrices, and fails for MxN matrices where M < N. Minor adjustments to test runner


### fsl-avwutils 2209.0 	2101.0    	-->	2209.0

- MNT: Removed the segment option from options object
- MNT: Removed segment option
- MNT: Moved NN interpolate to newimage library
- ENH: Euclidean Distance Transform
- Add dog edges
- ENH: Zero s/q forms after swap if unset
- BF: Force zero transforms for -deleteorient
- MNT: Clarify *thrP usage
- MNT: Don't assume namespaces included by external headers
- MNT: Clean up makefile
- MNT: include nemat -> include armawrap/newmat
- MNT: include cprob/libprob.h, rather than libprob.h directly
- RF: Skip file name, in case it contains e.g., "dim2"
- MNT: Handle paths with spaces in their names


### fsl-basisfield 2203.1	1910.0    	-->	2203.1
- TEST: update for new SplineField constructor (requires NoOfThreads instance)
- Added a vectorised Get(vec) for getting field and/or derivatives in one call
- Now uses Utilities::NoOfThreads for constructors
- TEST: remove unnecessary libs from ldflags
- TEST:macOS/M1 benchmarks
- TEST: parity between feedsRun and cpp names
- TEST: macOS-64 benchmarks
- MNT: Make sure string returned by <exception-class>::what is not a temporary
- TEST: linux benchmark files
- TEST: feedsRun script, allow RNG seed to be specified
- TEST: Jesper's splinefield tests. Need to rework a little bit
- Added functionality to set and check for number of threads
- Made sure that nthreads was passed from splinefield to any SpMat or BFMatrix that it returns
- BF: Missing initialiser from splinefield copy constructor. No effect in C++11, but was causing problems in C++17
- RF: Replace boost::shared_ptr with std::shared_pointer, disambiguate some function calls
- RF: Disambiguate symbols from MISCMATHS
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: Clean up makefile
- MNT: Fix newmat include path
- STY: Whitespace
- Fixed bug that meant parallel version wasn't actually used for Bending energy Hessian
- Parallelised a subset of functions in splinefield class
- BF: Fix accidental down-casting of unsigned int to int


### fsl-baycest 2111.0  	FSL6-0-1  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: clean up makefile


### fsl-bet2 2111.0     	2103.0    	-->	2111.0

- MNT: Don't assume any namespaces are included by external header files
- MNT: clean up makefile, shared lib renames


### fsl-bianca 2209.2   	2106.0    	-->	2209.2

- BF: Debug binary location left in
- MNT: Changes for new distancemap
- BF: use bash not sh
- BF: Rename cluster -> fsl-cluster


### fsl-bint 2111.0     	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: More namespacing
- MNT: Remove "using namespace" statements from header files
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: clean up make file, shared lib renames


### fsl-cluster 2201.0  	2004.0    	-->	2201.0

- MNT: rename cluster to fsl-cluster to avoid naming conflicts with third party packages. A wrapper script called "cluster" will still be installed, for legacy compatibility
- MNT: Don't assume any namespaces are included by external header files
- MNT: Clean up makefile, update shared lib names
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-cprob 2111.0    	2006.0    	-->	2111.0

- MNT: Don't declare extern INFINITY
- MNT: return values
- RF: We want to keep these definitions - they should only be undefined if compiling on super super old systems which don't support nan/inf
- RF: Alternative mtherr function which doesn't print anything
- MNT: true_gamma - alias for gamma, used by fabber_models_dsc
- DOC: readme
- MNT; Dis-ambiguate usage of gamma function
- RF: Use unique CPROB namespace, and isolate MISCMATHS namespace import hack to libprob.h
- RF: Compile as C++ lib
- RF: Refactor to compile as C++ library:  - Rename *.c to *.cc  - Rework legacy "void func(a) double a {..}" function signatures  - Put everything in MISCMATHS namespace in both obj and header files (will  change this shortly)  - Do not use extern - include <math.h>  and <cprob.h> instead  - Do not define INFINITY/NAN globals - use those from <math.h> instead
- MNT: macOS requires all shared lib dependencies to be specified when compiling a shared lib; don't specify -lfsl-cprob when compiling libfsl-cprob
- BF: typo in makefile
- MNT: Update makefile to compile shared, not static library



### fsl-data_standard 2208.0	2006.0    	-->	2208.0

- MNT: Fix some sform/qform codes. Fix lowercingulum affines and codes, and remove non-printable characters from aux_file field.



### fsl-eddy 2111.0     	2106.1    	-->	2111.0

- RF: Use new CUDACXXFLAGS for compiling .cpp files to be linked into eddy_cuda, so we can pass same compiler flags
- RF: Use g++ to compile .cpp code, and only use nvcc for .cu files and final linkage. This is because we may want to use different C++ standards for g++-compiled and nvcc-compiled code (c++17 for former, c++14 for latter).
- BF: Make sure build dirs are created
- RF: Rename eddy_openmp -> eddy_cpu
- RF: Rename "eddy_openmp" to "eddy_cpu", in anticipation of replacing openmp with std::thread. Add -rdynamic so that fsl/utils/stack_dump.h works. Add explicit build dependencies for CUDA object files to reduce compilation times during development.
- MNT: Not building eddy_cpu
- MNT: Don't build single threaddy
- MNT: Don't use EddyTry/EddyCatch macros on destructors, as they default to noexcept in C++>=11
- MNT: Namespace disambiguation in CUDA code
- MNT: Namespace disambiguation of vraious symbols. Use std::shared_ptr instead of boost::shared_ptr
- BF: cudabasisfield added to LIBS, so being included in non-CUDA builds
- BF: improper use of os.execv - need to pass command name as first argument
- RF: Link against cudaver-specific cudabasisfield solib
- MNT: Save nvcc object files to CUDA_VER-specific build directory
- MNT: don't delete wrapper script on make clean
- MNT: Port eddy wrapper script to python, for improved portability and cleaner code
- RF: Compile statically linked, and labelled eddy_cuda binaries - CUDA toolkit libraries are statically linked (depends on fsl/base 2107.0), and eddy_cuda binaries are named eddy_cudaX.Y
- MNT: new EddyCudaHelperFunctions.cu and CPUStackResampler.cpp files
- ENH: wrapper script to call eddy_cuda/eddy_openmp/eddy_cpu
- MNT: Fix cudabasisfield #include
- RF: Put cuda obj files in separate build dir, because there are name overlaps (the reason why original Makefile renamed cuda files to %_cuda.o)
- BF: openmp objs were being constructed incorrectly
- BF: Separate out CPU from CUDA objects - probably an artifact of shared lib experimentation
- BF: No longer building shared libs
- RF: I misunderstood the structure of eddy - cannot use separate shared libs, as there are circular dependencies between eddy.cpp and other files.
- RF: No longer naming binaries with cuda version. Compile eddy_cuda executable with nvcc
- MNT: note about compilation
- MNT: -fopenmp in final link
- MNT: fix eddy libs
- MNT: Clean up makefile
- MNT: Fix newmat/cprob include paths
- STY: whitespace


### fsl-fast4 2111.0    	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Remove "using namespace" statements from headers, don't assume namespaces included by external headers
- MNT: clean up makefile, shared lib renames


### fsl-fdt 2202.5      	2103.0    	-->	2202.5

- BF: fix argument parsing
- BF: Remove LD_LIBRARY_PATH as breaking script on cluster
- RF: Change Makefile so that gpu build *only* compiles/installs CUDA binaries. Everything else is done via cpu build
- BF: shared lib name has changed
- MNT: xfibres_gpu wrapper script which identifies the installed CUDA versino and calls an appropriate xfibres_gpuX.Y executable
- MNT: xfibres_gpu  and libfsl-bedpostx_cuda.so are labelled with the CUDA version they were compiled against
- MNT: Need to include thrust/[host|device]_vector
- MNT: Don't assume any namespaces are included by external header files
- MNT: Don't assume any namespaces are included by external header files. More to come
- MNT: Replace boost::shared_ptr with std::shared_ptr.
- MNT: -lcuda / -lcudart handled automatically
- RF: Use nvcc for compilation of xfibres_gpu, so that nvcc-specific flags and libs are applied
- MNT: Clean up build control - the cpu/gpu flags can be used to build the CPU and/or the GPU bits. Default behaviour is to build CPU bits only.
- RF: use "gpu" rather than "FDT_COMPILE_GPU"
- MNT: rename nvcc flags
- MNT: nvcc needs link options when compiling shared library
- MNT: clean up makefile; GPU compilation is now controlled via FDT_COMPILE_GPU variable
- MNT,STY: fix newmat/cuda include paths, whitespace
- STY: whitespace
- MNT: Include cprob/libprob.h rather than libprob.h


### fsl-feat5 2201.2    	2104.0    	-->	2201.2

- BF: Change copyright hook
- RF,BF: MAke sure JS callbacks are are accessible by other frames. Use iframe instead of frame. Suppress server log messages
- RF,BF: Copy js lib files into pnm output dir. Fix a few bugs in pnm_stage1. Keep web server running for a few secs to give browser enough time to load PNM report page
- RF: Re-write pnm_stage1 in python. Automatically start a local web server, and open PNM report in web browser from that server, to prevent CORS errors
- MNT: Defer javascript execution until page has loaded
- MNT: Use DATAFILES variable to install flobs/pnm data
- MNT: Add PNM html files (moved over from base project)
- RF: Feat is going to have a $FSLDIR/data/feat5/ directory - may as well store default_flobs.flobs files in there
- BF: Rename cluster -> fsl-cluster
- MNT: Don't assume any namespaces are included by external header files
- MNT: Dependencies need to be specified when compiling a shared lib on macos
- MNT: clean up makefile
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-film 2111.0     	2007.0    	-->	2111.0

- MNT: Don't assume any namespaces are included by external header files
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: Fix newmat include paths
- STY: whitespace
- MNT: fix newmat include paths
- STY: whitespace
- MNT: clean up makefile, shared lib renames
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-filmbabe 2111.0 	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume any namespaces are included by external header files
-  MNT: fix newmat include paths
- STY: Whitespace
- MNT: clean up makefile, shared lib renames
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-first 2203.0    	2006.0    	-->	2203.0

- RF: Change first3Dview to use fsleyes instead of fslview
- BF: Rename cluster -> fsl-cluster
- MNT: Don't assume any namespaces are included by external header files
- MNT: clean up makefile, shared lib renames


### fsl-first_lib 2111.0	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume any namespaces have been included
- MNT: Error in LIBS declaration
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: clean up makefile, compile shared not static
- MNT: fix newmat include path
- STY: whitespace


### fsl-flameo 2111.0   	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Remove "using namespace" statements from header files, don't assume namespaces included from external headers
- MNT: CiftiLib needs special handling
- MNT: fix newmat/cifti include paths
- STY: whitespace
- MNT: clean up makefile, shared lib renames
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-flirt 2111.0    	2108.0    	-->	2111.0

- MNT: Don't assume any namespaces included by external header files
- MNT: need meshclass
- MNT: fix newmat include path
- STY: Whitespace
- MNT: Clean up makefile, shared lib renames


### fsl-fnirt 2203.0    	FSL6-0-0  	-->	2203.0

- MNT: Remove unnecssary boost namespace - should have been removed in migration from boost::shared_ptr to std::shared_ptr
- MNT: Rename new_invwarp to invwarp (see related changes in fsl/fugue, removing the old invwarp)
- MNT: Replace boost::shared_ptr with std::shared_ptr. Don't assume any namespaces are included by external header files
- MNT: Need cprob
- MNT: fix newmat include paths
- STY: Whitespace
- MNT: Clean up make files, shared lib renames
- Makefile now uses c++11 to build. And changes brought about by that (explicit casts).
- BF: Address compiler errors about implicit cast from int to Real (a.k.a double)
- BF: typo
- BF: Avoid implicit downcast from int64_t to int


### fsl-fugue 2201.2    	2010.0    	-->	2201.2

- RF: Don't fail if sform/qform don't match. Use newimagevox2mm_mat instead, which will follow the convention of using sform if sform code !=0, or qform if qform code !=0, or fall back scaling affine
- DOC: Mention default value for interp in cli help
- MNT: Remove old invwarp - new_invwarp (in the fnirt project) has been in use for over 10 years now
- MNT: EXPOSE_TREACHEROUS needed for warpfns
- MNT: Replace boost::shared_ptr with std::shared_ptr. Don't assume any namespaces are included by external header files
- MNT: Need cprob
- MNT: Clean up makefile, shared library renames
- MNT: Fix newmat include paths
- STY: Whitespace


### fsl-giftiio 2111.0  	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: use shared lib
- MNT: clean up makefile
- Merge branch 'mnt/rmeove-have-zlib' into 'master'
- MNT: Remove all uses of HAVE_ZLIB


### fsl-gps 2111.0      	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Replace boost::shared_ptr with std::shared_ptr. Don't assume any namespaces are included by external header files
- MNT: clean up makefile
- MNT: fix newmat includes
- STY: Whitespace


### fsl-lesions 2111.0  	2104.0    	-->	2111.0

- MNT: Don't assume namespaces included by external header files
- MNT: clean up makefile


### fsl-libgdc 2111.0   	2006.0    	-->	2111.0

- BF: Seems like on macOS, other shared libraries need to be specified when compiling a shared lib?
- MNT: compile shared, not static


### fsl-libmeshutils 2111.0	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume any namespaces are included by external header files
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: clean up makefile, shared lib renames
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-libvis 2111.0   	1909.0    	-->	2111.0

- MNT: Fix executable rule (shared lib files were being passed)
- MNT: Don't assume that any namespaces are available
- MNT: Don't specify -l<lib> when compiling <lib>
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: poke libs, make sure shared libs are built before executables
- MNT: fix libvis (now local) include paths
- STY: whitespace
- RF: Move many utilities from miscvis into libvis, to resolve circular dependencies
- MNT: Compile shared, not static library
- MNT: Fix includes


### fsl-mcflirt 2111.0  	FSL6-0-1  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume namespcaes included by external headers
- MNT: cruft
- MNT: fix newmat include path
- STY: whitespace
- MNT: clean up makefile, shared lib rename


### fsl-melodic 2111.1  	2012.0    	-->	2111.1

- MNT: Use std::shuffle instead of std::random_shuffle, which has been removed in C++17
- MNT: Remove "using namespace" statements from headers, don't assume namespaces included in external headers
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: Actually use local version of zeropad
- STY; whitespace
- MNT: bwtf no longer exists
- MNT: Use a namespace to avoid conflicts with common names (e.g. "data")
- STY: Whitespace
- MNT: Use a namespace, as there are other things in the world called "data", "glm", etc etc.
- STY: whitespace
- MNT: Remove stupid dependency on fdt
- MNT: clean up makefile
- MNT: fix newmat/cifti include paths
- STY: whitespace
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-meshclass 2111.0	2102.0    	-->	2111.0

- MNT: Need LDFLAGS to compile shared libs on macOS
- MNT: More namespace disambiguation
- MNT: Remove "using namespace" statements from header files
- MNT: Clean up makefile


### fsl-misc_c 2111.0   	2106.0    	-->	2111.0

- MNT: Put ztop_function in a namespace. Remove "using namespace" statements from header files
- TEST: make feedsRun executable
- MNT: Clean up makefile
- MNT: Include cprob/libprob.h instead of libprob.h. Clean up some cruft in makefile


### fsl-misc_scripts 2111.0	2004.0    	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Text2Vest, Vest2Text, fsl_abspath are provided by fslpy
- MNT: clean up makefile


### fsl-misc_tcl 2206.0 	2010.0    	-->	2206.0

- MNT: Search for $FSLDIR/bin/fsleyes, not $FSLDIR/bin/fslview
- ENH: Allow global scaling of fsl_sub runtimes
- BF: Allow for fslversion not being present
- MNT: Added tcl symbols for internal projects
- MNT: Install a "master" pre-generated tclIndex, rather than using auto_mkindex whenever each TCL project is installed.
- MNT: migrate fslio.tcl from defunct fslio project
- BF: Typo
- MNT: Runtcl is now part of fsl-base. Makefile uses built-in rules
- MNT: minor cosmetic changes
- BF: Fix for very intermittent *can't read ::errorCode: no such variable bug*


### fsl-miscmaths 2203.2	2010.0    	-->	2203.2

- MNT: Use newmat rather than armadillo method
- BF: Replace problematic rotmat->quaternion routine with call-out to more robust, pre-existing niftio routine
- Added some std:: to make sure right numeric functions called.
- Changed constructors to take Utilities::NoOfThreads instead of unsigned int
- Changed nthr to NoOfThreads in splinterpolator constructor
- BF: Remove ambiguous Splinterpolator constructor overloads
- Added optional nthr to splinterpolator constructors. Will be used for deconvolution.
- Changed SplinterpolatorException class to avoid compiler warnings
- changes to exceptions that returned pointer to temporary object
- CPU parallelisation of many of the more time consuming operations
- MNT: ISO C++17 does not allow register storage class specifier
- MNT: Namespace in ridiculous macros
- MNT: Replace boost::shared_ptr with std::shared_ptr
- MNT: more namespacing
- STY: whitespace
- RF: Move "using namespace" statements to source files
- RF: Remove all "using namespace" statements from headers
- MNT: Missing dependency
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: NIFTI constants are now defined in the NiftiIO namespace
- MNT: remove -std=c++11 flag
- MNT: #include "newmat.h" -> #include "armawrap/newmat.h"
- MNT: Clean up, and compile shared not static


### fsl-miscvis 2201.0  	2007.0    	-->	2201.0

- BF: Rename cluster -> fsl-cluster
- RF: Migrated many low level utils from miscvis to libvis, to remove circular dependencies
- MNT: Clean up/update makefile to use new shared lib names


### fsl-mist 2111.3     	2010.0    	-->	2111.3

- MNT: get_header now raises a ExpiredDeprecationError in latest nibabel
- MNT: get_affine now raises a ExpiredDeprecationError in latest nibabel
- MNT: Build against vtk 9.1
- MNT: Don't assume any namespaces are included by external header files
- MNT: Clean up makefile
- BF: Wrong type for call to GetCellPoints
- MNT: fix include paths
- STY: whitespace
- MNT: Clean up makefile


### fsl-mm 2111.0       	1909.0    	-->	2111.0

- MNT: Remove "using namespace" statements from headers, don't assume any namespaces are included by external header files
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: fix shared libs
- MNT: fix newmat include paths
- STY: whitespace
- MNT: clean up makefile, shared lib renames
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-newimage 2203.6 	2104.0    	-->	2203.6

- TEST: Re-arrange test code, and add unit tests for read/save_complexvolume
- BF: Accidentally removed l/r flips in read_complexvolume
- RF: remove isComplex check in for-loop conditionals
- BF: Actually copy orientation  info from complex input file to real/imag outputs,  instead of just doing L/R flip
- RF: avoid pointless loops depending on the value of isComplex
- MNT: Restored legacy distancemap symbols for backwards compatibility
- ENH: Only smooth if sigma > 0
- ENH: Euclidean distance transform
- add mode for log_edge_detect
- Add dog edges
- ENH: env control of extension loading
- TEST: test that reinitialize works when given a pointer to data
- BF: data pointer not being passed through to initialize
- MNT: Use is_integral<T>::value, rather than is_integral_t<T>, for compatibility with C++14
- ENH: Threshold/binarise API
- Made changes related to moving NoOfThreads from NEWIMAGE to Utilities
- BF: Incorrectly changed to backgroundval during 6.x dev
- volume now creates splinterpolator with the same nthreads as itself.
- Removed static declaration that messed with multi-threading
- Fixed bug that meant that read_volume_hdr_only did not work with nThreads
- Added a mutex to protect recalculation of splines, and added an nThreads member variable
- TEST: Use correct rpath syntax on macOS. Doing this rather than adding these flags to USRLDFLAGS to ensure that $ORIGIN/.. comes first
- TEST: Allow testing against in-source build, or against installed newimage
- RF: std::pow is templated differently on macOS? Easier to cast
- MNT: More disambiguation
- MNT: disambiguate MISCMATHS::Max usage
- MNT: qualify MISCMATHS function calls
- MNT: qualify nifti constants
- MNT: list libfsl-cprob when compiling libfsl-newimage.so
- TEST: missing shared lib
- CI: channel variable name changed
- MNT: More namespacing
- RF: Remove "using namespace" statements from headers
- CI: change test invocation
- CI: update project pipeline
- TEST: feedsRun script, to execute tests via pyfeeds
- MNT: change include path in skeletonapp
- MNT: Clean up makefile - generate buildstring with git, remove test stuff
- TEST: Move test program to separate dir with its own makefile
- fix typo and ordering
- use standard fsl linux image
- update test case names
- update var name
- update libs for linux
- update conda command
- update conda command
- update conda script
- update docker image due to centos6 glibc
- update LD_LIBRARY_PATH to export
- update volume constructor unit tests
- set LD_LIBRARY_PATH for test
- update lib call
- update lib
- move BUILDSTRING
- add BUILDSTRING back in
- add FSL INIT
- add newimage unit test example implementation and ci
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: Compile shared, not static
- MNT: #include "newmat" -> #include "armawrap/newmat"
- Removed debug code
- MNT: Change in parameter name
- BF: Invalidate splines on a change in spline order
- MNT: renamed final instances
- MNT: renamed variable
- BF: Spline validity
- Added method  save_volume_and_splines  to write a volume along with its spline coefficients


### fsl-newmesh 2111.0  	1904.0    	-->	2111.0

- RF,MNT: Replace boost::shared_ptr with std::shared_ptr. Do not assume any namespaces have been included
- MNT: Missing library needed for linking on macOS
- MNT: error in LDFLAGS
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: nifti constants are now in NiftiIO namespace
- STY: whitespace
- MNT: fix newmat include paths
- STY: whitespace
- MNT: clean up makefile, compile shared not static


### fsl-newran 2111.0   	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Add link flags when compiling so file
- Merge branch 'mnt/conda' into 'master'
- MNT: Add link flags when compiling so file
- MNT: Need include statement
- RF: Remove "using namespace" statements from header files
- MNT: rename to libfsl-newran.so
- MNT: Compile shared library, not static library


### fsl-possum 2111.2   	FSL6-0-0  	-->	2111.2

- Merge branch 'mnt/register' into 'master'
- MNT: Remove uses of register keyword (disallowed in c++17)
- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume any namespaces are included by external header files
- BF: Can't use numeric operators on booleans
- STY: whitespace
- MNT: fix newmat includes
- STY: whitespace
- MNT: Clean up makefile
- MNT: include cprob/libprob.h rather than libprob.h
- Merge branch 'paulmc-master-patch-68773' into 'master'
- MNT: Don't manipulate [DY]LD_LIBRARY_PATH


### fsl-ptx2 2111.4     	2101.1    	-->	2111.4

- BF: Fix creation of seed->target affine  in probtrackx2_gpu  --omatrix2. Was previously assuming that target was defined in diffusion, not seed, space.
- RF: Size checks when stripping suffixes, bail if filestream is not good
- BF: Re-write/clean up code which generates names for target masks. Previous logic was flawed, and could result in tmpname[-1] = '_';
- RF: Change Makefile so that gpu=1 build only compiles/installs CUDA binary
- MNT: wrapper script to find+execute a suitable version of probtrackx2_gpu
- MNT: label probtrackx2_gpu binary with the CUDA version it was compiled against
- MNT: need EXPOSE_TREACHEROUS
- MNT: Don't assume any namespaces are included by external header files
- MNT: Compile probtrackx_gpu.o with nvcc
- MNT: cudadevrt handled automatically
- RF: Remove profiling calls (and hence remove dependence on libnvToolsExt)
- MNT: whitespace
- RF: Remove cudart/cuda (they are added autimatically), and intending to remove reliance on nvToolsExt
- RF: Allow conditional compilation of either CPU bits, or GPU bits, or both
- RF: Use "gpu" rather than "PTX2_COMPILE_GPU"
- MNT: rename nvcc flags
- MNT: Clean up makefile
- MNT: Disambiguate round function
- STY: whitespace
- use long to avoid overflow in num_threads


### fsl-qboot 2111.0    	2010.0    	-->	2111.0

- MNT: Don't assume any namespaces are included by external header files
- MNT: clean up makefile


### fsl-randomise 2203.2	2101.0    	-->	2203.2

- BF: short is too short to support NIFTI-2 lengths
- BF: combineScript uses bash-isms
- BF: inclusive to match older range
- ENH: Small p-values are no longer truncated, plus masking on thresholded output
- Plausable fix for underflow and masking problems
- MNT: URBG for C++17
- MNT: URBG for C++17
- MNT: Don't assume any namespaces are included by external header files
- MNT: clean up makefile, shared lib renames
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-relax 2111.0    	2007.0    	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- BF: Wrong include path
- MNT: clean up makefile
- MNT: Fix includes
- STY: whitespace


### fsl-siena 2111.0    	2006.0    	-->	2111.0

- MNT: Don't assume any namespaces are included by external header files
- MNT: clean up makefile
- STY: whitespace


### fsl-slicetimer 2111.0	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume any namespaces included by external headers
- MNT: fix newmat include paths
- STY: whitespace
- MNT: clean up makefile


### fsl-susan 2111.0    	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume any namespaces are included by external headers
- MNT: susan needs cprob
- MNT: Clean up makefile, shared lib renames


### fsl-swe v1.1.0      	v1.0.3    	-->	v1.1.0

- Merge branch 'mnt/conda' into 'master'
- RF: Don't assume any namespaces have been included by external headers
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS, don't specify -lfsl-swe when compiling libfsl-swe.so
- MNT: clean up makefile
- MNT: include cprob/libprob.h rather than libprob.h


### fsl-tbss 2111.0     	2101.0    	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: Don't assume any namespaces are included by external header files
- MNT: Clean up makefile, shared lib renames


### fsl-topup 2203.1    	1911.0    	-->	2203.1

- RF: Moved blas threading control to utils
- Added call to Utilities::fsl_init_blas_threading() to disable BLAS multi-threading
- Various refactorings aimed at speed ups
- Added _nthr to TopupScan and TopupScanManager
- Added _nthr to TopupCF and to construction of splinefield
- Added --nthr command line argument
- Added a function level try-catch on main
- Changed exception classes to avoid compiler warnings
- MNT: Replace boost::shared_ptr with std::shared_ptr. Don't assume any namespaces are included by external header files
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: Compile a shared library for topup, so eddy don't have to recomple topup files
- BF: typo
- MNT: clean up makefile
- MNT: fix newmat include paths
- STY: whitespace
- Changed default datatype of output from same as input to float. If one wants to preserve input type one must now use --datatype=preserve
- Added -std=c++11 to USRCXXFLAGS


### fsl-utils 2203.2    	2007.0    	-->	2203.2

- MNT: use local threading.h
- RF: superfluous  `__attribute__`
- DOC: Spelling mistake
- RF: Removed debug text
- RF: Rename to indicate general scope
- RF: Global object for setting threading parameters
- ENH: Prototype global options code
- RF: Need to load libblas.dylib on macOS
- MNT: Check for invalid key in EndAccumulate, small tweaks to threading_init
- ENH,BF: Remove call to no-longer-existing CreateAccumulate function. New fsl_init_blas_threading function, which can be called to disable BLAS threading in an implementation-independent way.
- STY: use of reference operator
- STY: whitespace
- ENH: Accumulate methods, for timimg repeat execution of code blocks
- ENH: Singleton/default FSLProfiler, handy if you want to direct all profiling outputs to a single file.
- Minor formatting
- Fixed X_OptionError to avoid compiler warning
- First commit of a file where we can collect utilities related to threading
- Fixed FSLProfilerException so it doesn't cause compiler warning
- MNT: Update stack_dump.h to work on apple M1
- STY: whitespace
- MNT: More namespacing
- RF: Disambiguate Tracer
- RF: Remove "using namespace" statements from header files
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- BF: Dodgy indentation, and not enough braces
- STY: Whitespace
- RF: Conformance with C++17
- MNT: use CXX, not CXX11
- MNT: Update makefile to new make system, and to build shared library
- MNT: Fix newmat includes


### fsl-warpfns 2203.0  	2106.0    	-->	2203.0

- Added a deffield2jac function for when displacements are 1D
- Changed nthr to Utilities::NoOfThreads everywhere
- BF: Make nthr optional in main raw_general_transform function
- `raw_general_transform`, and all interfaces to it, now takes nthr as an optional parameter. It has been extensively tested
- Added path to build test_parallel_warpfns
- There is now a parallel, but untested, version of raw_general_transform
- Fixed exception classes to not return address of temporary objet
- Starting to rewrite raw_general_transform for parallelisation
- MNT: Disambiguate Max function
- RF,MNT: Replace usage of boost::shared_ptr with std::shared_ptr. Don't assume any namespaces are included by external header files.
- MNT: Wrong shared lib name
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: fix newmat include path
- STY: whitespace
- MNT: Clean up makefile


### fsl-xtract_data 1.3.4	1.3.2     	-->	1.3.4

- Updated AF protocols to include ordered targets.
- Update do_reg_F99.sh
- Updated default xtract_data path


### fsl-znzlib 2111.0   	FSL6-0-0  	-->	2111.0

- Merge branch 'mnt/conda' into 'master'
- MNT: don't add -lfsl-znz to LDFLAGS for its own compilation, only for testprog
- MNT: Shared lib dependencies must be specified when compiling a shared lib on macOS
- MNT: Clean up makefile, compile shared, not static library
- Merge branch 'mnt/remove-have-zlib' into 'master'
- MNT: Remove HAVE_ZLIB and HAVE_FDOPEN switch. The latter is not used by anything, and the former is never unset.

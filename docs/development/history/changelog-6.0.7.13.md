# FSL 6.0.7.13, 01st August 2024


## file-tree 1.3.0 -> 1.4.0


## fsleyes 1.12.3 -> 1.12.4


### Detailed history

- Remove out-dated comment about installing old wxpython. [skip ci]
- MNT: As of numpy 2.x, np.array(copy=False) now raises an error if a copy cannot be avoided
- CI: Add a hook to run commands prior to testing - will allow me to update numpy in the CI env
- TEST: Shouldn't be testing for exact equality
- MNT: Apply arparse changes to python >=3.11.9 as they were backported to 3.11

## fslpy 3.19.0 -> 3.21.0

- MNT: Compatibility with `numpy` 2.0
- ENH: Ability to access F-test results through `fsl.data.featimage`
- MNT: Work-around `nibabel` auto-rescaling logic so that data/slope/intercept in `imcp`/`immv`'d images are not modified

### Detailed history

- MNT: np.array(a, copy=False) raises an error in numpy 2 if the array cannot be copied
- TEST: numpy 2 compat
- RF: fslversion was not being initialised until FSLDIR was set
- TEST: Some kind of regression in pytest 8.2 w.r.t. namespace packages
- ENH: New featanalysis.loadFTests and getZFStatFile functions, for getting F-test results. New loadFEATDesignFile for loading design.mat/con/fts files. String formatting fixes
- TEST: Unit tests for loadFTests and getZFStatFile functions
- MNT: String formatting
- ENH: accessors for zfstats
- ENH: Modify loadClusterResults to allow loading f-test results, accessor for f-test cluster mask file
- ENH,BF: add accessors to FEATImage class, fix zfstats accessor
- TEST: test loading f-test cluster results and zfstat/cluster_mask_zfstat files
- ENH: FEATImage.ftests accessor
- TEST: Test FEATImage f-test accessors
- MNT: Pre-release version bump
- BF: Missed one attribute on Cluster objects
- DOC: Update date in changelog
- MNT: Work-around nibabel auto-rescaling logic so that data/slope/intercept in imcp'd images is not modified
- TEST: Test ensuring that imcp does not modify data
- MNT: Pre-release dev version bump

## fsl-base 2404.0 -> 2407.0

- RF: Relax create/remove wrapper constraints to allow creation of wrappers for executables contained in child environments within `$FSLDIR`
- MNT: update code comment regarding creation of FSL wrapper scripts
- ENH: Allow additional ("extra") FSL components to be installed into an existing FSL installation with the `update_fsl_release` command
- *MNT: Adjust createFSLWrapper so that it does not re-create wrapper scripts that already exist*

  The `createFSLWrapper` script has been updated so that existing wrapper scripts are not overwritten. This is important in situations where a child conda environment is installed within `$FSLDIR` with packages that are also installed in the main FSL environment.

- BF: Bugfix and a few tweaks in `update_fsl_release` for installing extra environments

### Detailed history

- RF: Relax create/remove wrapper constraints to allow creation of wrappers for executables contained in child environments within $FSLDIR
- TEST: Test creating wrappers for exes in child env
- MNT: update comment
- ENH: Allow additional ("extra") FSL components to be installed into an existing FSL installation with the update_fsl_release command
- ENH: Update fslversion to support child environments
- MNT: Don't skip extra components that already exist - attempt to update them instead
- TEST: Test updates
- MNT: Guard against having no revisions (occurs in test env)
- TEST: Make sure FSL env file has conformant name. Need to list python in environment file
- MNT: comments
- MNT: Adjust createFSLWrapper so that it does not re-create wrapper scripts that already exist
- TEST: adjust wrapper script unit tests, new test to make sure existing wrappers are not overwritten
- BF: Bug in handling extra envs
- MNT: Accept cuda argument, and a few others

## fsl-feat5 2201.6 -> 2201.7

- *MNT: Adjust/simplify FILM runtime estimation*

  The old estimation seems to massively over-estimate run time for a 'typical' modern analysis, e.g. estimating ~10.5 days for a data set of dimensions (108, 108, 72, 750) and 37 EVs.


### Detailed history

- MNT: Adjust/simplify FILM runtime estimation

## fsl-installer 3.9.0 -> 3.14.0

- *Select appropriate versions of CUDA packages*

  To support projects which are based on Pytorch (and similar), we need to install the conda-forge version of the [CUDA Toolkit](https://anaconda.org/conda-forge/cuda-vesrion) and other CUDA libraries such as [cuDNN](https://anaconda.org/conda-forge/cudnn)<sup>*</sup>.

  Therefore, when FSL is installed, we need to ensure that appropriate versions of CUDA libraries are installed. To this end, this MR:
   - Adds capability to interrogate the installed CUDA version
   - Adds a command-line flag allowing the user to explicitly specify a CUDA version to install, or to disable installation of CUDA libraries.

  In principle all that is needed to accomplish this is to add `cuda-version=X.Y` to the list of package constraints. The `cuda-version` package is a meta-package which does not cause anything to be installed by itself, but will ensure that any CUDA libraries that are to be installed will be compatible with the specified version.

  https://github.com/conda-forge/conda-forge.github.io/issues/1963

  <sup>*</sup>The only alternative to depending on conda-forge libraries would be to build and distribute our own version of Pytorch (and other DL packages), which is a possibility, but would involve more work.

- *MNT: Prevent conda from updating itself during installation*

  Conda updates itself in-place whenever a new version is available, which can sometimes cause the installation to crash, e.g.:

   - https://github.com/conda/conda/issues/13560
   - https://github.com/conda/conda/issues/13920

  We can use the `auto_update_conda` configuration option to tell conda not to update itself unless explicitly requested.

- *MNT: Use a suitable miniconda installer based on the version of Python that is to be installed*

  See fsl/conda/manifest!258 and fsl/conda/manifest-rules!114

- *ENH: Add ability to install software into separate child environments*

   - The `fslinstaller.py` script is now capable of installing software into separate child environments, within `$FSLDIR/envs/`, to allow for better isolation of different software stacks. Future FSL releases will take advantage of this functionality to install optional/extra FSL components - these can be selected by the user via the new `--extra` option.
   - New `--channel` option, which can be used to install FSL packages from a local or alternative conda channel.

- *ENH: Retry installation on network errors*

  The `fslinstaller.py` script will now try to detect network errors, and will retry the main installation step. The number of retries can be changed with the `--num_retries` option. Conda versions >=23.11.0 will save interrupted package downloads - the downloads can be resumed simply by re-running the conda command that failed.

  This MR also has a new `--throttle_downloads` option, which limits number of simultaneous package downloads. This will hopefully make installations more robust when installing over unreliable network connections.

- *MNT: If extra child environment already exists, run `conda env update` instead of `conda env create`.*

  This allows existing child environments to be updated, a feature which will be used by the `update_fsl_release` command in the fsl/base> project.

- Retry installation on another type of download error
- MNT: Retry installation on another error type
- *MNT: Only add CUDA pin to environment specification if there is a `"cuda_enabled"` flag in the manifest entry for the FSL release*

  This is because adding `cuda-version` to the environment specification may affect installation of other packages.

- *RF: Only install CUDA packages in main or extra environments as specified in the manifest*

  Previously, if CUDA was detected/requested, the `fslinstaller` would add a CUDA package constraint to all environments (both the main FSL environment and any extra environments). Now it only adds the CUDA package constraint to specific environments as determined by the presence of the `cuda_enabled` flag in the FSL release manifest file.

- MNT: Assume all values read from `manifest.json` are strings
- *RF: Use CUDA-specific installation progress metrics*

  When installing a CUDA-capable FSL / extra environment, the installed set of packages will be different, and so requires different progress tracking parameters.

- BF: Bugfix in development release file name parsing
- DOC: Changelog
- RF: Move `add_cuda_packages` call, so that `update_fsl_release` has fewer things to do

### Detailed history

- MNT: Use auto_update_conda to prevent conda breaking due to in-place update
- CI: Remove py33 testing - I strongly doubt anybody is using py33 (or 34,35,..)
- MNT: Read information about python-version-specific miniconda installer from manifest.
- MNT: Error if we can't get the python version from the env.yml file
- TEST: Update tests for new manifest format (miniconda installer for each pyver)
- MNT: Add --throttle_downloads option, which limits number of simultaneous package downloads, for installation over unreliable network connections
- ENH: Installer now attempts to detect network-based failures during main installation stage, and retries the installation up to --num_retries times (default 3)
- TEST: Test retry_on_error, and LogRecordingHandler
- ENH: New --channel option which allows a conda channel to be prepended - this would allow e.g. downloading from a local package cache. Just CLI option in this commit - logic not yet implemented
- RF,ENH: Move environment file parsing into separate read/write functions. Prepend custom channels specified via --channel option
- ENH: New --extra CLI option, for installing additional FSL modules as child environments. Logic not implemented yet
- ENH: Installer now downloads additional environment files specified in "extras" field of manifest entry
- ENH: Allow a prefix string to be shown before progress bar
- RF: Store extra environment files separately
- ENH: Extra FSL environments now installed into $FSLDIR/envs/. A step number is shown before the progress bar for each mains stage of the installation
- DOC: Add some extra info to readme
- TEST: unit tests for read/write_environment_file. Fix typos in mock env.yml file. Installing as a child env requires --extras_dir to be specified
- MNT: py27 compatibillty - preserve dict order
- MNT: Sort env vars when printing on error
- RF: Only install extra environments on user request
- TEST: test installing extra environments
- RF: No need to pass envfile
- RF,BF: Use "component" rather than "module", to avoid nomenclature overlap with fsl_add_module, which is just a glorified tarball downloader. Typo in last commit
- CI: Notice in update-manifest CI job MR description
- BF: wrong var name
- DOC: Readme
- MNT: A couple of small comment/wording tweaks
- MNT: If extra child env already exists, run conda env update  instead of conda env create.
- MNT: Retry on another type of error
- ENH: Function to identify CUDA version supported by locally installed GPU
- ENH: New --cuda option for requesting a specific CUDA version, and --gpu=N option for requesting a CUDA version compatible with installed GPU #N. Just CLI options in this commit
- ENH: Add cuda-version constraint to package list when a GPU is detected, or the user specifies --cuda=X.Y
- MNT: Set CONDA_OVERRIDE_CUDA so that conda will/will not install CUDA packages
- RF: Just call nvidia-smi to get CUDA version, instead of using driver API to get compute capability. It also turns out that you can get the compute capability via a more specific call to nvidia-smi. Remove --gpu option - users with multuple GPUs can just use --cuda if needed. Will leave old version in commit history in case it comes in useful in the future
- ENH: Add reset() function to funccache decorator, for testing
- TEST: Tests for funccache, identify_cuda, and add_cuda_packages
- RF: Pass 'none' through to add_cuda_packages to disable CUDA
- DOC: Readme
- TEST: Failing stubs for --cuda usage
- TEST: filled out CUDA tests, some minor efforts to consolidate code duplicated across the test modules
- TEST: Fix construction of python version string
- TEST: reset identify_cuda cache after every test
- BF: Don't lock down pkgs_dirs, as this makes it impossible to use $FSLDIR/bin/conda to create environments when $FSLDIR is not owned by the user
- DOC: Changelog  [skip ci]
- MNT: Another error to retry installation
- MNT: Only add CUDA pin to environment specification if there is a "cuda_enabled" flag in the manifest entry for the FSL release
- TEST: Add cuda_enabled test
- RF: Look for 'cuda_enabled' flag in both the main environment build/specification, and in extra builds/specs, and apply on a per-environment basis.
- TEST: Make sure that cuda-version is only added to main/extra envs when the "cuda_enabled" flag is present in the manifest for that env
- TEST: Remove use of f-strings for compatibility with old Python versions. Adjust child env test - mock conda command copies env files into base (miniconda) dir, not destination (FSL) dir
- MNT: assume that all values in manifest are strings
- TEST: Test new str2bool helper function
- RF: Use CUDA-specific installation progress metrics. Log progress bars, so log file contains progress tracking
- BF: Make sure cuda_enabled flag is present for extra environmentstoo
- BF: bugfix in devrelease file name parsing - lstrip/rstrip remove all provided characters, they do not remove sub-strings
- RF: Move add_cuda_packages call, so that update_fsl_release has fewer things to do
- BF: typos
- MNT: Bump version - was incorrect in previous tag

## fsl-misc_tcl 2303.0 -> 2406.0

- *BF: runtime passed to `fsl_sub` must be an integer*

  Calling code should be passing an integer, but the value may be converted to a floating point when the `FSL_QUEUE_TIME_SCALE` variable is set.


### Detailed history

- BF: runtime passed to fsl_sub must be an integer

## fsl-mist 2111.5 -> 2111.6

- MNT: Bump vtk to 9.3

### Detailed history

- MNT: Bump vtk to 9.3

## fsl_mrs 2.1.20 -> 2.2.0

- BF: Deployment validation tagging
- MNT: Python 3.11, numpy 2.0

### Detailed history

- BF: Deployment validation tagging
- MNT: Python 3.11, numpy 2.0

## fsl-pipe 1.0.2 -> 1.0.3


## fsl-pyfeeds-tests 2401.1 -> 2407.0

- RF: Print `mmorf` executable. Save `fsl_sub` log files to test output directory
- TEST: Add basic tests for truenet

### Detailed history

- RF: Print mmorf executable. Save fsl_sub log files to test output directory.
- TEST: Add basic tests for truenet

## fsl-pyfix 0.8.0 -> 0.8.1

- *BF: Fix default highpass (`-h`) behaviour*

  Fixed the default behaviour of the `-h` option - when omitted, the default behaviour should be to read the highpass threshold from `design.fsf` (if present). But highpass-filtering was being skipped by default.


### Detailed history

- BF: Fix default highpass behaviour - default behaviour (-h omitted) should be that the highpass value is taken from design.fsf. But it was defaulting to -1 (no highpass). Also, -h -1 was not being detected, and was causing fslmaths to be passed a negative sigma, which would caus the filter to be skipped (which is intended, but if -h -1, then there's no need to call fslmaths at all).
- MNT: warning when -h is used superfluously

## fsl-sub-plugin-slurm 1.6.3 -> 1.6.4

- *Pyproject toml*

  Update build infrastructure to use new setup tools
  Add linting CI job and make code more PEP8 compliant


### Detailed history

- Pyproject toml
- When only using GRES to select GPUs it is not possible to request all GPUs of this type an better. Add warnings to reflect this - user can use --extra to define additional constraints to allow grouping of GPUs Refactor tests for new GPU functionality
- Add documentation about the warning generated when include_more_capable is true and class_constraint false

## fsl-topup 2203.4 -> 2203.5

- *Fixed bug that made sanity check on acqparams.txt reject synb0-DISCO use*

  There is a sanity check on readout times in the --datain/--acqp files. I recently coordinated that with eddy's sanity check, to ensure they have the same view of what is sane. But that meant that registering to a distortion free image no longer worked. And that is a valid thing to do, for example in the context of synb0-DISCO.
  I have now fixed it so that it allows one volume to have a readout time of zero, or almost zero.


### Detailed history

- Fixed bug that made sanity check on acqparams.txt reject synb0-DISCO use

## spec2nii 0.7.* -> 0.8.*


### Detailed history

- GE ISTHMUS/Hyper Sequence (#137)
- MNT: Drop python 3.8 support, add 3.12, deal with numpy 2.0 (#139)
- ENH/svs_slaser_dkd (#136)
- ENH: twix mixed headers (#135)
- Add support for .txt files exported by older versions of jMRUI (#133)
- Enhance support for GE data (#132)
- Add handling and tests for philips mega sequence. (#131)
- BF: Correct Universal editing sequence HERMES conditions and tests for XA50 twix and RDA (#130)
- dMRS and MEGA-sLASER fixes for PV360.1.1 (#124)
- ENH: Validated rda orientation and CSI handling (#128)
- Fix dicom naming bug with mixed types (#122)
- Imporvements in philips tag handling. (#121)
- Enh: Automatically add spectralwidth to header extension (#120)
- Add a duecredit citation for whenever spec2nii is used (#119)
- Update CHANGELOG.md (#118)
- add support for GE P-file rev 29.1 & 30.0 (#117)
- BF: RDA decoding issue (#115)
- Add handling and tests for CSI data stored in Simens enhanced DICOM (#114)
- Enh: Anonymisation at runtime (#113)
- in [x, y, z] → in (x, y, z) (#105)
- Avoid redefining Python builtins (#98)
- Bump actions/checkout from 3 to 4 (#95)
- Fix typos found by codespell (#93)

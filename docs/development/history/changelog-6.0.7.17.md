# FSL 6.0.7.17, 24th February 2025

- [**file-tree** 1.4.1 -> 1.6.0](#file-tree)
- [**fsleyes** 1.12.6 -> 1.13.0](#fsleyes)
- [**fsl-base** 2411.0 -> 2502.1](#fsl-base)
- [**fsl-installer** 3.15.1 -> 3.16.5](#fsl-installer)
- [**fsl_mrs** 2.3.1 -> 2.4.0](#fsl_mrs)
- [**fsl-pipe** 1.1.1 -> 1.2.2](#fsl-pipe)
- [**fsl-pyfeeds** 0.12.4 -> 0.13.1](#fsl-pyfeeds)
- [**fsl-pyfeeds-tests** 2407.0 -> 2501.0](#fsl-pyfeeds-tests)

## <a id="file-tree" href="#file-tree">file-tree 1.4.1 -> 1.6.0</a>

- [*Resolve "Support wildcards in file-tree `get` and `get_mult`"*](https://git.fmrib.ox.ac.uk/fsl/file-tree/-/merge_requests/50)
  Closes #11
- [*Add wildcards to CHANGELOG*](https://git.fmrib.ox.ac.uk/fsl/file-tree/-/merge_requests/51)
- [*Bump to version 1.6.0*](https://git.fmrib.ox.ac.uk/fsl/file-tree/-/merge_requests/52)

**Detailed history**

- `Add glob setting in file-tree`
- `Raise error for invalid glob value`
- `Implement pattern matching using * or ?`
- `Enable pattern matching in get_mult`
- `Support * and ? pattern matching in update_glob`
- `Fix forbidden characters for pattern matching`
- `Add tests for get_mult with filter`
- `Fix filtering of get_mult with pattern matching`
- `Use pattern matching in file-tree app`
- `Fix reraising of error`
- `Addglob keyword overriding to get and get_mult`
- `Add correct version number to __version__ and documentation`
- `Switch to default theme to get sticky sidebar`
- `Explain wildcards support in docs`
- `Mention option of using placeholders instead of wildcards`
- `Add wildcards to CHANGELOG`
- `Bump to version 1.6.0`

## <a id="fsleyes" href="#fsleyes">fsleyes 1.12.6 -> 1.13.0</a>

- [*RF: Reset display space to a sensible value when old reference image is removed*](https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes/-/merge_requests/460)
  This MR changes FSLeyes so that, when the display space is set to a reference image, and that reference image is removed, the display space is changed to another loaded NIfTI image (the first in the overlay list) instead of being reset to `"world"`.

  This change fixes an issue whereby the ortho panel zoom level would increase each time a new image was loaded. The cause of this is quite convoluted, but it is most easily reproduced when (e.g.) viewing T1 images in the file tree panel.

  When the user selects the first image in the file tree list, the image is loaded, and is set as the display space reference image. When the user selects the next image in the file tree list, the images are swapped in stages:
  1. The new image is loaded. If the images are slightly rotated with respect to each other, the bounding box surrounding all overlays will increase. The ortho panel has some logic which attempts to preserve the current zoom/pan settings, so internally it will increase the zoom level, although the effective (i.e. visible) zoom will not change.
  2. The old image is removed. This would cause the display space to be automatically reset to `"world"`, which will potentially substantially change the global bounding box, and cause the ortho panel to adjust its zoom settings again.
  3. The display space is manually set to the new image, which again changes the bounding box and triggers the ortho panel zoom preservation logic.

  The unnecessary swapping back and forth of the display space setting was causing the ortho panel zoom preservation logic to be disrupted, which would result in it incorrectly increasing the effective zoom level.

  The changes in this MR simplify the above process and avoid the zoom increase issue.
   - The file tree manager now swaps the old and new images in a single step
   - The display space is now automatically changed to the next NIfTI image in the overlay list, if the current reference image is removed
   - If the user manually changes the display space to e.g. `"world"`, that choice will be saved, and will be used as the new default.
- [*ENH: New line vector options*](https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes/-/merge_requests/461)
  New options for line vector overlays, including:
   - Ability to modulate the vector length by the modulate image, instead of modulating brightness/transparency.
   - Ability to modulate the vector RGB values to uniform brightness. This was being implicitly performed when the _Scale vectors to unit length_ option was enabled, but is now an independent option.

**Detailed history**

- `RF: Re-jig mesh reference image search routine so that it doesn't emit harmless warnings`
- `TEST: Reset display space to world in mesh test`
- `ENH: Scaffolding for new LineVectorOpts modulate-lineLength feature`
- `BF: OverlayDisplayWidgets specs were being merged in wrong order (with base-class specs taking precedence over sub-class specs)`
- `ENH: Scaffolding for new VectorOpts.normaliseColour property - this is equivalent to the existing RGBVectorOpts.unitLength property, but will be able to be applied to other (e.g. line) vector types independently`
- `BF: Prevent data processing task from running if the texture gets destroyed after it has been enqueued. This is a harmless error, but it seems to happen occasionally`
- `RF: Change RGB vector colour normalisation so that it is done in fragment shader instead of on the CPU`
- `RF,ENH: GLLineVector: don't pre-normalise vector image data; instead, do it in the shader. This allows the vector to be normalised independently from the colour. GL21 only.`
- `BF: GL14 line vector - orientFlip was only being applied when unitLength active`
- `BF: GL14 line vector shader parameter updates were not taking effect because the program wasn't being loaded. Good indication that nobody is running GL14 FSLeyes. Use voxValXform instead of clipping, as the latter is also used in the volume fragment program.`
- `TEST: -u has been replaced by -nr for rgbvectors (although the former will continue to work)`
- `ENH: Ability to modulate line vector length by another image. GL21 only`
- `TEST: Update test benchmark - vector normalisation now performed on GPU, which results in small colour difference`
- `Modulate line vector length by another image - GL14`
- `TEST: Line vector length modulation tests. Remove *nofloattexture* benchmarks, as they should match the normal benchmarks`
- `DOC: Small tweaks, changelog`
- `BF: modulate data was being reshaped in C order`

## <a id="fsl-base" href="#fsl-base">fsl-base 2411.0 -> 2502.1</a>

- [*MNT: Changes to support `micromamba`; remove `createFSLWrapper`/`removeFSLWrapper` scripts*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/117)
  Some small tweaks to work with `micromamba`-based FSL installations. The `createFSLWrapper`/`removeFSLWrapper` are now installed by the `fslinstaller.py` script (see https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/101).
- [*BF: `RPATHS` flags were not being added on macOS*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/118)
  The `RPATHS` flags need to be added to `ARCHLDFLAGS`, as `LDFLAGS` gets overridden from the environment in `vars.mk`.
- [*MNT: Add default compute capabilities for more CUDA versions*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/119)
- [*Fix bug in `.c` build rules; update CUDA C++ standard to `c++17`*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/120)
- [*MNT Guard against ipython/notebook not being installed*](https://git.fmrib.ox.ac.uk/fsl/base/-/merge_requests/121)

**Detailed history**

- `MNT: Wrapper management scripts have been moved to the fslinstaller`
- `MNT: Prefer micromamba, mamba, conda. Remove prefer_mamba flag`
- `RF: Don't call <conda> list to get rev history - read conda-meta/history directly. This is because  micromamba doesn't support the --revisions flag yet (and maybe never will)`
- `MNT: Remove/ignore --conda flag`
- `MNT: Strip base url from channel - in conda list --json output, conda doeds not include the base url, whereas micromamba does`
- `MNT: Call $FSLDIR/bin/python`
- `MNT: micromamba env export does not support -f option. Both conda and micromamba will export to stdout by default`
- `MNT: Accept --logfile`
- `TEST: Don't hard-code "conda". Print log file on failure`
- `BF: rpaths were being added to LDFLAGS, which was then getting overridden by the env later on (in vars.mk).`
- `DOC,MNT: Changelog, version`
- `MNT: Add default compute capabilities for more CUDA versions`
- `BF,RF: Using "CFLAGS" inside compilation rule generator seems to mess up C compilation in some circumstances. Update C++ standard to C++17 when compiling CUDA code using nvcc, as we will be building against CUDA 11 from this point on`
- `MNT Guard against ipython/notebook not being installed`

## <a id="fsl-installer" href="#fsl-installer">fsl-installer 3.15.1 -> 3.16.5</a>

- [*ENH: Support micromamba-based FSL installations*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/90)
  Add capability for creating an FSL installation based on [`micromamba`](https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html). This has a number of advantages, including:

   - Reduced download size - `micromamba` is released as a self-contained binary, a few MB in size. This is much smaller than `miniconda`/`miniforge` installers, which are up to 150MB.
   - Reduced chance of package conflicts during installation - `miniconda` installs a set of packages from the default anaconda channel, but during installation of FSL we source packages from `conda-forge`, which leads to an environment containing a mixture of packages from both channels. This has led to a number of package conflicts on certain platforms, most notably an incompatibility with the [`libarchive`](https://github.com/conda/conda-libmamba-solver/issues/283) library. This is in contrast to `micromamba`, which does not install any packages, meaning that when installing FSL, all packages will be sourced from `conda-forge`.

  The `fslinstaller.py` script will search for a `micromamba` entry in the release manifest, alongside the entries for different `miniconda` installers, e.g.:

  ```
  {
    ...
     "miniconda": {
       "linux-64": {
         ...
         "python3.12": {
           "output": "...",
           "sha256": "...",
           "url": "https://repo.anaconda.com/miniconda/Miniconda3-py312_24.7.1-0-Linux-x86_64.sh"
         },
         ...
         "micromamba": {
           "output": "...",
           "sha256": "...",
           "url": "https://anaconda.org/conda-forge/micromamba/2.0.5/download/linux-64/micromamba-2.0.5-0.tar.bz2"
         }
       },
    ...
  }
  ```

  Where `miniconda` installers are assumed to be shell scripts, and the `micromamba` installer is assumed to be a tar file. If a `micromamba` entry is present, the installer will preferentially select it, unless the user provides the `--conda` argument, in which case a `miniconda` entry will be selected. When `micromamba` is selected, the `micromamba` tar file is extracted, and then installation proceeds in the same way, with `${FSLDIR}/bin/micromamba` being used instead of `${FSLDIR}/bin/mamba`/`conda`.
- [*CI: Bug fix to CI deployment job*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/102)
- [*BF: Fix bug in `install_miniconda` routine*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/103)
- [*MNT: Prevent conda from installing conda-forge CUDA packages during main environment installation*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/104)
- [*`MAMBA_ROOT_PREFIX` needs to be set for child environment installations*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/105)
  When using `micromamba`, the `MAMBA_ROOT_PREFIX` environment variable needs to be set when installing child/extra environments such as `truenet`, otherwise `micromamba` will not be able to find `$PREFIX/.condarc`.
- [*MNT: Python 2.7 compatibility fix*](https://git.fmrib.ox.ac.uk/fsl/conda/installer/-/merge_requests/106)

**Detailed history**

- `CI: Typo in CI rules`
- `CI: Fix coverage reporting`
- `MNT: Add progress bar for micromamba install, for consistency in terminal output`
- `CI: Arguments appear to be getting passed in with leading whitespace, probably due to escaped linebreaks in .gitlab-ci.yml`
- `BF: Was incorrectly overwriting kwargs`
- `MNT,DOC: Changelog, version`
- `MNT: Prevent conda from installing conda-forge CUDA packages during main environment installation`
- `MAMBA_ROOT_PREFIX needs to be set for child env installations too, otherwise micromamba won't read $FSLDIR/.condarc`
- `MNT: exist_ok not available in py27`

## <a id="fsl_mrs" href="#fsl_mrs">fsl_mrs 2.3.1 -> 2.4.0</a>

- [*BF: Fix bug in fsl_mrs_summarise.*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/119)
- [*ENH: Improved preproc singleton handling*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/120)
  Improves the handling of data with trailing singleton dimensions in processing (align and average) and in fsl_mrs_preproc
- [*BF: unlike removal and headers*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/121)
  Fix bug reported by kristine@drcmr.dk. Headers weren't resampled by the unlike operation.
- [*BF: p-spline uncertainty and MH compatibility*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/122)
  - Added p-spline baseline option to MH fitting algorithm.
  - Updated documentation around MH fitting.
  - Widened MH fitting priors to allow better handling of poorly scaled data.
  - Fixed incorrect covariance and uncertainty calculation with new p-spline baseline option.

**Detailed history**

- `BF: Fix bug in fsl_mrs_summarise.`
- `ENH: Improved preproc singleton handling`
- `BF: unlike removal and headers`
- `BF: p-spline uncertainty and MH compatibility`

## <a id="fsl-pipe" href="#fsl-pipe">fsl-pipe 1.1.1 -> 1.2.2</a>

- [*Scale jobtime*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/37)
  Resolves #2
- [*Release v1.2.0*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/38)
- [*Fix and test parser*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/39)
- [*Release v1.2.1*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/40)
- [*Ensure scaled job times are still integers*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/41)
  Otherwise the sbatch tool crashes
- [*Release v1.2.2*](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/merge_requests/42)

**Detailed history**

- `Allow for float jobtimes when scaling`
- `Add scaling of jobtime to CLI`
- `Describe support for scaling jobtime`
- `Make use of dashes consistent`
- `Release v1.2.0`
- `Add tests for fsl-pipe parser`
- `Fix type of parameter`
- `Check that warning is emitted`
- `Remove excessive error handling`
- `Add tests for pattern matching`
- `Fix which files should exist if setting subject explicitly`
- `Explain changes in CLI`
- `Release v1.2.1`
- `Ensure scaled job times are still integers`
- `Release v1.2.2`

## <a id="fsl-pyfeeds" href="#fsl-pyfeeds">fsl-pyfeeds 0.12.4 -> 0.13.1</a>

- [*MNT: Ignore any candidate tests within output/benchmark/input data directories*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds/-/merge_requests/38)
   * Ignore candidate tests which are contained within the output/benchmark/input data directories, in case those directories are contained within the test directories. This allows input/benchmark directories to be stored alongside the test directories, e.g. in a flat directory structure.
   * New `evalImageMaxDiff` routine, which evaluates an image based on its maximum absolute difference with the benchmark image.
- [*MNT: Print hostname/platform inforomation, so we know what machine a test was run on*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds/-/merge_requests/39)

**Detailed history**

- `MNT: Ignore any candidate tests within output/benchmark/input data directories, in case any of those are contained within the test directories`
- `ENH: New evaluation routne to evaluate images based on the maximum absolute difference`
- `RF: Handle cases where in/out/bm dirs are not set`
- `CI: Drop py38/9, add py312/13`
- `BF: Typos`
- `MNT: Print hostname/platform info in log, so we know what machine a test was run on`

## <a id="fsl-pyfeeds-tests" href="#fsl-pyfeeds-tests">fsl-pyfeeds-tests 2407.0 -> 2501.0</a>

- [*BF: Fixes to truenet tests*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds-tests/-/merge_requests/65)
- [*Update example command in readme*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds-tests/-/merge_requests/66)
- [*More tweaks to readme*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds-tests/-/merge_requests/67)
- [*Allow mmorf executable to be specified by an environment variable*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds-tests/-/merge_requests/68)
- [*Bf/startup*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds-tests/-/merge_requests/69)
  This fixes two issues

  1) Using the system `activate` may fail as it can start using the `conda` libraries in `FSLDIR` which may not be compatible, e.g.:

  ```
    File "/opt/fmrib/conda/python3/bin/conda", line 12, in <module>
      from conda.cli import main
    File "/vols/Scratch/mwebster/fsl_dafc4c0c/lib/python3.12/site-packages/conda/cli/__init__.py", line 3, in <modul
  e>
      from .main import main  # NOQA
    File "/vols/Scratch/mwebster/fsl_dafc4c0c/lib/python3.12/site-packages/conda/cli/main.py", line 6, in <module>
      from ..deprecations import deprecated
    File "/vols/Scratch/mwebster/fsl_dafc4c0c/lib/python3.12/site-packages/conda/deprecations.py", line 60
      if version_tuple := self._get_version_tuple(version):
  ```


  2) The current script will try to build in the _test_ directory and not the _output_ directory. This fix simply copies the `Makefile` and source files to the output directory.
- [*Bf/filenames*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds-tests/-/merge_requests/70)
  Some of the input files/paths don't exist in `dataSets`. Renamed to use `subj1.bedpostX`
- [*MNT: Adjust `fslStartup` test to support micromamba-based installations*](https://git.fmrib.ox.ac.uk/fsl/pyfeeds-tests/-/merge_requests/71)

**Detailed history**

- `BF: Fixes to truenet tests`
- `Update example command in readme`
- `More tweaks to readme`
- `Allow mmorf executable to be specified by an environment variable`
- `BF: Compilation must occur in output directory`
- `MNT: Removed debug print`
- `BF: Original paths/files don't exist in our datasets`
- `MNT: Adjust fslStartup test to support micromamba-based installations`

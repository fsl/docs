# FSL 6.0.7.12, 17th June 2024


## fmrib-unpack 3.8.0 -> 3.8.1

- MNT: Minor changes to placate a range of pandas and numpy (mostly the former) deprecations

### Detailed history

- CI: drop py38, add py312
- MNT: remove use of deprecated delim_whitespace argument to pd.read_csv
- MNT: Don't pass empty dataframes to pd.concat
- MNT: Another use of delim_whitespace; use raw strings for regexes
- TEST: remove use of infer_datetime_format; kwargs only for calls to to_csv; don't pass errors argument to to_datetime
- MNT: Explicitly make CTYPES integer, otherwise pandas warns about enum->float64 conversion
- TEST: More pandas/numpy deprecation fixes
- MNT: pd.unique will not accept pythonlist in the future, use np.unique instead; pd.DataFrame.dtypes is a pd.Series, so use iloc to get first element
- MNT: Update UKB schema
- DOC,TEST: version
- MNT: Fix bug in UKB schema file
- TEST: -nd was not being applied in test_demo calls, which was causing demo tests to fail because the current online version of field.txt is malformed. Yay.
- CI: Drop pandas 1.x testing
- MNT: Revert enum change as it caused more problems
- TEST: Need a space
- MNT: Explicitly handle empty data
- RF: Adjust ctypes enum issue in yet a different way
- TEST: bugs in prior refactor

## fsleyes 1.11.0 -> 1.12.3

- *ENH: New `--autoName` option*

  New `--autoName` / `-an` option which automatically renames overlays that would otherwise have the same name. For example two images `a/T1.nii.gz` and `b/T1.nii.gz` would both be given the name `T1`. With the `--autoName` option they would be respectively named `a/T1` and `b/T1`. Only on-disk overlays are renamed - in-memory overlays (e.g. masks created in the image editor) are not renamed. 

  Note that the names of manually renamed overlays may be overwritten when this setting is enabled.

- MNT: Update *Show command-line for scene* menu item to support `--asVoxels` for lightbox views

### Detailed history

- ENH: Skeleton for new --autoName option
- ENH: Automatically rename overlays with the same name when --autoName option is passed
- TEST: Test autoname
- MNT: Scaffolding for supporting command-line generation of --asVoxels options
- MNT: Implement zrange/sliceSpacing -> voxel conversion. Still needs some tweaking
- TEST: Failing zrange/sliceSpacing convesion round trip test
- BF: Fixes to getSlicesAsVoxels routine
- TEST: updates to asVoxels test
- ENH: New utility function to lazily import a module
- TEST: lazyimport unit test
- BF: Need to call PlatformDefaults, otherwise can crash with GLX BadValue error in some environments
- MNT: Lazily import OpenGL.GL, as on linux we need to be able to select the PyOpenGL backend platform (EGL/GLX) after a window has been displayed, but before any GL work takes place
- RF: Rename lazyimport module to make usage a bit cleaner
- TEST: update lazyimport test
- MNT: update gl lazyimport use
- MNT: Lazily import OpenGL.GL everywhere
- RF: Avoid accessing any OpenGL submodules at import time, as it causes PyOpeGL to freeze the backend (EGL/GLX)
- RF: Update method used to determine a suitable value of PYOPENGL_PLATFORM
- BF,RF: Wrong logic in selectPyOpenGLPlatform. Don't do anything under Windows. String formatting updates
- RF: Rename GLCanvasTarget._setGLContext to setGLContext - external code needs to be able to call it, e.g. Annotations. Overwrite IsShownOnScreen to return IsShown on Windows
- MNT: Do not apply wx.FRAME_TOOL_WINDOW style to AuiFloatingFrame windows in WSL, as this seems to be the cause of the unresponsive dialog window issue
- MNT: Fix guards in FRAME_TOOL_WINDOW patch
- MNT: update FSL doc URL
- MNT: Update FSL doc URLs
- MNT: Need latest fslpy
- CI: Looks like pip behaviour is changed - I'm sure it used to downgrade dev packages to stable packages, but it seems to be ok now
- BF: Typo in __createWXGLContext
- MNT: Seems that mipmap generation is always required on macOS
- MNT: Adjust mipmap generation for older GL versions
- DOC: Add rtd dark mode extension to user and API docs [skip ci]
- DOC: Fix little typo in cmap overview
- BF: Names not being read correctly from lut file which contains comments
- TEST: regression test
- DOC: Fix MR number
- MNT: Remove mip-mapping, and vbox chromium driver workarounds from 3D texture configuration
- MNT: Delete/recreate texture handle on every refresh on intel macs using an integratedd intel GPU, to work around texture corruption issues

## fslpy 3.18.2 -> 3.19.0

- *BF: Fix issue with `fsl.wrappers` functions ignoring  `$FSLOUTPUTTYPE` when searching for output files*

  Reported on the FSL mailing list at https://www.jiscmail.ac.uk/cgi-bin/wa-jisc.exe?A2=FSL;673217e7.2405

  `fsl.wrappers` functions which allow output image files to be loaded (via `fsl.wrappers.LOAD`) were only searching for files ending in `.nii.gz`, instead of honouring the `FSLOUTPUTTYPE` environment variable.

- *Adding a new wrapper for bedpostx*

  Adding a new wrapper for bedpostx so that there is a bedpostx_postproc that does not use gpu

- *ENH: Add wrapper functions for `bedpostx` and `bedpostx_gpu`*

  The (unreleased) `bedpostx_postproc` wrapper function has been removed, and the `bedpostx_postproc_gpu` wrapper function has been deprecated, as the `bedpostx_postproc[_gpu].sh` scripts are not intended to be called directly.

- ENH: Add new `platform.wsl` property for detecting whether we are running under WSL

### Detailed history

- TEST: Failing regression test for fileOrImage file suffix issue
- BF: Honour $FSLOUTPUTTYPE, otherwise output files saved by FSL tools may not be detected
- TEST: Remove all relative imports from test modules as it is incompatible with the default import mode in pytest >= 8
- TEST: fix error in adjusted imports
- Adding new wrapper to bedpostx
- DOC: apidoc
- TEST: unit test for bedpostx_postproc wrapper
- Adding wrappers for bedpostx and bedpostx_gpu
- MNT: Remove bedpostx_postproc, and deprecate bedpostx_postproc_gpu, as they're not intended to be called directly. Add bedpostx[_gpu] wrapper functions
- TEST: add bedpostx[_gpu] tests
- CI: Drop py38, add py312
- ENH: new platform.wsl property for detecting WSL
- TEST: Smoke tests for platform properties
- MNT: Pre-release version bump
- TEST: Typo in test

## fsl-bet2 2111.7 -> 2111.8

- ENH: Compatibility with BSD sed

### Detailed history

- ENH: Compatibility with BSD sed

## fsl-feat5 2201.5 -> 2201.6

- BF: Pass fnirt_config through

### Detailed history

- BF: Pass fnirt_config through

## fsl-fnirt 2203.0 -> 2203.1

- *BF: Fix `fnirt_error.what()` - was returning a pointer to a local variable*

  This could result in garbage error messages being emitted, e.g.:

  ```
  Error occurred when preparing to fnirt
  Exception thrown with message: >"G\
  ```

- *Fixed logic in invwarp that detects out-of-FOV voxels when --in > --ref*

  It turned out invwarp was very slow. This was due to a failure to properly detect voxels in the inverse warp field that was out of the FOV of the original --ref. That meant that there as a sharp edge between the "valid" and the "invalid" parts of the field in the inverse field. These edges were then detected as singularities, and "nudged" by NEWIMAGE::constrain_topology, which took a very long time.
  It now correctly detects the "out of FOV" voxels and use a mean dilation from the "valid" to the "invalid" parts of the field. This makes it much faster, and also a little more accurate.

- *Fixed tiny bug in mean dilation*

  Fixed bug that meant that the mean was calculated very mildly wrong if the voxel happened to have the index 1 in any of the directions.


### Detailed history

- BF: Fix fnirt_error.what() - was returning a pointer to a local variable
- Fixed logic that detects out-of-FOV voxels when --in > --ref
- Fixed tiny bug in mean dilation

## fsl-pyfix 0.7.0 -> 0.8.0

- *MNT: Allow custom mask image directory to be passed via `--species` argument*

  This allows pyFIX to be used on data from different species, or aligned to a reference image other than the MNI152, without the sagittal masks having to be explicitly added into pyFIX.


### Detailed history

- MNT: Allow custom masks to be passed in via --species argument.
- TEST: Test passing custom mask directory
- MNT: Print reference image in log message
- MNT: Allow feature extractor decorator to be used without registration - useful for testing
- TEST: MAke sure that custom species directory is passed through from command-line when doing feature extraction

## fsl-sub 2.8.3 -> 2.8.4

- *Return False when projects key not present in*

  Fixes issue #16 
    A malformed configuration file can cause a crash when parsing the command line
  Fix incorrect 'del' usage
  Handle missing mail_support and projects configuration options when parsing the command line

- *DOC: Emphasise that package name contains hyphens; some formatting fixes*

  The `fsl-sub` conda package has been renamed from `fsl_sub` to `fsl-sub` for parity with the PyPi and conda-forge releases. This is an annoying constraint of publishing to PyPi - it automatically converts underscores to hyphens.

- *BF: Accept command-file with semi-colons*

  (Discovered by @flange)

  Submitting an array task file with multiple commands per line is common practice, e.g. a file called `commands.txt` may contain:

  ```
  cmd1 subject1/; cmd2 subject1/
  cmd1 subject2/; cmd2 subject2/
  ```
  and be submitted via `fsl_sub -t commands.txt`

  However, when given a command file which contains multiple commands per line, where the first command does not take any arguments, `fsl_sub` throws an error. For example,. this file:

  ```
  cmd1; cmd2 subject1/
  cmd1; cmd2 subject2/
  ```

  would result in an error such as:

  ```
  Traceback (most recent call last):
    File "fsl_sub/utils.py", line 305, in check_command_file
      check_command(cmd)
    File "fsl_sub/utils.py", line 282, in check_command
      raise CommandError("Cannot find script/binary '{}'".format(cmd))
  fsl_sub.exceptions.CommandError: Cannot find script/binary 'cmd1;'
  ```

  This error originates in the `fsl_sub.utils.check_command_file` function, which essentially rejects command files where `shutil.which(shlex.split(line)[0])` returns `None`, for any line in the file. In the second example above, this would resolve to `shutil.which('cmd1;')`, with the trailing semi-colon causing `shutil.which` to return `None`.

  This PR simply ensures that trailing semi-colons are stripped from the tested command, and adds a regression test to that effect.

- Update changelog and version number

### Detailed history

- Return False when projects key not present in method options
- Disable mail options when mail_support key missing
- Incorrect usage of the 'del' keyword
- Handle miss-configuration where Parallel Environments are enabled but none are configured in the queues Fixes issue #16
- DOC: Emphasise that package name contains hyphens; some formatting fixes
- BF: Ignore trailing semi-colons in first command read from command file
- TEST: Make sure commands with semi-colons are accepted
- Changelog/version

## fsl-topup 2203.2 -> 2203.4

- *Coordinated check of readout time with eddy*

  topup used to accept total readout values which were later rejected by eddy, which could be very annoying. Now the checks are the same for topup and eddy.

- *Added --acqp as alternative to --datain*

  Added --acqp as alternative to --datain. It has irked me for a long time that the exact same parameter has different names in topup and eddy. I have retained the option to use --datain, so as not to brake any existing scripts.

- *Added --featout option and fixed buglet*

  I have added a --featout option that will generate two files. If one specifies --featout=my_fname the files will be named my_fname_fieldmap_rads and my_fname_fieldmap_mag, where the first is the field in rad/s and the second is the average unwarped image. These can be directly used in FEAT.
  In the process of doing that I discovered a "buglet", which meant that, even in the absence of movement, the most basal slice would get replicated at the very top. This had been reported as a bug, but I had not seen it because of the masking that is done. But it did mean that there would always be a missing slice at the top, which could have been a problem if the FOV was very tight in the slice direction. And it prevented me from outputting an unwarped image that wasn't truncated at the top and/or bottom.
  I have now fixed the bug, which means that my_fname_fieldmap_mag will have full FOV. I also added writing of a mean unwarped image as part of the --iout output, also with full FOV. This will be useful for creating a mask for eddy and/or dtifit.
  It means that for data with very tight FOV the estimated field will be slightly different, especially in the topmost slice. But the field generated by the new version will be, slightly, more accurate.


### Detailed history

- Coordinated check of readout time with eddy
- Added --acqp as alternative to --datain
- Added --featout option and fixed buglet

## fsl-xtract 2.0.4 -> 2.0.5


### Detailed history

- Merge pull request #6 from SPMIC-UoN/xpy
- fix typo
- return to accepting any format warp fields
- fix bug with missing tracts
- bug fix with savetxt

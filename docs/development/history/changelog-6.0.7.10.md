# FSL 6.0.7.10, 09th April 2024


## fsleyes 1.10.2 -> 1.10.4


### Detailed history

- DOC: Fix link
- BF: Fix depth sort shader so that it ignores transparent/killed inputs
- ENH: New -u/--ungroupOverlays option, which disables overlay grouping (i.e. so that overlays are ungrouped/unlinked by default)
- BF: Change sort routine to use a stable sorting algorithm, so that the order of equal entries is preserved. This means that when blending overlays with the same depth, the user can select which one is to be displayed on top via the overlay list order.
- DOC: Describe mesh coordinate space options
- RF: Reverse overlay order when calling blend shader, so that overlays which are higher in the list are drawn on top (where depths are equal)
- TEST: A couple more blending tests, to test behaviour with transparent fragments (clip plane), and with ordering of overlays of equal depth
- TEST: Test ungroup overlays. Hack to run specific CLI tests
- DOC: Changelog; overview of new lightbox settings - I forgot to add this when developing them
- TEST: update one test that was affected by 3D depth update
- TEST: Update benchmark - slight difference in default tracto line width in CI environment
- RF: Add a few more properties that are linked across overlays by default
- BF: Make sure referred types are imported
- BF: Lightbox slice overlap broke label/mask overlays - seems that offset xform was being applied twice
- TEST: Rgression test for mask/label overlap bug
- TEST: update orient tests affected by new grouped/linked display properties
- RF: Make lightboxcanvas honour colour for slice labels
- RF: Allow patch ordering to be controlled
- MNT: Override send2trash on old macOS
- DOC: comment about projecting volumetric data onto surface
- BF: MetaPathFinders need to implement find_spec

## fslpy 3.18.1 -> 3.18.2


### Detailed history

- Remove duplicate test_func_to_cmd test  This test is already defined in test_run.py
- Fix the test_job_output to actually test something
- Test calling func_to_cmd on a fslpy wrapper
- Store dry run commands in global variable  This fixes an infinite regression error when pickling any of the wrapper functions. For unclear reasons this regression error was caused by having the `commands` attribute of the `fsl.utils.run.dryrun` function.
- Remove unused `real_stdout` attribute  The reason for removing this is that pytest sets the `sys.stdout` to some object which cannot be pickled. So, any `test_run.test_wrapper_to_cmd` fails within pytest, even though there is no such problem outside of pytest.

## fsl-base 2401.0 -> 2404.0

- Remove obsolete `${FSLGECUDAQ}` environment variable from shell initialisation scripts
- *BF: Bug fix to `update_fsl_package`*

  Don't request a specific build string for packages when running `update_fsl_package`, as some `noarch` packages (e.g. FSLeyes) are actually built for each platform (macOS, Linux, Windows), and have the platform encoded into the build string, so matching against the full build string would potentially request a build of the package for the wrong platform. Instead, just match against the build number, which should be all that is needed to request the latest version.


### Detailed history

- MNT: Remove obsolete FSLGECUDAQ variable from fsl.[c]sh
- BF: Don't request specific build string, as some noarch packages (e.g. FSLeyes) actually have the platform encoded into the build string, so matching against the full build string is potentially requesting a build of the package for the wrong platform.

## fsl-data_atlases_xtract 1.1.0 -> 1.2.0

- *Neo update*

  Added neonatal atlases


### Detailed history

- added neonatal atlases
- update macaque atlas names
- added neonatal atlases

## fsl-fdt 2202.7 -> 2202.8

- *BF: Update GUI to call `$FSLDIR/bin/eddy`, remove "Use GPU version" checkbox*

  The `eddy` executable files were renamed to `eddy_cpu` and `eddy_cuda10.2` about two years ago, and a helper script simply called `eddy` was added, which will call one or the other, depending on whether or not a GPU is present.


### Detailed history

- BF: Update GUI to call $FSLDIR/bin/eddy, remove Use GPU checkbox

## fsl-sub-plugin-slurm 1.6.2 -> 1.6.3


### Detailed history

- Updates to support python 3.12
- DOC: Emphasise that package names contain hyphens and not underscores
- Add option and environment variable to allow fsl_sub to submit jobs when already running within a cluster job This is of most use when using with Open OnDemand batch connect desktops which are typically queued jobs.
- Don't use 'type' use 'is'
- Clean up README.md
- Must be isinstance not comparison to object

## fsl-xtract 2.0.3 -> 2.0.4


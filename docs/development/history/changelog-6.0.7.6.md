# FSL 6.0.7.6, 16th November 2023

- **New** fsl-data_omm: Multimodal templates constructed by jointly optimising similarity across multiple scalar- and tensor-based modalities using MMORF for nonlinear registrations.

## fsleyes-plugin-mrs 0.1.1 -> 0.1.2

- *BF: Fix FSLeyes layout injection*

  Version parsing was buggy (e.g `"1.10"` was being interpreted as numeric `1.1`), and wrong layout variable name (`mrs_layout` rather than `mrs_fsleyes_layout`).


### Detailed history

- BF: Fix FSLeyes layout injection - version parsing was buggy (e.g "1.10" was being interpreted as float 1.1), and wrong layout var name ("mrs_layout" rather than "mrs_fsleyes_layout")
- Version 0.1.2

## fslpy 3.15.1 -> 3.15.3


### Detailed history

- BF: Obsolete cluster function was masking the new one
- TEST: Remove obsolete cluster test
- MNT: Suppress cluster stdout unless otherwise instructed
- MNT: The file format that keeps on giving. Old label files may not contain an entry for all components.
- TEST: Accept files with missing components
- MNT: Handle FIX files with two lines (meldir, noisy comps)
- TEST: Another fix labels test case
- BF: Wrong argument names to fsl_sub. Fix a few documentation quirks

## fsl-nets 0.8.1 -> 0.8.2

- *MNT: Run `randomise` via `fsl_sub`*

  Change `nets.glm` so that `randomise` is executed via `fsl_sub`. This has the effect that, even when running locally, any standard output/error emitted by `randomise` will be saved as `*.o` / `*.e` files in the present working directory.

- MNT: If running randomise on the cluster, block execution until it has completed

### Detailed history

- MNT: Submit randomise call so that stdout/stderr files are saved
- MNT: If running randomise on the cluster, block execution until it has completed

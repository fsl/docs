## 6.0.7, 14th July 2023

### fsleyes             	1.6.1     	-->	1.7.0


- New colour range option (available via the --colourRange command-line option) which can be used on vector overlays when colouring them by a secondary image, to specify the mapping between the voxel intensities and the colour map.
- New Show slice location option, allowing the location of each slice to be displayed in the lightbox view.
- Changed the --initialDisplayRange command-line option to have the same behaviour as the --displayRange option - by default, the values will now be interpreted as raw intensities. Values can be specified as percentiles by appending a % to the high value.
- Colour map and lookup table files may now have a .txt suffix instead of .cmap / .lut.
- Colour map interpolation will now be applied to the colour bar shown in the ortho/lightbox/3D views.
- Changed the behaviour of the Clip by and Modulate by settings for volume overlays - when clipping/modulating by a secondary image, and a negative colour map is in use, the absolute values of the secondary image are now used for clipping/modulation.
- Changes to the mechanism used to save screenshots/movies, which should make the process more robust.
- Fixed some issues with parsing command line arguments for RGB vector and complex images.
- Fixed an issue with plotting the MELODIC power spectrum for data with an odd number of timepoints.
- Fixed an issue when passing a colour map file path to the --negativeCmap option.
- Fixed an issue related to loading NIfTI qform matrices which arose with nibabel >= 5.1.0.
- Fixed an edit mode issue where a drawn line would have gaps in it.
- Some minor fixes which allow the overlay type of a NIfTI image to be changed whilst it is being edited.
- Fixed a small issue with volume overlays sometimes not being refreshed.


### fslpy               	3.11.3    	-->	3.13.2

- New scaleAtOrigin option to the affine.compose() function, which allows the origin to be preserved by scaling parameters.
- New runfunc() function which can be used to execute a Python function in a separate process (or as a submitted cluster job), via the func_to_cmd() function.
- New keys(), values(), and items() methods on the Cache class.
- The run.func_to_cmd(), run.hold(), and run.job_output() functions have been moved from fsl.utils.fslsub to the fsl.utils.run module.
- The fsl.utils.fslsub module has been deprecated, and scheduled for removal in fslpy 4.0.0.
- The fsl.scripts.Text2Vest now handles column vector inputs correctly.
- The tempdir() function was not changing back to the original working directory when the override argument was used.
- New randomise() wrapper function.
- The fslmaths() wrapper function now allows the -dt and -odt options to be set.
- Assertions (from the assertions module) called within wrapper functions are now disabled if the command is to be submitted via fsl_sub.
- The Image class will now resolve symlinks when loading images from file.
- The fslstats() wrapper no longer overwrites the log option that is passed to run(), if a wrapperconfig() context is active.



### fsl-add_module      	0.4.0     	-->	0.5.0

- New command to generate/update a manifest from a set of files
- Rename Plugin.termsOfUse to Plugin.terms_of_use, so naming is same in code and JSON
- Ability to refer to FSL course / grad course manifests by alias
- Add support for a "size" field


### fsl-base            	2303.2    	-->	2306.1

- New --conda options for update_fsl_package/release to prefer conda over mamba
- New fslnotebook alias
- New custominstall make rule - can optionally be defined in project Makefile for any custom installation steps (e.g. flirt/feat5 "schedule" rules).
- Adjust the update_fsl_package so that it does not call conda with --freeze-installed or --no-channel-priority - they cause too much problems; in particular, conda seems to dislike being asked to install/update anything if it is not allowed to update ssl/certificates (which it can't do when called with --freeze-installed).


### fsl-bet2            	2111.2    	-->	2111.4

- ENH: bet4animal from Takuya Hayashi
- add h2c function and --ct option


### fsl-bianca          	2209.2    	-->	2209.3

- BF: Removed breaking .nii.gz from remove_ext call


### fsl-feat5           	2201.4    	-->	2201.5

- MNT: Replace "defaultfiles" with "custominstall", so it gets executed at install time, not build time.


### fsl-flirt           	2111.1    	-->	2111.2

- MNT: Some additional dc calls that were missed in the code audit
- MNT: Use new "custominstall" rule, which gets executed by make install - the schedule rule was being executed at build time.


### fsl-get_standard    	0.1.0     	-->	0.2.0

- Don't absorb system exit
- raise exception if an unknown resolution is requested


### fsl-giftiio         	2111.0    	-->	2111.1

- MNT: Pre-declare one nifti_ function which was being implicitly used in gifti_tool.c
- MNT: Add an alternative header for C++


### fsl-installer       	3.4.1     	-->	3.5.3

- New --conda option to use conda instead of mamba
- Print an error message on attempt to install FSL < 6.0.6
- New hidden --debug option which enables super verbose logging on conda/mamba
- Use pwd module instead of op.expanduser to determine user home directory
- New hidden --root_env option to disable automatic --no_env when run as root. Adding primarily for testing purposes
- Adjust defaults when installer is called as root - install to /usr/local/fsl/, and don't modify shell profile


### fsl-mist            	2111.3    	-->	2111.5

- MNT: Update for nibabel 5 API
- MNT: VTK to 9.2


### fsl_mrs             	2.1.0     	-->	2.1.6

- Merge branch 'enh/fmrs_model_improvements' into 'master'
- Update changelog info
- Test tweaks
- Fix one fo the new tests.
- lint
- Add more informative errors in dynamic fitting module and tests.
- Add very basic tests for scalings.
- First pass at scaling of dynamic results.
- Remove debug logging
- Add ability to form betas with different parameter categories
- Better and tested linewidth estimates.
- Add model performance parameters to dynamic output.
- Merge branch 'enh/mrsi_coil_comb' into 'master'
- Fix phase test.
- Update changelog date.
- Final set of coil combine tests. cov/noise extended to commandline scripts.
- Update docs.
- Add noise and covariance options to coil combine in fsl_mrs_proc
- lint
- Further testing
- Update denmatsim commit.
- New structure for mrsi and tests for coil combination
- Merge branch 'bf/fsl_proc_apodise' into 'master'
- Typo
- Fix non working fsl_mrs_proc apodize
- Merge branch 'bf/fsl_summarise_names3' into 'master'
- [skip-tests] attempt 3 at summarise naming fix.
- Merge branch 'bf/fsl_summarise_names2' into 'master'
- [skip-tests] attempt 2 at summarise naming fix.
- Merge branch 'bf/summarise_naming' into 'master'
- Improved name ID for directories with subdirectories.
- Merge branch 'bf/qc_warnings' into 'master'
- Update QC detrending.
- Merge branch 'enh/basis_in_report' into 'master'
- Update report with new basis view.
- Merge branch 'bf/fsl_summarise_conj' into 'master'
- Update install instructions.
- Ensure conjugation of basis in summarise matches that in fitting.
- Merge branch 'bf/matplotlib_grid' into 'master'
- Update changelog version date.
- Fix depreciation bug in plotting.
- Merge branch 'enh/x_nuclei' into 'master'
- Update tests for new extension in symlinks.
- Minor fixes for x-nuceli and symlinks
- Updates for x-nuclei handling


### fsl-newimage        	2203.8    	-->	2203.9

- BF: samesize shouldn't write to cerr


### fsl_sub             	2.7.5     	-->	2.8.3

- Merge branch 'shell_to_files_default' into 'master'
- Return default of shell log output being
- Add license file Change development status
- Bump version number
- Fix bad indentation
- Testing an file opened for writing is not safe on Linux hosts Test for non-file redirecting exception not matching generic error message
- Fix utils tests
- Make sure exception tests are outside of with block
- Merge branch 'mnt/pypi-publish' into 'master'
- Fix _run_job test
- Remove Python 3.6 from requirements
- MNT: add long_description to setup call
- TEST: Work around strange mock conflict in python 3.11
- CI: Remove py36, add py310+311 to tests. Add build+deploy pypi package jobs
- MNT: Change license to Apache, update supported pyvers
- Make shell runner output to controlling console
- Linting
- Improve handling of thread requests on SLURM
- Update template to suggest shutil.move as this supports moving files across file systems
- Add some debug messages to configuration loader Lint clean up
- Add check for empty queues


### fsl_sub_plugin_sge  	1.5.2     	-->	1.5.4

- Switch to Apache 2 license Make Python 3.7 earliest supported version
- Merge branch 'mnt/pypi-publish' into 'master'
- Update setup.py to require python 3.7
- MNT: revert name changes
- MNT: fsl_sub -> fslsub
- CI: Need fsl_sub
- CI: Pipeline config for unit tests and pypi build+publish
- MNT: Change name, change license to Apache, update python versions
- Merge branch 'mnt/qsub-output' into 'master'
- MNT: Ignore additional messages emitted from qsub when scraping job ID
- Use warnings not logger.warn
- Ensure fsl_sub 2.7 is installed
- Merge branch 'master' of git.fmrib.ox.ac.uk:fsl/fsl_sub_plugin_sge
- Add method to report whether the plugin suggests GPU config
- Merge branch 'master' of git.fmrib.ox.ac.uk:fsl/fsl_sub_plugin_sge
- Add configuration option to specify that the plugin supports parallel environments.


### fsl_sub_plugin_slurm	1.4.4     	-->	1.6.1

- Fix issue #7  'jobhold' of empty list/tuples is interpreted as a valid job hold list This causes the slurm submission to be invalid.  Add additional input validation and associated tests.
- Fix minor linting issues
- Improve reliability of tests against time based output
- Switch default for job dependencies to 'afterany' from 'afterok'.  This is the SLURM default Introduce option/environment variable to control above Add ability to specify complex SLURM job holds via the -j option. Refactor code which builds the dependency argument to simplify testing Add tests for job holds
- Bump version number
- Add ability to retain the job script on failure to submit  Resolves issue #6
- Move minimum python supported version to 3.7 Add license file
- Merge branch 'mnt/pypi-publish' into 'master'
- Update setup.py to require python 3.7 as per header
- MNT: revert name change
- CI: Pipeline config for unit tests and pypi build+publish
- MNT: Change name, change license to Apache, update python versions
- Merge:
- Fix specification of constraints Add mem/cpu-per-gpu support
- Merge branch 'bf/get_queue_gres' into 'master'
- BF: Splitting wrong string


### fsl-surface         	2111.0    	-->	2111.1

- Fixed some typo's in stdout text from surf2surf


### fsl-topup           	2203.1    	-->	2203.2

- New config file that works a little better on data with large and sharp distortions


### fsl-xtract          	1.6.2     	-->	1.6.4.1

- Added xtract_qc, add subcortical to blueprint

# FSL 6.0.4, 11th August 2020


## `avwutils`

- Use enough leading zeros based on number of number of output files, to allow them to be sorted alphabetically (but always at least 4)
- fslcreatehd should always honour `FSLOUTPUTTYPE`, even if the file already exists

## `basil`

- Add `--batim` to provide voxelwise ATT estimates

## `bet2`

- Updated error trap to new form

## `bianca`

- Remove unicode characters

## `cluster`

- Fix smoothest verbose printing
- Fixed FWHM reporting

## `config`

- Switch `INC_PROB` for legacy projects
- use cfzlib rather than system zlib
- Remove system path from ZLIB defines as causing macports builds to break
- Added fdt/ptx2 rules for master build
- Added variables to control ptx2/fdt during main build
- Switch cuda path for singularity images

## `cprob`

- Move `libprob.h` from the soon-to-be-defunct `libprob` project into `cprob`

## `data_standard`

- Removed legacy/linear images

## `distbase`

- Rename `mist-clean` -> `mist`
- rename `deface` -> `fsl_deface`
- build zlib for all builds

## eddy

- The changes to eddy are mainly internal. A lot of effort has gone into speeding up in particular the slice-to-volume motion correction and the movement-by-susceptibility estimation. This effort has focused on the GPU implementation, and you should expect a 2--5-fold speed up depending on what settings you use. As part of the speed-up efforts it has also become more "stable", in that any given update step is much more likely to take us towards the "true solution", and less likely to end up having to be rejected.
- It may seem that those using the non-GPU version of eddy has been short changed in this release. And in a way they have, but in a more important way they have not. The reason that slice-to-volume registration has not been implemented for the CPU/OpenMP version is that we have perceived such an implementation too slow to be practically useful. But a major part of the speed-up of the GPU version has _not_ been more effective code at a detail level, but rather finding new ways of calculating things. And these are equally applicable to the CPU version, so for the next release we envisage CPU-slice-to-vol. Not fast, but bearable.

## `fabber_core`

- update linking order
- Update for FSL 6.0.3 build system
- Fix to trialmode convergence detector so that it still respects max iterations
- Protect calculation of coefficient resels from numerical errors in inference Belt and braces approach
- ignore voxels where we have already had problems and also catch any exceptions. We don't want a run to be stopped just because of a numerical problem here (e.g. non-invertible covariance matrix)
- Try to be more tolerant of bad MVN data when constructing the final result MVN Instabilities during inference may have led to a non-invertible covariance matrix. These voxels are ignored during subsequent inference steps but can cause an exception at the end when we construct the combined params/noise MVN. This protection substitutes zeros when this happens since we have nothing valid for this voxel
- Move log message to avoid confusion - suggests data is being loaded when the file does not exist
- Add option to save variance directly (rather than have to square the STD or get it out of the MVN)
- Fixed data type of evaluate-nt option
- Fixed bug which caused new-style models to write out an empty paramfiles.txt
- Clarify nature of noise parameter being inferred
- Priors consistent with SVB
- Minor addition to priors docs
- Minor tweaks to build instructions
- Clarify that you should try compiling before messing with config
- Too many heading levels...
- Fix broken link
- More detail about setting up FSL dev environment

## `fabber_models_asl`

- change link order
- Hidden option for leadscale value to enable experimentation in future
- Revert previous change to AIF lead in/out scale parameter Gives odd results on test data - to investigate more carefully before making any change
- Relax leadscale to avoid sharp edged arterial bolus
- Remove duplicated member variable what was breaking ARD This variable is defined in the base class and therefore the subclass copy was masking the assignment of ARD to the arterial component
- Add --tiimg option to provide a voxelwise TI image This is handled identically to providing TIs as a list except they are able to vary by voxel. The number of volumes in the image is the number of TIs. Note that PLDs are not supported - it must be TIs
- Change BAT prior to 1.3s which is appropriate with pCASL Multi-TE implies pCASL
- Document sliceband option
- Bugfixes - result sizing gets LFA wrong way round, also incorrect indexing on vector and failure to increment timepoint. (Other than that, fine)
- Add support for variable repeats and multi-band readout
- Add basic documentation
- Removed build scripts which are no longer in use
- Fix incorrect check on number of volumes in data (ignoring repeats)
- Fixed bugs present in previous premature commit!
- Use same defaults as aslrest/oxford_asl for T1, T2, BAT etc.
- Double precision on variables to avoid numerical overflow errors
- Fix typo breaking build
- Clean out of currently unused options and parameters Some of these (e.g. inferring tau, arterial component) may be reintroduced in the future but the existing code is not functional and is easily re-written as necessary - no need for it to clutter up the model at present
- Support variable tau values
- Tweaks to improve fitting to test data. Initialize phase from minimum rather than maximum as it is generally more specific. Also relax prior on phase and clarify restriction of range of phase in evaluation. Now fits test data nicely
- Ignore test output (and remove file added in wrong place!)
- Ignore test output
- Updated multiphase test and put in own dir
- Document all used options
- Fix multiple errors in log when using --save-model-extras This is because the AIF output does not typically have the same number of output volumes as the data.
- Add multi-TE model, refactored to new API from Federico's code

## `fabber_models_cest`

- change link order for FSL 6.0.4
- Remove commented out old code
- Remove old build script no longer in use
- Add placeholder documentation
- Correct option type for fa and tr, plus improve log output a bit
- Minor stylistic changes to new steady shape code
- Document lineshape option

## `fabber_models_dce`

- change link order for FSL 6.0.4
- New priors based on literature value
- Remove old misleading comment
- Correct option name for AIF T1 value

## `fabber_models_dsc`

- change link order for FSL 6.0.4
- Roll back on previous changes - do not try to clamp parameters to sensible limits This is largely pointless since by clamping the parameter values we ensure that the gradients are zero - so if the parameter gets into this range it will never get out. Instead we have added more robust protection against numerical errors to the fabber core.
- Restrict CBF and ABV to >= 0 to avoid numerical problems in nonbrain voxels Would prefer to let the model recover from this itself but it seems to struggle to do so.
- Clarify that delt is actually in seconds not minutes Tweak priors to avoid instability caused by very very uninformative priors Numerical warnings should be displayed only once per evaluation
- Add initialization method for voxel posterior
- Initialize sig0 parameter from first data point Also tweak prior mean for cbf so we can add a log prior without redefining the mean
- Remove references to Verbena in theory section
- Updates
- Add image
- Initial documentation

## `fabber_models_qbold`

- change link order for FSL 6.0.4
- Remove old build scripts not needed any more
- Add basic documentation - mostly placeholder for now.
- Re-order parameter extraction to allow for change in parameter order
- Make sure all opts have a documentation string Put sig0 as first parameter as its the main one that willl always be included

## `fdt`

- change linking order
- add `-db` flag to set maximum offset in b-value
- `select_dwi_vols` fix requested by MC
- Update dtifitOptions to describe `--kurtdir` output
- remove mean kurtosis map (`--kurt` produces a nicer map)
- Raise errors of incompatible flags before reading the data
- Raise error if `--wls` and `--kurtdir` flags set
- back to 1% of S0 as minimum signal
- add prior to kurtosis values
- allow lower minimum signal when fitting full kurtosis model
- optionally use weighted least square for `--kurtdir`
- fit 3 kurtosis components
- set minimum signal to 0.01
- S0 like in original fit
- recompute eigen-values, FA, and mode
- fix angular dependence of kurtosis
- actually use the `--kurtdir` flag
- document new Amat function
- Use standard master rule eval
- Remove fslconfig dependency

## `feat5`

- Fix when to report non-parametric

## `film`

- Fix assignment to handle odd number of time points - use miscmaths pinv

## `first`

- Use MNI152-named version of mask

## `flirt`

- change python env to fslpython - changes for more interp choices, updated options/usage and modified 3D option arg.

## `FSL_MRS`

- FSL-MRS: an end-to-end tool for MRS data analysis, including data conversion, basis simulations, FID processing, fitting, quantitation, and visualisation

## `fugue`

- Added option to write out full Jacobian matrix

## `libgdc`

- RF: Changes to allow libgdc to be compiled as a self-contained shared library.

## `melodic`

- don't force varnorm flag if `--keep_meanvol` set
- Remove MELODIC from stage 4 thresholding code
- Simplify z-stat thresholding
- Stop removing intermediate zstat maps
- Remove sleep statement

## `misc_scripts`

- change python env to fslpython

## `miscmaths`

- removed use of Profiler to break dependency on utils
- Compile all with CXX11
- Made changes to Levenberg-Marquardt (levmar) so that there is now a pure Gauss-Newton option
- Change make to use C++11 and added profiler to levmar in nonlin
- If the SVD in the default pinv call fails, re-call with std method

## `miscvis`

- Revert `INC_PROB` > `INC_CPROB`, because miscvis depends on miscmaths, which has not been updated
- add fsl/sliceanimate use `CPROB` rather than `PROB`
- Migrate fslanimate/sliceanimate from avwutils into miscvis, to remove circular dependency between the two projects.

## `newimage`

- fix read error where an extension is in path
- Allow newimage cog to use all intensities or val-min intensitites

## `NewNifti`

- change byteswap from 8 to 2
- Avoid using zlib autodetect on img data blocks due to risk of false positives

## `oxford_asl`

- Replace subprocess.check_output with python2.6 friendly version for centos 6
- Allow for possibility of modifying minimum base PVE for WM/GM
- Support the new --tiimg option to pass TIs as a voxelwise map
- CHange in location of linear MNI template for FSL 6.0.4+
- Fix for FSL build
- Protect against NaN values in i2 calculation and output floats to 2 d.p.
- Allow GM and WM 'pure' thresholds to be user-specified Voxels with at least this much GM/WM are included in ROIs designed to capture GM/WM perfusion
- Split output into separate files for overall perfusion, GM and WM plus add gm and wm threshold options
- Add generation of regionwise statistics
- Constrain the Jacobian to try to avoid negative values Negatives don't make much sense geometrically and result in negative perfusion values when using voxelwise calibration
- Add support for --batim - voxelwise ATT estimates to be passed to Basil
- Boost font size - it is particularly small on Mac
- Tweak label to try to make sure update button is visible on Mac
- Improve proportion of space assigned to preview windows
- Fix disabling of IAF (tag/control) choice - no longer needed
- Move WP compatibility widget to analysis tab and make accessible by checkbox
- Allow extra widgets after a checkbox
- UI tweaks - label for grouping option and give straightfowrward choice for TC pairing options
- Use recommended method to exit app on close clicked
- Fix missing import cause error on Mac
- Keep 'running please wait' message until run completes
- Re-order links in white paper widget and make sure they are enabled/disabled correctly
- Display TI/PLD as required when casl/pasl selected
- Fix - make asl_file do subtraction where required
- Update options after GUI completely set up
- Add Layout() call to make sure number lists are displayed correctly at startup
- Update GUI after calib enabled as it may be done by the WP widget
- Remove debug message
- Add exchange and dispersion options
- Rationalize layout to work better with white paper widget and run button
- Fix white paper mode to control T1 and BAT
- Updated
- Fix for change to main() function in gui
- Include all bitmaps
- Clarify use of matplotlib
- Add other bitmaps used in gui
- Switch fsleyes for matplotlib
- Add Conda build recipe - potentially better route for FSL install
- Add FSLeyes as preview widget plus various bugfixes and add icon
- Merge branch 'master' into newgui
- Remove autogenerated file
- Updated
- First attempt at new gui
- Fix python install - now versioned in setup.py
- Issue #8: Fix typo: asl2struct rather than asl2struc THis would affect output of the asl->std transformation in the case where the struc->std transform was supplied as a matrix (normally it's a nonlinear warp which is not affected by this)
- Add support for variable repeats and reformat as a standard Python module In the future we may want to pip install this or make a conda recipe
- Issue #8: Handle case where FSLOUTPUTTYPE is set to NIFTI rather than NIFTI_GZ THis is only really a workaround - the longer term solution is to move to fslpy which handles all this transparently
- Use informative noise prior if there are 5 of fewer timepoints Previously this was enabled by default only if you had a single timepoint but it causes an issue for data with small numbers of timepoints generally so relax this a bit.
- Need to pass repeats string to asl_file when re-ordering ASL data after distortion correction
- Fix bug preventing re-generation of mask during PVC
- Output and calibrate WM variance in partial volume runs

## `post_install`

- update miniconda URL
- Added FSL_MRS 1.0.3
- Added TIRL 2.1.2b1
- FSleyes+fslpy to latest
- Copy, don't sym-link, .app bundles, to avoid macOS .app caching errors. Drop fslview.app/assistant.app - now only FSLeyes.app is placed into /Applications/.
- eddy_qc to 1.0.3
- Add channels to fslpython env, use verbose flag when creating environment
- explicitly list pip
- Change channel priority to conda-forge > defaults > FSL. Use strict channel priority to enforce this.
- Bump up retry backoff factor - can't hurt
- update configure_eddy.sh to also check for installed cuda in /usr/local/cuda
- Tell curl to follow redirects
- Make fslpython install errors more prominent

## `ptx2`

- Added Nearest neighbour interpolation
- Use standard master rule eval
- Remove fslconfig dependency

## `siena`

- Use MNI152-named version of template
- Make consistent indenting (remove all tabs)
- Fix usage for -m option, which incorrectly references *stdmask

## `TIRL`

- Tensor Image Registration Library v2.1 (beta): a Python-based general-purpose image registration framework with prebaked scripts for registering stand-alone histological sections to volumetric MRI data.

## `topup`

- Fixed checking bounds for MoveMatrix and MovePar
- Fixed check for indexing past last volume

## `utils`

- CXX11 Changes for Centos6
- Added some includes needed for build on CentOS
- Added initialisation of class static variables
- Added FSLProfiler class

## `verbena`

- Add log CBF option
- Add `--allow-bad-voxels.` The DSC model is prone to numerical instability and this can help allow a run to complete even with some non-fitting voxels (e.g. when the mask includes nonbrain voxels)
- Removed theory page - now hosted in FABBER_DSC docs
- Reference theory section from FABBER_DSC documentation

## `warpfns`

- Added functionality to allow fnirtfileutils to write full jacobian matrix

## `xtract`

- Update Makefile
- changes to readme
- Updated xtract_data location to FSLDIR/data
- Added xtract_stats
- Added reference space tractography functionality
- Added `-list` flag to show tract names. Added option to select a subset of the pre-defined tracts from either Human or Macaque - user provides structure file with tract names or tract names and nsamples. Small bug fixes
- Changed order of `fsl_sub` `-T` and `-q` args to overcome `fsl_sub` queue selection issue
- Updated to reflect xtract_viewer changes
- Added species flag
- Added CPU time allocation of 1.5 days

## `xtract_data`

- `$FSLDIR/data/xtract_data`
- Update README
- Small changes to ILF and FX protocols to remove false positives
- Removed PTR from Macaque protocols

## `zlib`

- add new CFLAGS
- use `--static` for all builds now
- add latest non-gpl cloudflare zlib

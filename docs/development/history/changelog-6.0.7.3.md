# FSL 6.0.7.3, 5th September 2023

## fsleyes 1.8.1 -> 1.8.3

- New `--cmapCycle` command-line option, which automatically assigns a different colour map to each volume overlay (!402).
- Fixed an issue with overlays being interleaved when overlapping slices in the lightbox view.
- Fixed some bugs in edit mode - crashes could occur when drawing/selecting
voxels in highly anisotropic images, and the cursor size could vary for
images with floating point imprecision in their pixdims.
- Fixed an issue with loading built-in plugins - the atlas panel (amongst others) was not being loaded.


### Detailed history

-  CI: Adjust pipeline rules - test 3.10 on MRs, test all pyvers on release branches
-  BF: Fix a couple of subtle bugs a) anisotropic voxels, and b) images with floating point drift in their pixdims (e.g. nifti2)
-  BF: fix log statement - display is no longer defined here
-  TEST: regression test for slice overlap issue
-  BF: Fix display artifact in lightbox view when overlapping slices, and drawing more than one overlay - the slices/overlays were being interleaved according to draw order. The only work around is to draw in order of slices, so that each slice completely overlaps previously drawn slices.
-  DOC: install page updates
-  CI: fslpy default branch is now called main
-  TEST: Test --cmapCycle
-  CI: Default branch renamed to main
-  ENH: New --cmapCycle option to automatically assign colour maps
-  DOC: Fix super script formatting
-  MNT: Add log statement back in
-  TEST: Regression test for loading builtin plugins in submodules
-  DOC: Explain atlas panel value discrepancy


## fslpy 3.13.3 -> 3.14.1

- BF: Accidentally removed tbss from fsl.wrappers namespace
- *Mnt/doc*

  Fix sphinx formatting/build issues

- *ENH: New option to compose function*

  allowing order of scale/origin translate to be changed, with the effect that the position of the origin will not be changed by the scaling parameters

- MNT: Allow Notifier callback functions to accept no arguments
- *ENH,MNT: Automatically set sform for FNIRT displacement fields which do not have their sform set. Replace setup.py with pyproject.toml*

  * Automatically set sform for FNIRT displacement fields which do not have their sform set.
  * Replace setup.py with pyproject.toml
  * Move `tests` into `fsl/tests`

- *Enh/flip*

  New `fsl.transform.affine.flip` function, for applying a flip/inversion to the axes of an affine transformation.

- *CI: id_doc_deploy no longer used*

  Documentation publishing is now via gitlab pages, and not via SSH key-based login to a remote host.

- MNT: Adjust `immv`/`imcp` so that they support `FSLOUTPUTTYPE=ANALYZE`
- MNT: Remove some obsolete warning suppression logic, and suppress nibabel warning when converting to NIFTI2
- REL: 3.14.0
- REL: 3.14.0
- REL: 3.14.0
- REL: 3.14.0
- REL: 3.14.1

### Detailed history

- BF: Accidentally removed tbss from fsl.wrappers namespace
- DOC: Sphinx fixes
- DOC: Fix references, mock dill  in doc build environment
- DOC: Changelog
- ENH: New option to compose function allowing order of scale/origin translate to be changed, with the effect that the position of the origin will not be changed by the scaling parameters
- CI: Allow tests to be skipped via SKIP_TESTS env var
- TEST: Test origin preservation
- DOC: Changelog, apidocs
- MNT: Notifier callback functions can now be no-args
- DOC: Changelog
- TEST: Test noargs callback function
- MNT: Automatically set sform for FNIRT displacement fields which do not have their sform set
- MNT: Replace setup.py with pyproject.toml
- CI: update doc/dist build commands, remove some cruft
- MNT: Remove coveragerc
- CI: id_git no longer exists
- MNT: no more setup.py
- MNT: pass args directly to pytest, another setup.py ref
- TEST: Move tests into fsl package
- CI: update test paths
- MNT: Move conftest to root. Don't include tests in built wheels
- TEST: More updated test paths
- TEST: Change pytest import mode so that pytest does not mess with sys.path
- MNT: wrong path
- CI: sphinx-build, not sphinx build
- MNT: Wrong path again
- BF: Did not mean to add this
- DOC: Changelog
- CI: rename default branch to main
- MNT: Use atlasquery not atlasq in cli help
- ENH: New afffine.flip function; make scaleOffsetXform args optional
- DOC: Update readme r.e. move to pyproject.toml
- DOC: Changelog
- MNT: Don't apply boundary offset
- TEST: Basic teSt for affine.flip
- CI: id_doc_deploy no longer used
- MNT: change to f-strings
- MNT: Add file type enumeration to Image class, and fileType function which infers type of a file. Adjust immv/imcp so that they support FSLOUTPUTTYPE=ANALYZE
- TEST: Don't set FSLOUTPUTTYPE in env
- TEST: Make sure that immv/imcp performs file type conversions correctly
- DOC: Changelog
- DOC: CHangelog
- MNT: Remove some obsolete warning suppression. Suppress nibabel warning when converting to NIFTI2
- DOC: additional entry in changelog regarding immv/imcp
- MNT: Version
- CI: Wrong venv path
- CI: Wrong docker image for doc build
- CI: fix images/commands used for pypi dist build job
- BF: Image setitem notifier was returning the un-normalised slice object, instead of the normalised one (with trailing dimensions of length 1 removed)
- DOC: Changelog
- TEST: Regression test for __setitem__ callback value
- CI: Don't run tests on upstream/main, as they will have already been run on the MR branch
- MNT: Post-release version bump
- CI: allow tests to be skipped via commit message [skip-tests]

## fsl-avwutils 2209.1 -> 2209.2

- *RF: Change `fslchfiletype` to rely  on `imcp` doing the type conversion*

  In FSL 5.0.11, the `imcp` and `immv` commands were updated to honour the `$FSLOUTPUTTYPE` environment variable and to potentially convert image files, rather than just copying them.

  Unfortunately at that time we failed to notice that `fslchfiletype` relied on `immv` performing a naive copy, and so since then `fslchfiletype` has not worked correctly in some circumstances.

  This MR updates `fslchfiletype` so that it performs the file conversion by setting `$FSLOUTPUTTYPE` and then calling `imcp`. A small suite of unit tests are also included, to validate the behaviour of `fslchfiletype`.

  The binary `fslchfiletype.cc` executable (installed to `fslchfiletype_exe`) as been removed, but a copy of the `fslchfiletype` script is installed to `fslchfiletype_exe` in case anybody was directly calling the binary.


### Detailed history

- RF: Change fslchfiletype to rely  on immv doing the type conversion
- MNT,TEST: Remove fslchfiletype binary as it is no longer used. Add fslchfiletype unit tests
- TEST: Add additional tests to check fslchpixdim copy/inplace semantics
- TEST: enumerate over file types for copy/inplace semantics tests

## fsl-installer 3.5.5 -> 3.5.6

- BF: Context.admin_password was returning None on first call

### Detailed history

- BF: admin_password was returning None on first call
- DOC: changelog
- MNT: replace setup.py with pyproject.toml
- CI: Rename default branch to main
- RF: Make Context.admin_password trigger evalution of need_admin. Make sure destdir is abspathed when specified at Context creation
- TEST: fix dir in pyproject, add context test
- TEST: Mark test which will fail if run by root user
- CI: Run noroottests as non-root user

## fsl-newimage 2203.10 -> 2203.11

- *BF: Fix NIfTI file format identifier names*

  It seems that file format identifier names were inadvertently changed in a search+replace mishap. This would have affected uses of the `fslchfiletype` command, with the `NIFTI_PAIR`, `NIFTI_PAIR_GZ`,  `NIFTI2_PAIR_GZ`, or  `NIFTI2_PAIR_GZ` identifiers.


### Detailed history

- BF: It seems that file format identifier names were inadvertently changed in a search+replace mishap
- TEST: Regression test for file formats

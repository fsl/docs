# FSL 6.0.7.9, 13th March 2024

## fslpy 3.18.0 -> 3.18.1


### Detailed history

- BF: Use a unique identifier for the current thread of execution, otherwise collisions occur when running concurrent wrapper functions
- TEST: Test concurrent wrapper function execution
- TEST: Awkward unit test for fot collision. Not particularly robust

## fsl-first 2203.0 -> 2203.1

- Doc/cli mistakes
- BF: Fix conflicting long-form cli options
- *ENH: New `first_utils --saveAverageMesh` option, which outputs the average mesh as a VTK file*

  Having a surface version of the group average mesh allows for better visualisation of results.


### Detailed history

- DOC: Fix errors in first cli option help text
- STY: consistent whitespace/indentation
- BF: Fix conflicting long-form cli options
- STY: Normalise indentation
- ENH: New `first_utils --saveAverageMesh` option, which outputs the average mesh as a VTK file

## fsl-pyfix 0.6.0 -> 0.7.0

- *ENH: New pre-trained model for Cynomolgus Macaque, provided by Takuya Hayashix*

  - New pre-trained `NHP_HCP_MacaqueCyno` model, for NHP-HCP BOLD fMRI of *Macaca fascicularis*, provided by Takuya Hayashi. The associated mask images can be used when performing feature extraction by specifying `--species macaque_cyno`.
  - Added the macaque mask images used with the `NHP_HCP_Macaque` model, as they had been accidentally omitted in the previous two versions.
  - Built-in models can now be referred to by their alias (as specified in `pyfix/resources/models/info.json`), or their file name prefix.


### Detailed history

- MNT: Update dependencies (should have been in last release)
- CI: xgboost/optuna in dev image
- BF: Fix mask packaging
- ENH: New NHP HCP macaque_cyno mask images, provided by Takuya Hayashi
- BF: Fix to log message
- ENH,BF: New cynomolgus macaque model, provided by Takuya Hayashi. Fix typo in info.json
- RF: Accept either alias or model file name prefix for built-in models
- TEST: Sanity check test for builtin models
- DOC: Changelog, note about new model
- BF: Use species ID saved in model file when running main fix
- TEST: Test that species ID saved in model file is used

## fsl-xtract 2.0.2 -> 2.0.3


## 6.0.6.1, 5th December 2022

### fsl-base            2209.0 -->  2212.0
- BF: don't add "None" to candidate list
- DOC: Changelog
- RF: Search for mamba/conda on $PATH, fall back to conda shell function if can't find
- DOC: Changelog
- MNT: Prefer mamba over conda if it is available
- DOC: Changelog
- BF: Use conda platform identifiers, not FSL platform identifiers
- TEST: update fsl-installer version used in test (old versions have been removed from channel)
- DOC: changelog
- BF: Should not be pinging defaults channel

### fsl-gps             2111.0 -->  2211.0
- MNT: qualify string namespace
- ENH: Multishell
- Major rewrite to enable simultaneous optimisation of multiple shells

### fsl-installer       2.1.0 -->   3.0.0
- TEST: fix test
- DOC: changelog
- ENH: Ability to provide custom progress reporting function to Process.monitor_progress, as an alternative to reading #lines on stdout. Monitor number of files in $FSLDIR/pkgs/, instead of #lines of stdout, to report progress on main FSL installation, as mamba does not print package download progress to stdout.
- RF: Make installer compatible with non-mamba envs
- MNT: abstract out conda, so we can switch between mamba/conda
- MNT: Fix progress endline; don't log tput calls
- TEST: update installer tests (no longer have a "conda install" command)
- MNT: Revert to single-step installation method
- MNT: make file for FSL installs
- MNT: version
- DOC: changelog
- MNT: Insert insertcopyright hooks

### fsl_mrs             2.0.8 -->   2.0.9
- Fix inverted peak QC issues.
- Added normalisation to the segmentation scripst, all voxels should now sum to 1.

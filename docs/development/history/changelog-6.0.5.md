# FSL 6.0.5, 20th July 2021

## `asl_mfree`

- Added README and LICENSE for completeness

## `avwutils`

- RF: Migrate the distancemap tool from tbss to avwutils
- DOC: Update usage
- BF: fslcreatehd was not populating intent fields
- RF: More precise num-args check
- RF: Remove use of no-longer-existing fslcreatehd -r option
- RF: fslcreatehd: set number of dimensions sensibly
- STY: Fix indentation

## `basil`

- Ensure that the T1 parameter is included in the model if --t1im is specified, even if --infert1 is not --infert1 will allow the T1 image to vary, otherwise it is treated as ground truth. This is handled by manipulating the precision on the prior within the model
- Remove old debug message

## `bet2`

- RF: Make d parameters hidden for now
- MNT: Comment regarding d1/d2 type
- MNT: int64_t -> double to match current bet2 results
- ENH: Improved 2D "mode"

## `bianca`

- MNT: make sure `bianca_perivent_deep` gets installed to `$FSLDIR/bin`
- Handle UTF-8 files with BOM
- Added script `bianca_perivent_deep` for subclassification of pvent and deep lesions
- replaced deprecated `get_data` with `get_fdata`

## `config`

- Added 10.2 to eddy CUDA build
- RF: Allow supportedGencodes.sh script to be overridden by FSLCONFDIR
- MNT: Remove 11.2 target for Centos 7 compat
- Added JIT build also to default branch
- MNT: Added CUDA SDK 11.2 to list of builds for eddy
- Added CUDA versions 11.0-11.2. Also added a JIT build to ensure forwards compatibility of any executables.
- Added support for CUDA 10.1

## `data_atlases`

- ENH: Update from 28/12/2012 release

## `data_atlases_xtract`

- SW macaque atlas update

## `distbase`

- MNT: Build the fabber pet module

## `eddy`

- Removed openmp parallellisation from write_jac_registered_images
- Fixed bug that made CPU-slice-to-vol crash when no topup was provided
- Uncommented openmp pragma in write_jac_registered_images
- Changed the way the EC and susc fields are combined for the scan2model transforms.
- More debugging and refactoring related to slice-to-vol for CPU
- Ongoing debugging of slice2vol for CPU. Changed how interpolation parameters were passed to GPU branch of slice2vol
- Ongoing debugging of slice2vol for CPU
- Added EddyTry/EddyCatch to a number of functions where they were missing
- Removed check that disallowed slice-to-vol for the CPU version
- All options for slice-to-volume resampling implemented. First version that compiles
- CPU version now able to calculate mixed derivatives for slice-to-volume
- Fixed a problem with debug output in the CPU build
- ENH: Laxer check for sameness of voxel sizes. As as new criteria in newimage.
- Added routines to write out outliers only. Potentially useful for debugging.
- Made sure that predictions are correctly scaled when written
- Made sure that cublas_v2.h was included before anything else
- Added files with low level CUDA utility functions
- Changes that allows me to write out predictions in scan space
- Better error reporting for CUDA/cuBLAS errors.
- Changes to GetSuscHzOffResField for when estimating move-by-susc without a static field
- Added more checks in InitGpu to provide more informative error messages
- Added option to specify compute capability to Makefile

## `etc`

- MNT: Update version to 6.0.5

## `fabber_core`

- Rationalize spatial prior calculation to avoid multiplying and later dividing by 8
- Add newlines at end of files to remove compiler warning
- Only log mean value on first load Otherwise we get this reported for every voxel when using an image prior since the image prior now informs the initial posterior
- Add undocumented expert-option to tweak prior on spatial precisions
- Switch to Apache2 license
- Issue #6: Make initial posterior value equal to voxelwise value when we have an image prior
- Minor clarity to comment
- Correct references to old web site
- Fix includes for new FSL build system
- Fix warning about multi-timepoint data. The main data is generally 4D so we should open it as 4D even when we just want the 3D extent

## `fabber_models_asl`

- added copy right text
- Modified CBF equation
- updated default T2 blood values
- Corrected some error. Added TE correction
- added some comments
- VS ASL
- Switch to Apache2 license
- Update README.md
- Minor logfile fix
- Fix includes for new FSL build system
- Add option for WM partition coefficient rather than purely hardcoded

## `fabber_models_cest`

- Add options for custom water T1/T2 for CESTR* calculation only This is independent of the priors in the poolmat which will be used for CESTR* as well if these options are not given
- Switch to Apache2 license
- Fix includes for new FSL build system

## `fabber_models_dce`

- Switch to Apache2 license
- Fix includes for new FSL build system
- Rationalization of priors so they are consistent and sensible in all models
- Updated docs

## `fabber_models_dsc`

- Switch to Apache2 license
- Fix includes for new FSL build system

## `fabber_models_dualecho`

- Switch to Apache2 license
- Fix includes for new FSL build system

## `fabber_models_dwi`

- Switch to Apache2 license
- Fix includes for new FSL build system

## `fabber_models_qbold`

- Switch to Apache2 license
- Updated to work with new FSL build system

## `fabber_models_t1`

- Switch to Apache2 license
- Fix includes for new FSL build system

## `fdt`

- BF: move wildcard out of quotes
- STY: Remove all tabs, fix indentation
- BF: Fix improperly quoted command
- RF: Concatenate arrays with += syntax
- BF: Correctly concatenate array options
- Fix compilation issue with limits.h
- Switch to bash arrays for arguments
- Convert to using arrays for command building Quote as many variables likely to hold paths as possible Don't execute output of `fsl_sub --has_coprocessor='cuda'`
- Modern command capture Call GPU version earlier to ensure arguments not dequoted
- Quote variables
- White space clean up
- Check for GPU hardware or queues - error if none found
- Add time to single_slice jobs Make job times configurable by variables
- Ensure `fsl_sub` called as `$FSLDIR/bin/fsl_sub`
- Error messages should go to stderr
- Use -n not ! -z
- Make sure `fsl_sub` is called as `$FSLDIR/bin/fsl_sub`
- Ask `fsl_sub` if it has a cluster
- Ask `fsl_sub` whether it has a cluster Ask `fsl_sub` whether it has CUDA Get fsl_sub to delete jobs
- More rm quoting
- Don't mail the user
- Remove hard coded queue name
- Make rm safer
- Switch to new `fsl_sub`

## `feat5`

- BF: Fix for overflow bug

## `flirt`

- Switch to simpler imcp use
- set tdim on outpout vol to match flirt.cc applyxfm

## `fslsurface`

- Merge branch 'mnt/libprob' into 'master'
- MNT: Include cprob/libprob.h instead of including libprob.h directly
- Merge branch 'mnt/remove-opengl' into 'master'
- MNT: Remove friend refs to fslsurfacegl
- MNT: Remove obsolete junk from makefile
- MNT: Remove unused/obsolete structs which use GL types, and references to GL
- STY: Consistent indentation
- Merge branch 'bf/readGIFTI' into 'master'
- BF: Explicit instantiation of readGIFTI is required, otherwise implicit instantiations may be inlined during optimisation
- STY: whitespace

## `fugue`

- BF: Make sure convertwarp correctly sets the sform/qform of the output displacement field according to the provided reference image
- STY: whitespace

## `lesions`

- Revert "add lesion split program. it splits a binary lesion mask into multiple lesions if multiple disconnected commonents exist"
- add lesion split program. it splits a binary lesion mask into multiple lesions if multiple disconnected commonents exist
- make white matter mask optional

## `melodic`

- RF: in>0 not valid for modern compilers

## `meshclass`

- MNT: Removed irregular symbol from comment
- ENH: Added anisotropic rescale

## `misc_c`

- TEST: Resolve sym-links
- MNT: Remove C version of tmpnam
- TEST: pyfeeds unit tests for tmpnam
- RF: Port tmpnam to python
- DOC: Logic for tmpnam
- BF: Fix and streamline tmpnam/TMPDIR changes
- RF: Hopefully simplify logic
- use TMPDIR if set

## `misc_tcl`

- Remove SGE hard dependency Update usage comment to reflect that this isn't SGE only now

## `miscmaths`

- MNT: Include cprob/libprob.h instead of including libprob.h directly

## `mist`

- Removed hardcoded queue names

## `newimage`

- MNT: invalidateSplines is more grammatical
- MNT: rename negateSpline
- MNT: Missing semicolons
- BF: Invalidate splines on non-const dereferences
- ENH: Revised samedim logic
- BF: Explicit instantiations of readGeneralVolume are required, as implicit instantiations may be removed/inlined by optimisation
- ENH: Extensions are now a basic property ( for copying )

## `NewNifti`

- RF: Whitespace
- RF: Simplified after re-read of standard wording
- MNT: Renamed member
- MNT: datumByteWidth needs to be const
- RF: Renamed method
- ENH: more robust determination of data-width

## `oxford_asl`

- Move generation of WM tissue segmentation to before first registration in case we are doing BBR for reg 1
- Remove test for registration before trying to copy registration QC files This will mean warnings in the log if these files don't exist but it seems in UKB that the test is not working correctly
- Move calculation of bias field/sensitivity map in ASL space until after registration This fixes a bug caused by reordering of registration steps because we can't put the sensitivity map in ASL space until we have the struc2asl registration. Also minor log re-ordering for cosmetic reasons
- Add option to save region analysis ROIs/masks
- Naming - refer to 'cerbral WM' not 'cortial WM'
- Allow multiple custom MNI atlases
- Add option to save structural space ROIs and handle case where don't have both GM and WM PVE maps
- Add cortical GM/WM ROIs to region analysis script
- Change MNI atlas option to add regions to standard atlas rather than replacing them
- Add automatic masking of subcortical regions in GM/WM ROIs and generate separate stats for these ROIs
- Fix typo in capturing output of initial registration
- Make sure QC output is BBR registration in case where we use BBR in initial
- Add option to use BBR on initial registration This also involves reordering the segmentation to be before the registration
- Ignore voxels where variance is zero This occurs typically as a failure of extrapolation of the calibration image in voxelwise calibration. Normally the perfusion is also zero which means the averages will be biased and the precision-weighted mean will be zero or close to it.
- Increase degree of dilation to cope with poor initial registration
- Protect against non-existant GDC warp file THis is a bit random but is useful in UKB as the script which generates the warp does not always seem to work! And in the end a sanity check does no harm here
- Clarify log message to indicate the source of the structural data if not FSL_ANAT
- QC output - include motion parameters and simple plots plus calibration image in structural space Also fix accidental botching of some of the other registration QC output in previous commit.
- Merge stdout and logfile stream into a single log which is sent to both. Previously some messages went to stdout and some to the logfile. I will obviously miss the fun of trying to figure out how these message interleave in real time, and of having to ask users to provide both the stdout and the logfile, but probably not that much.
- Provide option for explicit specification of the registration reference image
- Warn if Basil mask did not include all the voxels in the final analysis mask
- Support the use of a dilated mask for BASIL analysis. Final output now all uses the same mask which is that derived from the final registration. Previously PVC outputs used this mask but non-PVC outputs used the original mask
- When generating ROI stats from a PVC run, use the same mask for the stats as used for the analysis On a PVC run, the mask is regenerated from the improved final registration and that is what's used for the PVC Basil run. So when we are generating statistics from these outputs we should use the same mask as used in basil
- Add option to use a label image as an ROI atlas The region analysis script has the ability to use multiple atlases but for simplicity we restrict oxford_asl to the ability to use either the FSL standard atlas or a single label image atlas
- Fix problem identified in UKB where incorrect Python is used when FSLDEVDIR is not set This occurs because on some systems /bin/python exists. To avoid using this, check to see if FSLDEVDIR exists before using it.
- Fix comparison to work with POSIX-standard bc Previous code relied on GNU extensions to allow evaluation of a boolean expression directly
- Added --qc-output and initial set of additional outputs for quality control
- Switch to Apache2 license
- Prevent pipeline failing when fieldmap warp contains NaN values THis can happen occasionally but typically only outside the ASL image area
- When fslpython does not exist (e.g. in UKB) attempt to fall back on regular python This will only work if the user has the required libraries installed, i.e. fslpy, so we generate a warning in this case
- Make sure ROIs are masked with the brain mask so we don't accidentally include any voxels which have not been fitted by basil
- Support region analysis without --fslanat provided we have struc->std warp and struc image
- Handle version failure better for cases where we are not in a git repo
- Document --fa option
- Merge branch 'master' of https://github.com/ibme-qubic/oxford_asl
- Add artonly option as per basil
- Merge pull request #11 from hanayik/master
- update call for latest FSLeyes embed

## `post_install`

- MNT: FSLeyes to latest
- BF: fsl_sub script handling
- CI: work if TMPDIR is unset
- MNT: Minor updates to fsleyes, fslpy. New version of fsl_add_modue. Initial release of fsl_get_standard.
- MNT: Stick with wxpy 4.0.7
- CI: Use shell runner - job was being dispatched to macOS docker runner, and hence running in linux
- CI: File tests use single bracket operator in bash
- MNT: Update expected #lines for progress update
- MNT: Passing the -v/--verbose flag actually suppressees the download progress output, so it's better to call without -v for the sake of a more useful progress update.
- CI: Run test script with bash
- CI: Wasn't finding log file on macos
- MNT: Add nilearn (#5)
- MNT: Bump FSLeyes to latest
- Added fsl_sub sge for internal branch only
- Added fsl_sub
- MNT: Update various packages to resolve dependecy issues
- MNT: Update scipy for fsl_mrs
- ENH: Latest fsl_mrs from WC
- make eddy setup less chatty
- change application install location if not root
- TEST: bug
- TEST: typo
- RF,BF: Added paramiko+pyyaml back in, fixed TIRL package name, use nidmresults conda packages instead of installing via pip
- TEST: Test fslpython_install on macOS, adjust to run in either shell or docker runnesr
- RF: Remove implicit "non-sciency" dependencies. Bump Python to 3.8. Bump all core dependencies to latest available.
- BF: fix path
- BF: fix path
- CI: need libgl to install fslpython env
- CI: dump install log
- CI: install basic utils
- CI: Test fslpython installation on new commits
- MNT: bump funpack to latest, add fsl-add_module
- MNT: fslpy and funpack to latest
- MNT: fsl_mrs to 1.0.5
- MNT: Remove tabs
- RF: Only support linking to files
- DOC: typo
- DOC: fixed some typos in comments
- R: Change link/unlink scripts so they create wrapper scripts instead of symlinks

## `ptx2`

- RF: More explicit down-casts, to avoid errors on strict compilers
- RF: Explicitly down-cast from double to float, to avoid errors on strict compilers
- STY: Consistent indentation
- MNT: include cprob/libprob.h rather than libprob.h
- RF: Disambiguate round

## `qboot`

- BF: reference persistence not guaranteed
- MNT: include cprob/libprob.h rather than libprob.h
- Update for new fsl_sub

## `randomise`

- BF: Existing quoting prevented wildcard expansion
- Update for new fsl_sub
- OPT: Quote for safety
- More BASHification
- Modernise and convert to BASH
- BF: Removed debug text from --skipTo setup

## `tbss`

- Merge branch 'rf/move-distancemap' into 'master'
- RF: Remove distancemap * it is being moved to avwutils

## `verbena`

- Handle case where FSLDEVDIR is not set - do not add /bin the the path ahead of everything else!
- Expose options to save model fit and residue function

## `warpfns`

- Cleaned up debug code
- Only reinitialise in `apply_warp` if size of input mask is wrong
- Debug version
- Printouts so I can see if the right sources are used
- Changes logic in `raw_general_transform` to avoid `&=` construct

## `xtract`

- Merge branch 'xtract_blueprint' into 'master'
- Xtract blueprint
- Merge branch 'wayorder' into 'master'
- Added wayorder option to allow for specific target ordering
- Merge branch 'bug-allow-noninteger' into 'master'
- Bug allow noninteger
- Merge branch 'master' into 'master'
- Quote strings with variables within
- Add support for new fsl_sub
- Merge branch 'CUSTOMspec' into 'master'
- Added -species CUSTOM option to allow tractography in any species

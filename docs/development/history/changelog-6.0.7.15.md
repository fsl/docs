# FSL 6.0.7.15, 4th November 2024

- [**fsl_mrs** 2.2.0 -> 2.3.1](#fsl_mrs)

## <a id="fsl_mrs" href="#fsl_mrs">fsl_mrs 2.2.0 -> 2.3.1</a>

- [*ENH: P-Spline baseline*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/113)
  Add a penalised spline baseline option
- [*ENH: dask driven MRSI parallel processing*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/114)
  Move to Dask for parallel processing of individual voxels in MRSI fitting.
- [*BF: Coilcombine with singleton ref*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/115)
  - Fix bug in coil combination with trailing singleton dimensions
  - Fix bug for numpy versions < 2 calculating quantifiaction areas
- [*BF: Align reports with dim=all*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/116)
  Fix reporting bug in align with dim=all
- [*REL:2.3.1*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/117)
  - Increment to python 3.12 and scipy >1.13
  - Python testing and validation versions incremented to 3.12
- [*MNT:spec2nii version*](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs/-/merge_requests/118)
  require versions of spec2nii (>=0.8.5) with more flexible scipy req

**Detailed history**

- `ENH: P-Spline baseline`
- `ENH: dask driven MRSI parallel processing`
- `BF: Coilcombine with singleton ref`
- `BF: Align reports with dim=all`
- `REL:2.3.1`
- `MNT:spec2nii version`

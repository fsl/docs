# Support, tutorials & training

For anyone new to FSL we recommend working through the relevant parts of the FSL course tutorial/practical exercises.


## FSL Course

The [FSL course](https://open.win.ox.ac.uk/pages/fslcourse/website/) contains data and self-paced practical/tutorial exercises that are an excellent way to learn how to use FSL. In addition, an intensive, hands-on course is run approximately every year; follow the link for more information on the course as well as lecture slides, practical instructions and data for downloading.


## FSL mailing list

The primary means of contacting the FSL development team is via the [FSL mailing list](https://www.jiscmail.ac.uk/cgi-bin/webadmin?A0=FSL). This list is monitored by the FSL developers, along with a large number of FSL users from all over the world.

Please join this list for all email support regarding FSL - sign up specifically to the FSL list on the [JISCMail website](https://www.jiscmail.ac.uk/cgi-bin/webadmin?A0=FSL), then follow instructions in an email sent by jiscmail to verify your regsitration.

Before sending an email, try [searching the list archives](https://www.jiscmail.ac.uk/cgi-bin/wa-jisc.exe?REPORT&z=4&1=FSL&L=FSL) to see if you can find a solution to your problem.


### How to ask a question on the FSL mailing list?

In order to understand and analyse problems please provide the following pieces of information:

- **What version of FSL are you using?** You can find this out by running `fslversion` in a terminal.

- **What part of FSL did not work as expected?** In the case of FEAT / MELODIC problems, please check the log files (`<my_analysis>.feat/logs/`) files first - common errors are that you might be out of disk space, or certain files might have the wrong permissions.

- **What commands were you trying to run?** If you are calling FSL commands in the terminal, include the specific commands that you were trying to run in your email. Be as specific as possible about any command line error messages that were printed to the terminal.

- **What type of input data were you running the software on?** Please make sure that the data is not corrupted: check your input data by loading it into FSLeyes. If your input data is not what you expect, try and fix this first. You can also obtain useful header information with `fslhd <filename>` and useful information about the image intensity range from `fslstats <filename> -r -R`. If you need to report a problem, please provide this information in an email. If it is possible to do so, share your data online (for example with Dropbox or Google Drive), so we can try and reproduce the problem ourselves.


## Technical reports and publications

Detailed technical reports on various research topics undertaken at FMRIB can be found at https://www.fmrib.ox.ac.uk/analysis/techrep/. A list of conference and journal papers produced by the FMRIB Analysis Group can be found at https://www.fmrib.ox.ac.uk/analysis/fmribindex/.


## Commercial licensing

Use for which any financial return is received shall be defined as commercial use (see the [license](license.md) for details). If you are interested in using the Software commercially, please contact Oxford University Innovation ("OUI") by sending an email to fsl@innovation.ox.ac.uk.

#!/usr/bin/env python
#
# Scans through all *.md files, and checks that all cross-references
# are valid.

import            glob
import os.path as op
import            re
import            string
import            sys

from dataclasses import dataclass

from typing import List


@dataclass
class File:
    """A markdown file which contains
      - Sections (headers, e.g. "# User guide")
      - Links (e.g. "[check out FLIRT](registration/flirt/index.md)")
    """
    filename : str
    sections : List[str]
    links    : List['Link']


@dataclass
class LinkTarget:
    """Target of a markdown link - represents the "target" in a link
    "[text](target)".  The target comprises a target file, and optionally a
    section within that file.
    """

    # URL or file path
    url     : str
    section : str = None

    def __str__(self):
        if self.section is not None: return f'{self.url}#{self.section}'
        else:                        return self.url

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))

    @property
    def external(self):
        return LinkTarget.is_external(self.url)

    @staticmethod
    def is_external(url):
        if url.startswith('http'):    return True
        if url.startswith('mailto:'): return True
        if url.startswith('ftp'):     return True
        return False


    @staticmethod
    def create(filename, text):

        # external URL
        if LinkTarget.is_external(text):
            return LinkTarget(text)

        # doscify includes are relative to the current file
        isrel = any(("':include'" in text,
                     '":include"' in text))

        text = text\
            .replace('":ignore"',  '')\
            .replace("':ignore'",  '')\
            .replace("':include'", '')\
            .replace('":include"', '')\
            .strip()

        parts = text.split('#', maxsplit=1)
        parts = [p.strip() for p in parts]
        parts = [p         for p in parts if p != '']

        if len(parts) == 1:

            # link to a section in this file
            if text.startswith('#'):
                destfile = filename
                section  = parts[0]

            # link to another file
            else:
                destfile = parts[0]
                section  = None

        # link to a section in another file
        elif len(parts) == 2:
            destfile, section = parts

        # multiples hashes, I'm confused
        else:
            raise ValueError(f'Invalid link? {filename} : {text}')

        destfile = destfile.lstrip('/')
        if destfile == '':
            destfile = 'README.md'

        if isrel:
            destfile = op.join(op.dirname(filename), destfile)

        return LinkTarget(destfile, section)


@dataclass
class Link:
    """A markdown link, e.g. "[text](target)". For image files
    (e.g. "![image](image.png)", the image attribute is set to True.
    """
    filename : str
    text     : str
    target   : LinkTarget
    lineno   : int
    image    : bool = False

    def __str__(self):
        return f'Link({self.filename}#{self.lineno}): [{self.text}]({self.target}) '

    def __repr__(self):
        return str(self)


def build_file_index(docdir):
    index     = {}
    filenames = list(glob.glob(op.join(docdir, '**', '*.md'), recursive=True))

    for filename in filenames:
        sections = read_sections_in_file(filename)
        links    = read_links_in_file(filename, docdir)
        filename = op.relpath(filename, docdir)

        index[filename] = File(filename, sections, links)

    return index


def read_sections_in_file(filename, titles=False):
    sections = []
    with open(filename, 'rt') as f:
        content = f.read()

    allsections = []

    # extract all sections
    for line in content.split('\n'):

        line = line.strip()

        if (not line.startswith('#')) and ('id="' not in line):
            continue

        # <a id="some-section">
        if 'id="' in line:
            allsections.extend(re.findall(r'id="(.+?)"', line))
        # markdown header
        else:
            allsections.append(line.lstrip('# '))

    if titles:
        return allsections


    # slugify. This is insanity.
    for section in allsections:

        section = section.lower()

        sanitised = []

        # strip any links contained in section header
        def rep(m):
            return m.group(1)
        pat     = r'\[([^\[\]]+?)\]\((.+?)\)'
        section = re.sub(pat, rep, section)

        # drop:     drop this character
        # replace:  Replace this character
        # keep:     keep this character
        # all else: hyphenate
        drop    = '/='
        replace = {
            '"' : 'quot'
        }
        keep    = string.ascii_lowercase + string.digits + '_-'

        # We use "@" as a placeholder, then
        # replace with hyphens below
        for c in section:
            if   c in drop:     c = ''
            elif c in replace:  c = replace[c]
            elif c not in keep: c = '@'
            sanitised.append(c)

        # Squeeze any replaced characters down
        # to a single hyphen. If a section
        # began/ended with a hyphens, a single
        # hyphen is preserved. But if a section
        # began/ended with replaced characters,
        # they are removed.
        section = ''.join(sanitised)
        section = section.lstrip('@').rstrip('@')
        section = re.sub('@+', '-', section)
        section = re.sub('-+', '-', section)

        # handle duplicated names
        basename = section
        count    = 0
        while section in sections:
            count  += 1
            section = f'{basename}-{count}'

        sections.append(section)

    return sections


def read_links_in_file(filename, docdir):

    with open(filename, 'rt') as f:
        content = f.read()

    newline = '~$~$~$~$~$~$~$~$'
    content = newline.join(content.split('\n'))

    # markdown links
    pat     = r'(!?)\[([^\[\]]+?)\]\((.+?)\)'
    links   = []
    relfile = op.relpath(filename, docdir)

    for hit in re.finditer(pat, content):
        image, text, target = hit.groups()
        lineno              = content[:hit.start()].count(newline) + 1

        links.append(Link(relfile,
                          text,
                          LinkTarget.create(relfile, target),
                          lineno,
                          image == '!'))

    return links


class BadLinkError(Exception):
    pass


def check_link(link, docdir, file_index):

    target = link.target

    if target.external:
        return

    # link to another doc file - should be in index
    if target.url.endswith('.md') and (target.url not in file_index):
        raise BadLinkError(f'Link target file not in index:\n  {link}')

    # link to an image file - should
    # be relative to the file
    if link.image:
        targetfile = op.join(op.dirname(link.filename), target.url)

    # link to another doc or data file -
    # should be relative to the root dir
    else:
        targetfile = target.url

    if not op.isfile(op.join(docdir, targetfile)):
        raise BadLinkError(f'Link target file doesn\'t exist:\n'
                           f'  {link} -> {targetfile}')

    if target.section is None:
        return

    targetfile = file_index[target.url]

    if target.section not in targetfile.sections:
        raise BadLinkError(f'Link section not in target file:\n  {link}')


def main():

    if len(sys.argv) != 2:
        print('Usage: check_links.py docroot')
        sys.exit(1)

    docdir  = sys.argv[1]
    index   = build_file_index(docdir)
    ngood   = 0
    nbroken = 0

    for file in index.values():

        for link in file.links:
            try:
                check_link(link, docdir, index)
                ngood += 1
            except BadLinkError as e:
                print(str(e))
                nbroken += 1

    if nbroken == 0:
        print(f'Scanned {ngood} links, none appeared to be broken')
    else:
        print(f'Scanned {ngood + nbroken} links - {nbroken} appear to be broken')

    return nbroken


if __name__ == '__main__':
    sys.exit(main())

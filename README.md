# FSL documentation

This is the markdown source for the FSL documentation. These markdown files are
rendered as html via our Gitlab Pages site, currently at:

https://fsl.fmrib.ox.ac.uk/fsl/docs/


## How to contribute - the easy way

You can edit files directly through the gitlab web interface.

1. Navigate to the file you want to edit, and click the **Edit** button at the top right. If you just want to edit the one file, choose _Edit single file_. If you want to edit multiple files, you can choose _Open in Web IDE_ (although this is a much more complicated interface). The instructions below assume that you have chosen _Edit single file_.

2. Edit the file as needed.

3. Click the **Commit changes** button at the bottom of the page. This will create a _Merge request_, which is a request for your edits to be merged into the documentation.

4. Add a title and description if you like, and then click the **Create merge request** button.

5. Wait for one of the FSL maintainers to merge your changes.


## How to contribute - working locally

Follow these instructions if you want to edit the documentation on your local computer using your text editor of choice.

1. Install `git` and `git lfs` if you have not already done so. You probably already have `git`, but `git lfs` is not often installed by default. On macOS, you can install it from https://git-lfs.com/, or via homebrew or macports. Once you have installed `git lfs`, open a terminal and run:


   ```bash
   git lfs install
   ```

2. Download a local copy of the new documentation site:

   ```bash
   git clone git@git.fmrib.ox.ac.uk:fsl/docs.git
   cd docs
   ```

3. Download any recent changes, and then create a branch for your new changes - give your branch a sensible name, e.g.:

   ```bash
   git fetch --all
   git checkout -b add-fnirt-docs origin/main
   ```

4. Make your changes/add your content (more details on this below).

5. Add/commit your changes:

   ```bash
   git add docs/registration/fnirt.md
   git commit -m "Added FNIRT page"
   ```

6. Push your changes to gitlab:

   ```bash
   git push origin add-fnirt-docs
   ```

7. Open a merge request at https://git.fmrib.ox.ac.uk/fsl/docs/


## Adding content

You can edit any markdown file in `docs/`.

If you need to create a new section:

- create the new files/directories in `docs/`
- if you want the file to appear in the sidebar for navigation then add the
  link to `docs/_sidebar.md` (or `docs/<subdirectory>/_sidebar.md`). You can see other links in that file as a guide.

See the [Markdown cheat sheet](#markdown-docsify-cheat-sheet) below for more details and suggestions.

## Viewing content locally

### 1. Without a server

If you want to view the docs locally you can simply open `docs/index.html` in
a web browser.

For example, on macOS you could run this command in a terminal:

`open docs/index.html # to open with your default browser`

### 2. With a server

You can also serve them with a locally running server. If you have python 3.7
or greater installed, or `fslpython` (part of FSL) you can do:

`fslpython -m http.server 8080 # or some other port if 8080 is taken`

or

`python -m http.server 8080 # or some other port if 8080 is taken`

And then visit http://localhost:8080/docs/ in your web browser.


## Markdown/docsify cheat sheet

The official Markdown syntax reference can be found at https://daringfireball.net/projects/markdown/syntax

There are many markdown renderers, all of which may support slightly different features - docsify uses the [marked](https://github.com/markedjs/marked) parser.

Some docsify-specific extensions are detailed at https://docsify.js.org/#/helpers


## Custom block quotes

Docsify provides a few custom markdown extensions for nicer block quotes. The contents for these block quotes must all be on a single line (or with newline
characters escaped with a back slash):

* Regular block quotes (standard Markdown)
  ```
  > **Some important information**
  > This will be rendered in a separate box.
  ```

* Warnings (note that newlines are escaped)
  ```
  !> WARNING: **Do not press the red button under any circumstances**\
  The last person who pushed it hasn't been seen since.
  ```

* Informative
  ```
  ?> Note that if you do inadvertently push the red button, \
  your warranty will be voided immediately.
  ```


## Linking to other pages and content

When link to other markdown files and attachments, you must always specify the path as being relative to the root of the `docs` directory. For example, to link to `registration/flirt.md`:


```
Check out the [FLIRT documentation](registration/flirt.md).
```

### Linking to images

In contrast to the above, when displaying an image using markdown syntax, you must specify the image file as being relative to the _markdown_ file. For example, if you want to display `structural/t1.png` in `structural/bet.md`, you would use:

```
![T1 image](t1.png)
```

You can also use raw HTML, in which case you need to refer to the image file using a path relative to the root of `docs`. This option gives you more control over layout, e.g.:

```
<img src="structural/t1.png" align="right" width="30%"/>
```


### Linking to attachments

If you are trying to use markdown syntax to link to a data file, e.g.:

    [Download this file](resting_state/dual_regression.pdf)

Docsify may create the URL incorrectly. You can work around this by telling Docsify to use the link as-is, e.g.:

    [Download this file](resting_state/dual_regression.pdf ":ignore")


### Linking to sections within a page

Every Markdown heading will be turned into a linkable anchor - for example, this Markdown header:

```
## Some section
```

can be linked to with the following syntax:

```
Refer to [some section](#some-section) for more details.
```

The link (a.k.a _slug_) for a header is the same as the header, converted to lower-case, and with all non-alpha-numeric characters replaced with hyphens (e.g. the slug for `Some section` is `some-section`).

It is also possible to manually create and link to anchors anywhere in your file - for example, you can create an anchor for a paragraph like so:

```
<a id="some-paragraph" href="#some-paragraph">This paragraph</a> is a very
good paragraph
```

And link to it elsewhere like so:

```
Check out [this paragraph](#some-paragraph) as it is a very good paragraph.
```


## Code highlighting

If you are displaying code snippets, you can apply language-specific syntax highlighting by beginning the snippet with the language name:

    ```bash
    ls -l
    ```

    ```python
    def some_function():
        print('hello')
    ```

Only a few languages are supported - get in touch if it isn't working for you.

You can also create code blocks by indenting the block four spaces, although note that if your code contains dollar signs, it may confuse the latex renderer (see below). A safer option is to always use triple-backticks to denote code blocks.

The latex renderer can also get confused by escaped newlines, so avoid them if you can.


## Latex

We have enabled [mathjax](https://docs.mathjax.org/en/latest/) for rendering LaTeX mathematical equations. For an inline equation, use:

```
The equation $e=mc^2$ is a very good equation.
```

Or for a block equation, use:

```
The equation:

$$
e=mc^2
$$

is a very good equation.
```

## NIfTI preview with NiiVue

You can display one or more NIfTI images with NiiVue like so:


```
<div class="niivue-canvas" thumbnail="thumbnail.png">
<vol url="T1.nii.gz"/>
<vol url="T1_brain.nii.gz" colormap="red" opacity="0.5"/>
</div>
```

Paths to all files must be relative to the `docs/` directory. The thumbnail image is used as a preview before the NIfTI images are loaded.


## Automatic table of contents

You add an automatically generated table of contents to the top of your page like so:

```
<div id="pagetoc"
     start="1"
     end="5"
     offset="1"
     ordered="false"></div>
```

where:
 - `start`:   Highest header level to include (e.g. 1 = `<h1>`)
 - `end`:     Lowest header level to include (e.g. 5 = `<h5>`)
 - `offset`:  Skip the first `offset` header elements
 - `ordered`: `"true"` = use `<ol>` element, `"false"` = use `<ul>` element.
